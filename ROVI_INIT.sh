#!/bin/bash

# Source setup.bash
cd ~/catkin_ws/
source devel/setup.bash
cd

gnome-terminal --window -e 'bash -c "roscore"'
sleep 1 
 

if [ -z "$1" ]
then 
	echo "Specify if you use the Robot or UR Simulator"
	echo "Robot: 	    sudo ./ROVI_INIT.sh -r"
	echo "UR Simulator: sudo ./ROVI_INIT.sh -s"
else
	while getopts "rs" opt; do
		case $opt in
			s)
				gnome-terminal --window -e 'bash -c "gnome-terminal --window -e 'bash -c "rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd ; read"'  --tab -e 'rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd "'   --tab -e 'rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd "'   --tab -e 'rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd "'  --tab -e 'rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd "'  --tab -e 'rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd "' "' --tab -e 'bash -c "roslaunch wsg_50_driver wsg_50_tcp.launch; read"' --tab -e 'bash -c "roslaunch caros_universalrobot caros_universalrobot.launch; read"' --tab -e 'bash -c "sudo ~/ursim-5.2.0.61336/start-ursim.sh; read"'  			
				;;
			r)		
				gnome-terminal --window -e 'bash -c "roslaunch realsense2_camera rs_camera.launch filters:=pointcloud; read"' --tab -e 'bash -c "roslaunch wsg_50_driver wsg_50_tcp.launch; read"' --tab -e 'bash -c "roslaunch caros_universalrobot caros_universalrobot.launch  device_ip:="192.168.100.1"; read"'
				;;
			\?)
				echo "Specify if you use the Robot or UR Simulator"
				echo "Robot: 	    sudo ./ROVI_INIT.sh -r"
				echo "UR Simulator: sudo ./ROVI_INIT.sh -s"
				;;
		esac
	done
fi

