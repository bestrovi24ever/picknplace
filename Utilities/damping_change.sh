#!/bin/bash

cd ~/Robwork/RobWork/Build/RWHardware
cmake -DCMAKE_BUILD_TYPE=Release ../../RobWorkHardware
make -j4
cd ~/catkin_ws/
catkin build --force-cmake
