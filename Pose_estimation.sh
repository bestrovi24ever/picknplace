#!/bin/bash

source devel/setup.bash

#catkin_make 

echo "Step 1: Convert the .ply file to .pcd:"  
echo ""
#rosrun vision PLY_2_PCL             ./test.ply           -v 2>/dev/null 
echo "Step 2: Filter XYZ values of the point cloud:" 
echo ""
rosrun vision Filter_XYZ            ./Scene_new.pcd         2>/dev/null 
echo "Step 3: Fit plane corresponding to the table:"
echo ""
rosrun vision fit_plane             ./Result.pcd           2>/dev/null 
echo "Step 4: Remove all the small clusters:"
echo ""
#rosrun vision remove_small_clusters ./Result.pcd       -v   2>/dev/null 
echo "Step 5: Apply global alignment to locate the piece:"
echo ""
rosrun vision locate_piece       ./Utilities/Piece.pcd ./Result.pcd 

echo "Step 6: Apply global alignment to orientate the piece:"
echo ""
#rosrun vision locate_orientation ./Utilities/Piece.pcd ./Result.pcd -v
