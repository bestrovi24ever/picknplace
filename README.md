## ROS packages :
* Package needed for the tf topics:
```
	sudo apt-get install ros-melodic-tf2-tools
```

## Catkin Building :
* Symlink warnings, not being ale to create the symlink:
```
	catkin config --merge-devel
```

## Compiling the ROS Packages:
Build Catkin:
```
catkin_make NOT Catkin build
```

## Running the Project:
```
sudo ./ROVI_init.sh
```
## Running Pose estimation:
```
rosrun vision pose_estimation
```

## References 

* [PointClouds](http://pointclouds.org/documentation/tutorials/cylinder_segmentation.php) - Cylinder model segmentation
* [PointClouds](http://pointclouds.org/documentation/tutorials/cluster_extraction.php) - Euclidean Cluster Extraction
* [MatLab](https://se.mathworks.com/matlabcentral/fileexchange/40098-tolgabirdal-averaging_quaternions) - Quaternion Averaging function
* [Spiral](https://www.issackelly.com/blog/2017/01/20/mark-equidistant-points-on-a-spiral) - Equidistant Spiral algorithm
