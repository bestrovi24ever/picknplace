#ifndef SAMPLEPLUGIN_HPP
#define SAMPLEPLUGIN_HPP

#include <RobWorkStudioConfig.hpp> // For RWS_USE_QT5 definition

#include <rws/RobWorkStudioPlugin.hpp>

#include "ui_ROVI_Plugin.h"

class ROVI_Plugin: public rws::RobWorkStudioPlugin, private Ui::ROVI_Plugin
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
#if RWS_USE_QT5
Q_PLUGIN_METADATA(IID "dk.sdu.mip.Robwork.RobWorkStudioPlugin/0.1" FILE "ROVI_Plugin.json")
#endif
public:
    ROVI_Plugin();
	virtual ~ROVI_Plugin();

    virtual void open(rw::models::WorkCell* workcell);

    virtual void close();

    virtual void initialize();

private slots:
    void btnPressed();

    void stateChangedListener(const rw::kinematics::State& state);


};

#endif /*RINGONHOOKPLUGIN_HPP_*/
