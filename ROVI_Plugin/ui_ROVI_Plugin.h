/********************************************************************************
** Form generated from reading UI file 'ROVI_Plugin.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ROVI_Plugin_H
#define UI_ROVI_Plugin_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class UI_ROVI_Plugin
{
public:
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QPushButton *_btn0;
    QPushButton *_btn1;

    void setupUi(QDockWidget *ROVI_Plugin)
    {
        if (ROVI_Plugin->objectName().isEmpty())
            ROVI_Plugin->setObjectName(QStringLiteral("ROVI_Plugin"));
        ROVI_Plugin->resize(476, 119);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout_2 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        _btn0 = new QPushButton(dockWidgetContents);
        _btn0->setObjectName(QStringLiteral("_btn0"));

        verticalLayout->addWidget(_btn0);

        _btn1 = new QPushButton(dockWidgetContents);
        _btn1->setObjectName(QStringLiteral("_btn1"));

        verticalLayout->addWidget(_btn1);


        verticalLayout_2->addLayout(verticalLayout);

        ROVI_Plugin->setWidget(dockWidgetContents);

        retranslateUi(ROVI_Plugin);

        QMetaObject::connectSlotsByName(ROVI_Plugin);
    } // setupUi

    void retranslateUi(QDockWidget *ROVI_Plugin)
    {
        ROVI_Plugin->setWindowTitle(QApplication::translate("ROVI_Plugin", "DockWidget", Q_NULLPTR));
        _btn0->setText(QApplication::translate("ROVI_Plugin", "PushButton0", Q_NULLPTR));
        _btn1->setText(QApplication::translate("ROVI_Plugin", "PushButton1", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ROVI_Plugin: public UI_ROVI_Plugin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ROVI_Plugin_H
