#include "ROVI_Plugin.hpp"

#include <RobWorkStudio.hpp>
#include <rws/RobWorkStudio.hpp>
#include <rw/loaders/WorkCellFactory.hpp>

#include <boost/bind.hpp>

using rw::kinematics::State;
using rw::models::WorkCell;
using rws::RobWorkStudioPlugin;

ROVI_Plugin::ROVI_Plugin():
    RobWorkStudioPlugin("ROVI_PluginUI", QIcon(":/pa_icon.png"))
{
    setupUi(this);

    // now connect stuff from the ui component
    connect(_btn0    ,SIGNAL(pressed()), this, SLOT(btnPressed()) );
    connect(_btn1    ,SIGNAL(pressed()), this, SLOT(btnPressed()) );
    
}

ROVI_Plugin::~ROVI_Plugin()
{
}

void ROVI_Plugin::initialize() {
    getRobWorkStudio()->stateChangedEvent().add(boost::bind(&ROVI_Plugin::stateChangedListener, this, _1), this);
     // Auto load workcell
    rw::models::WorkCell::Ptr wc = rw::loaders::WorkCellLoader::Factory::load("/home/ant69/Documents/ROVI_2/picknplace/WorkCell/Scene.wc.xml");
    getRobWorkStudio()->setWorkCell(wc);

}

void ROVI_Plugin::open(WorkCell* workcell)
{
}

void ROVI_Plugin::close() {
}

void ROVI_Plugin::btnPressed() {
    QObject *obj = sender();
    if(obj==_btn0){
        log().info() << "Button 0\n";
    } else if(obj==_btn1){
        log().info() << "Button 1\n";
    }


}

void ROVI_Plugin::stateChangedListener(const State& state) {

}

#if !RWS_USE_QT5
#include <QtCore/qplugin.h>
Q_EXPORT_PLUGIN(ROVI_Plugin);
#endif
