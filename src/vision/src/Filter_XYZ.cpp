﻿#include <iostream>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

static void show_usage(std::string name)
{
    std::cout << "Usage: " << "<Path to pcd file> <option(s)>"
              << "This program applies a spatial filter on XYZ."
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-v,--visualization \tEnable point cloud visualization"
              << std::endl;
}

int main( int argc, char *argv [] ){

    std::cout << "--> Applying Pass trough filter in XYZ:"<<std::endl;
    // Parameters to modify the behaviour of this program:
    // Filter Treshold values:        X         Y          Z
    std::vector<float>   lim = { -0.3 ,0.1 , -0.2,0.1 , 0,0.7 };

    bool visualize=false;

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            show_usage(argv[0]);
            return 0;
        } else if ((arg == "-v") || (arg == "--visualization")) {
            visualize = true;
            cout << "--> Visualization of the Point Cloud enabled" <<endl;
        }
    }

    // Load the .pcd file into a Point cloud:
    pcl::PointCloud<pcl::PointXYZ>::Ptr original_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    if(!pcl::io::loadPCDFile(argv[1], *original_cloud)){
        std::cout << "--> File (.pcd) loaded correctly" << std::endl;
        std::cout << "----> PointCloud has: " << original_cloud->points.size () << " data points." << std::endl;
    }else {
        std::cout << "--> Couldn't load file (.pcd) " << std::endl;
    }

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> original_cloud_color (original_cloud, 0, 255, 0);
    if(visualize){
        // Visualize the Original Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_original_PCL (new pcl::visualization::PCLVisualizer("Original Cloud"));
        viewer_original_PCL->setBackgroundColor(0, 0, 0);
        viewer_original_PCL->addPointCloud<pcl::PointXYZ>(original_cloud, original_cloud_color, "Original Cloud");
    }

    //Filtering in Z:
        pcl::PassThrough<pcl::PointXYZ> pass_z;
    pass_z.setInputCloud (original_cloud);
    pass_z.setFilterFieldName ("z");
    pass_z.setFilterLimits (lim[4] ,lim[5]);
    //pass.setFilterLimitsNegative (true);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass_z (new pcl::PointCloud<pcl::PointXYZ>);
    pass_z.filter (*cloud_pass_z);

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_pass_z_color (cloud_pass_z, 0, 0, 255);
    if(visualize){
        // Visualize the Filtered_Z_Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_pass_z (new pcl::visualization::PCLVisualizer("Filtered Z Cloud"));
        viewer_pass_z->setBackgroundColor(0, 0, 0);
        viewer_pass_z->addPointCloud<pcl::PointXYZ>(cloud_pass_z, cloud_pass_z_color, "Cloud Pass Z");
    }
    std::cout << "--> Filtering in Z, keeping points between: " << lim[4] << " and " << lim[5] <<std::endl;
    std::cout << "----> Point cloud has " << cloud_pass_z->points.size () << " data points." << std::endl;

    //Filtering in Y:
    pcl::PassThrough<pcl::PointXYZ> pass_y;
    pass_y.setInputCloud (cloud_pass_z);
    pass_y.setFilterFieldName ("y");
    pass_y.setFilterLimits (lim[2] ,lim[3]);
    //pass.setFilterLimitsNegative (true);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass_y (new pcl::PointCloud<pcl::PointXYZ>);
    pass_y.filter (*cloud_pass_y);

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_pass_y_color (cloud_pass_y, 0, 255, 0);
    if(visualize){
        // Visualize the Filtered_Y_Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_pass_y (new pcl::visualization::PCLVisualizer("Filtered Y Cloud"));
        viewer_pass_y->setBackgroundColor(0, 0, 0);
        viewer_pass_y->addPointCloud<pcl::PointXYZ>(cloud_pass_z, cloud_pass_z_color, "Cloud Pass Z");
        viewer_pass_y->addPointCloud<pcl::PointXYZ>(cloud_pass_y, cloud_pass_y_color, "Cloud Pass Y");
    }

    std::cout << "--> Filtering in Y, keeping points between: " << lim[2] << " and " << lim[3] <<std::endl;
    std::cout << "----> Point cloud has " << cloud_pass_y->points.size () << " data points." << std::endl;

    //Filtering in X:
    pcl::PassThrough<pcl::PointXYZ> pass_x;
    pass_x.setInputCloud (cloud_pass_y);
    pass_x.setFilterFieldName ("x");
    pass_x.setFilterLimits (lim[0] ,lim[1]);
    //pass.setFilterLimitsNegative (true);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass_x (new pcl::PointCloud<pcl::PointXYZ>);
    pass_x.filter (*cloud_pass_x);


    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_pass_x_color (cloud_pass_x, 255, 0,0);
    if(visualize){
        // Visualize the Filtered_X_Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_pass_x (new pcl::visualization::PCLVisualizer("Filtered X"));
        viewer_pass_x->setBackgroundColor(0, 0, 0);
        viewer_pass_x->addPointCloud<pcl::PointXYZ>(cloud_pass_z, cloud_pass_z_color, "Cloud Pass Z");
        viewer_pass_x->addPointCloud<pcl::PointXYZ>(cloud_pass_y, cloud_pass_y_color, "Cloud Pass Y");
        viewer_pass_x->addPointCloud<pcl::PointXYZ>(cloud_pass_x, cloud_pass_x_color, "Cloud Pass X");

        while (!viewer_pass_x->wasStopped ())
        {
            viewer_pass_x->spinOnce (100);
            boost::this_thread::sleep (boost::posix_time::microseconds (100000));
        }
    }
    
    std::cout << "--> Filtering in X, keeping points between: " << lim[1] << " and " << lim[2] <<std::endl;
    std::cout << "----> Point cloud has " << cloud_pass_x->points.size () << " data points." << std::endl;

    // Save Point cloud to .pcd file:
    pcl::io::savePCDFileASCII ("Result.pcd", *cloud_pass_x);
    std::cout << "-->  Saved " << cloud_pass_x->points.size () << " data points to Result.pcd." << std::endl<< std::endl;

    return (0);
}
