#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>

using namespace std;
using namespace cv;

// Function declarations
void drawAxis(Mat&, Point, Point, Scalar, const float);
bool getOrientation(const vector<Point> &, Mat&,float,  float );
void calibration(const Mat&);

void drawAxis(Mat& img, Point p, Point q, Scalar colour, const float scale = 0.2)
{
    double angle = atan2( (double) p.y - q.y, (double) p.x - q.x ); // angle in radians
    double hypotenuse = sqrt( (double) (p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));
    // Here we lengthen the arrow by a factor of scale
    q.x = (int) (p.x - scale * hypotenuse * cos(angle));
    q.y = (int) (p.y - scale * hypotenuse * sin(angle));
    line(img, p, q, colour, 1, LINE_AA);
    // create the arrow hooks
    p.x = (int) (q.x + 9 * cos(angle + CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle + CV_PI / 4));
    line(img, p, q, colour, 1, LINE_AA);
    p.x = (int) (q.x + 9 * cos(angle - CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle - CV_PI / 4));
    line(img, p, q, colour, 1, LINE_AA);
}

bool getOrientation(const vector<Point> &pts, Mat &img, float ratio_upper, float ratio_lower)
{
    //Construct a buffer used by the pca analysis
    int sz = static_cast<int>(pts.size());
    Mat data_pts = Mat(sz, 2, CV_64F);
    for (int i = 0; i < data_pts.rows; i++)
    {
        data_pts.at<double>(i, 0) = pts[i].x;
        data_pts.at<double>(i, 1) = pts[i].y;
    }

    //Perform PCA analysis
    PCA pca_analysis(data_pts, Mat(), PCA::DATA_AS_ROW);
    //Store the center of the object
    Point cntr = Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
                       static_cast<int>(pca_analysis.mean.at<double>(0, 1)));

    //Store the eigenvalues and eigenvectors
    vector<Point2d> eigen_vecs(2);
    vector<double> eigen_val(2);
    for (int i = 0; i < 2; i++)
    {
        eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                                pca_analysis.eigenvectors.at<double>(i, 1));
        eigen_val[i] = pca_analysis.eigenvalues.at<double>(i);
    }

    // Draw the principal components
    Point p1 = cntr + 0.02 * Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
    Point p2 = cntr - 0.02 * Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]), static_cast<int>(eigen_vecs[1].y * eigen_val[1]));

    double angle = atan2(eigen_vecs[0].y, eigen_vecs[0].x); // orientation in radians

    // Take only the contours which are circular enough
    if(std::abs(eigen_val[0]/eigen_val[1]) < ratio_upper/100 && std::abs(eigen_val[1]/eigen_val[0]) <ratio_upper/100){
          if(std::abs(eigen_val[0]/eigen_val[1]) >ratio_lower/100 && std::abs(eigen_val[1]/eigen_val[0]) >ratio_lower/100){
            circle(img, cntr, 3, Scalar(0, 0, 255), 2);
            drawAxis(img, cntr, p1, Scalar(0, 255, 0), 1);
            drawAxis(img, cntr, p2, Scalar(255, 255, 0), 5);
            putText(img,string(), Point(5,100), FONT_HERSHEY_DUPLEX, 1, Scalar(0,143,143), 2);
            return true;
        }else {
            return false;
        }
    }else {
        return false;
    }
}

// Calibration Camera to World.
void calibration(const cv::Mat& img)
{
    // Sliders values
    int t_Binary_lower = 0;
    int t_Binary_upper = 255;

    int t_Area_lower = 0;
    int t_Area_upper = 200;

    int blur_level=1;

    int ratio_upper = 200;
    int ratio_lower = 0;


    cv::Mat out;
    cv::Mat filtered_contours;

    auto f = [&]() {

        out = img.clone();
        filtered_contours = img.clone();

        // Convert image to grayscale
        cv::Mat gray;
        cvtColor(out, gray, COLOR_BGR2GRAY);

        // Blur to reduce effect of noise
        cv::Mat blured;
        if(blur_level==0){
            cv::medianBlur(gray , blured , 1);
        }else{
            cv::medianBlur(gray , blured , (blur_level*2 -1));
        }

        // Convert image to binary
        cv::Mat bw;
        cv::threshold(blured, bw, t_Binary_lower, t_Binary_upper,THRESH_OTSU);

        // Find all the contours in the thresholded image
        vector<vector<Point> > contours;
        findContours(bw, contours, RETR_LIST, CHAIN_APPROX_NONE);

        for (size_t i = 0; i < contours.size(); i++){

            // Calculate the area of each contour
            double area = contourArea(contours[i]);

            if(area < t_Area_upper && area > t_Area_lower){
                // Filtered contours:
                drawContours(filtered_contours, contours, static_cast<int>(i), Scalar(0, 255, 0), 2);
                // Check for circular shapes:
                if(getOrientation(contours[i], out, ratio_upper,ratio_lower)){

                    drawContours(out, contours, static_cast<int>(i), Scalar(0, 255, 0), 2);

                }
            }
        }

        // Containers to put all the images together:
        cv::Mat blured_and_bw = cv::Mat::zeros(2*out.rows,2*out.cols,out.type());
        cv::Mat contour_and_final = cv::Mat::zeros(2*out.rows,2*out.cols,out.type());
        cv::Mat result = cv::Mat::zeros(4*out.rows,2*out.cols,out.type());

        // Convert gray and blured images to rgb:
        cv::Mat bw_dummy;
        cv::Mat bw_dummy2;
        bw_dummy = bw.clone();
        cvtColor(bw_dummy,bw_dummy2,CV_GRAY2RGB);

        cv::Mat blured_dummy;
        cv::Mat blured_dummy2;
        blured_dummy = blured.clone();
        cvtColor(blured_dummy,blured_dummy2,CV_GRAY2RGB);

        // Put all the images together:
        hconcat(blured_dummy2,bw_dummy2,blured_and_bw);
        hconcat(filtered_contours,out,contour_and_final);
        vconcat(blured_and_bw,contour_and_final,result);

        cv::imshow("Calibration",result);


    };

    cv::namedWindow("Calibration",WINDOW_NORMAL);
    auto callback = [](int, void* f_ptr) { (*static_cast<decltype(f)*>(f_ptr))(); };
    cv::createTrackbar("Bluring", "Calibration", &blur_level, 10, callback, &f);
    cv::createTrackbar("Axis Ratio: L", "Calibration", &ratio_upper, 300, callback, &f);
    cv::createTrackbar("Axis Ratio: U", "Calibration", &ratio_lower, 300, callback, &f);
    cv::createTrackbar("C.Area: L", "Calibration", &t_Area_lower, 200, callback, &f);
    cv::createTrackbar("C.Area: U", "Calibration", &t_Area_upper, 200, callback, &f);
    f();

    while (cv::waitKey() != 27)
        ;

    cv::destroyWindow("Calibration");
}


int main(int argc, char** argv)
{
    // Load image
    CommandLineParser parser(argc, argv, "{@input | ../input_image.jpg | input image}");
    parser.about( "This programs filters an image in order to obtain the markers on the table.\n" );
    parser.printMessage();
    Mat src = imread(parser.get<String>("@input"));
    // Check if image is loaded successfully
    if(src.empty())
    {
        cout << "Problem loading image!!!" << endl;
        return EXIT_FAILURE;
    }

    calibration(src);

    return 0;
}
