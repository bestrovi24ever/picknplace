#include <pcl/conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/common/random.h>
#include <pcl/common/time.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/spin_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
// ROS headers
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
// point cloud definition
#include "PreProcessing.h"
#include "localization.h"
#include "rw/rw.hpp"
#include <rw/kinematics/Frame.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <chrono>
#include <fstream>
#include <iostream> 

// namspace
using namespace std;
using namespace sensor_msgs;
using namespace pcl;
using namespace pcl::common;
using namespace pcl::io;
using namespace pcl::registration;
using namespace pcl::search;
using namespace pcl::visualization;
using namespace Eigen;

typedef PointNormal PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef Histogram<153> FeatureT;

void save_data(std::string filepath, double d1,double d2,double d3,double d4,double d5,double d6);
pcl::PointCloud<PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<PointXYZRGBA>);
PCLVisualizer* v1;


void callback(const sensor_msgs::PointCloud2 pCloud)
{
  // new cloud formation
  pcl::fromROSMsg(pCloud, *cloud);
  v1->updatePointCloud(cloud, "scene");
  v1->setCameraPosition(-0.252034, 0.341164, -0.57032,-0.128131, 0.122338, 0.0488766,-0.0385026, -0.94455, -0.326102);
  
  // cloud is the one you are interested about.
}
static void show_usage(std::string name)
{
    std::cout << "Usage: " << "<Path to pcd file> <option(s)>\n"
              << "This program estimates the position of the piece in the scene.\n"
              << "Options:\n"
              << "\t-h,--help \tShow this help message\n"
              << "\t-s,--save \t Save file eg: -c -s test.pcd\n"
              << "\t-l,--load \t Load file eg: -l Pointcloud_samples/test.pcd\n"
              << "\t-c,--camera \t enable ros node and real time camera "
              << std::endl;
}
int main(int argc, char **argv)
{
  bool save_file = false;
  bool load_file = false;
  bool enable_camera = false;
  std::string load_filename, save_filename;
  PreProcessing Pre_processor;
  Localization localizer;

  pcl::PointCloud<PointT>::Ptr object(new pcl::PointCloud<PointT>);

  typedef pcl::PointCloud<pcl::PointNormal> CloudType2;
  CloudType2::Ptr scene(new CloudType2);
  CloudType2::Ptr scene_post_global(new CloudType2);
  CloudType2::Ptr object_local(new CloudType2);
  CloudType2::Ptr object_global(new CloudType2);
  CloudType2::Ptr object_global_new(new CloudType2);

  
  Matrix4f pose_global, pose_local, pose_global_new;
  pcl::PointCloud<PointXYZ>::Ptr filter_xyz(new pcl::PointCloud<PointXYZ>);
  pcl::PointCloud<PointXYZ>::Ptr fit_pl(new pcl::PointCloud<PointXYZ>);
  pcl::PointCloud<PointXYZ>::Ptr fin(new pcl::PointCloud<PointXYZ>);

  pcl::ModelCoefficients::Ptr coefficients_plane(new pcl::ModelCoefficients);
  pcl::PointCloud<PointXYZ>::Ptr cloud2(new pcl::PointCloud<PointXYZ>);



  for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            show_usage(argv[0]);
            return 0;
        } else if ((arg == "-s") || (arg == "--save")) {
            save_file = true;
            save_filename = argv[i+1];
            cout << "--> Saving of the Point Cloud enabled" <<endl;
        } else if ((arg == "-l") || (arg == "--load"))
        {
            load_file = true;
            load_filename = argv[i+1];    
            cout << "--> Loading of the Point Cloud enabled" <<endl;
        } else if((arg == "-c") || (arg == "--camera"))
        {
            enable_camera = true;
            cout << "--> ROS node and Camera Point Cloud enabled" <<endl;
        }
  }
  if(enable_camera)
  {
    ros::init(argc, argv, "vision_node");
    ros::NodeHandle nh;
    ros::Rate rate(30); // frequency of operation
    
    // subscribe
    ros::Subscriber sub = nh.subscribe("/camera/depth_registered/points", 1, callback);
    // ros::Subscriber sub2 = nh.subscribe()
    //ros::Publisher pub = nh.advertise<std_msgs::String>("object_pose",100);

    //ros::spinOnce();
    v1 = new pcl::visualization::PCLVisualizer("Print scene");
    v1->addPointCloud<PointXYZRGBA>(cloud, PointCloudColorHandlerCustom<PointXYZRGBA>(cloud, 255, 0, 0), "scene");
    while (!v1->wasStopped())
    {

      ros::spinOnce();
      rate.sleep();
      v1->spinOnce();

      //viewer_local->spinOnce (100);
    }
    // // Shutdown subscriber
    sub.shutdown();
    if(save_file)
    {
        if(!pcl::io::savePCDFileASCII(save_filename, *cloud))
        {
            std::cout << "--> File (.pcd) saved correctly (Scene)" << std::endl;
            std::cout << "----> PointCloud has: " << cloud->points.size () << " data points." << std::endl;
            return 0;
        }
        else{
            cout << "File saved: " << save_filename << endl;
        }
        
    }
  }
  else
  {
    if(!pcl::io::loadPCDFile(load_filename, *cloud)){
        std::cout << "--> File (.pcd) loaded correctly (Scene)" << std::endl;
        std::cout << "----> PointCloud has: " << cloud->points.size () << " data points." << std::endl;
        // Removing the extension:
        size_t lastindex = load_filename.find_last_of(".");
        load_filename = load_filename.substr(0,lastindex);
        
    }else {
        std::cout << "--> Couldn't load file (Scene**.pcd) " << std::endl;
    }
  }
    
  
  copyPointCloud(*cloud,*cloud2);
  auto start_time = std::chrono::high_resolution_clock::now();
  filter_xyz = Pre_processor.Filter_XYZ(cloud2, false,load_file);
  auto filter_time = std::chrono::high_resolution_clock::now();
  fit_pl = Pre_processor.fit_plane(filter_xyz, coefficients_plane, false,load_file,load_filename);
  auto fitpl_time = std::chrono::high_resolution_clock::now();
  fin = Pre_processor.remove_small_clusters(fit_pl,false,load_file);
  auto fin_time = std::chrono::high_resolution_clock::now();
  copyPointCloud(*fin, *scene);
  auto pre_time = std::chrono::high_resolution_clock::now();
  pose_global = localizer.global_alignment(scene, object_global,coefficients_plane,5000, true,load_file,load_filename);
  auto global_time = std::chrono::high_resolution_clock::now();
  pose_local = localizer.local_alignment(object_global, scene, object_local, 1000, true,load_file,load_filename);
  auto local_time = std::chrono::high_resolution_clock::now();
  pose_global_new = pose_local * pose_global;
  localizer.Display_result_RGB(cloud,pose_global_new);

  
  //double filter_micro = std::chrono::duration_cast<std::chrono::microseconds>(filter_time).count();

  //rw::math::Vector3D<double> tempP(pose_local(0,3),pose_local(1,3),pose_local(2,3));
  cout << pose_global_new(0, 0) << " " << pose_global_new(3, 0) << endl;
  rw::math::Rotation3D<float> tempR(pose_global_new(0, 0), pose_global_new(0, 1), pose_global_new(0, 2), pose_global_new(1, 0), pose_global_new(1, 1), pose_global_new(1, 2), pose_global_new(2, 0), pose_global_new(2, 1), pose_global_new(2, 2));
  cout << tempR << endl;
  rw::math::Quaternion<float> tempQ(tempR);
  cout << tempQ << endl;

  if(load_file){
  double result;
  cout << "Did the pose estimation succeed? 0 for FAIL, 1 for SUCCESS" << endl;
  cin >> result;

  save_data("../Matlab/Vision/"+load_filename+"/Final_Position"+load_filename+".m",pose_global_new(0, 3),pose_global_new(1, 3),pose_global_new(2, 3),result,0.0,0.0);
  save_data("../Matlab/Vision/"+load_filename+"/Final_Rot_quaternions"+load_filename+".m",tempQ.getQx(),tempQ.getQy(),tempQ.getQz(),tempQ.getQw(),0.0,0.0);
  //save_data("./Matlab/Vision/"+load_filename+"/runTime_"+load_filename+".m",a,b,c,d,0,0);
  }

  //ros::spin();
  if(enable_camera)
  {
      tf2_ros::StaticTransformBroadcaster static_broadcaster;

      geometry_msgs::TransformStamped static_tf;

      static_tf.header.stamp = ros::Time::now();
      static_tf.header.frame_id = "ObjectPose";
      static_tf.child_frame_id = "Pose_for_robot";
      static_tf.transform.translation.x = pose_global_new(0, 3);
      static_tf.transform.translation.y = pose_global_new(1, 3);
      static_tf.transform.translation.z = pose_global_new(2, 3);
      static_tf.transform.rotation.x = tempQ.getQx();
      static_tf.transform.rotation.y = tempQ.getQy();
      static_tf.transform.rotation.z = tempQ.getQz();
      static_tf.transform.rotation.w = tempQ.getQw();
      ros::Rate rate(30);
      while (ros::ok())
      {
      // pub.publish(pose_local);
      static_broadcaster.sendTransform(static_tf);
      ros::spinOnce();
      rate.sleep();
      }

  }
  

  return 0;
}


void save_data(string filepath, double d1,double d2,double d3,double d4,double d5,double d6)
{
    std::ofstream file;
    //can't enable exception now because of gcc bug that raises ios_base::failure with useless message
    //file.exceptions(file.exceptions() | std::ios::failbit);
    file.open(filepath, std::ios::out | std::ios::app);
    if (file.fail())
        throw std::ios_base::failure(std::strerror(errno));

    //make sure write fails with exception if something is wrong
    file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file.seekp(0,ios::end);
    size_t size = file.tellp();
    if(size==0){

      cout<< "File: " << filepath << " wasn't initialized, initializing now." << endl;
      file  << " Data =[" ;
    }

    file  << d1 << "," <<  d2 << "," <<  d3 << "," <<  d4 << "," <<  d5 << ";"  << "\n";
}

