﻿#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

#include <pcl/filters/extract_indices.h>

using namespace pcl::visualization;


static void show_usage(std::string name)
{
    std::cout << "Usage: " << "<Path to pcd file> <option(s)> "
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-v,--visualization \tEnable point cloud visualization"
              << std::endl;
}


int main( int argc, char *argv [] )
{
    bool visualize=false;
    std::cout << "--> Remove the small clusters:"<<std::endl;
    // Parameters to modify the behaviour of this program:

    // Set RANSAC parameters
    int max_ransac_iterations = 100;
    const float thressq = 0.01 * 0.01;
    const int ransac_model_min_points = 10;    
    float inliers_treshold = 0.000001;

    cout << "--> Ransac Parameters:" << endl;
    cout << "----> Num of iterations: " << max_ransac_iterations << endl;
    cout << "----> Inliers treshold : " << inliers_treshold << endl;

    //float leaf_size = 0.0005f;
    float leaf_size = 0.0001f;
    cout << "--> Downsampling Parameters:" << endl;
    cout << "----> Leaf size: " << leaf_size << endl;

    // Clustering Parameters:
    int min_cluster_size = 600;
    int max_cluster_size = 25000;
    float cluster_tolerance = 0.02; // cm

    cout << "--> Cluster Parameters:" << endl;
    cout << "----> Cluster size, Max: " << max_cluster_size << " , Min: "<< min_cluster_size << endl;
    cout << "----> Cluster tolerance : " << cluster_tolerance << endl;


    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            show_usage(argv[0]);
            return 0;
        } else if ((arg == "-v") || (arg == "--visualization")) {
            visualize = true;
            cout << "--> Visualization of the Point Cloud enabled" <<endl;
        }
    }

    // Load the .pcd file into a Point cloud:
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);
    if(!pcl::io::loadPCDFile(argv[1], *cloud)){
        std::cout << "--> File (.pcd) loaded correctly" << std::endl;
        std::cout << "----> PointCloud has: " << cloud->points.size () << " data points." << std::endl;
    }else {
        std::cout << "--> Couldn't load file (.pcd) " << std::endl;
    }

    if(visualize){
        {
            PCLVisualizer v("Original Cloud");
            v.addPointCloud<pcl::PointXYZ>(cloud, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud, 0, 255, 0), "original cloud");
            v.spin();
        }
    }

    // Create the filtering object: downsample the dataset using a leaf size of X cm
    std::cout << "--> Downsampling the scene with leaf size: "<< leaf_size <<endl;
    pcl::VoxelGrid<pcl::PointXYZ> vg;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    vg.setInputCloud (cloud);
    vg.setLeafSize (leaf_size, leaf_size, leaf_size);
    vg.filter (*cloud_filtered);
    std::cout << "----> PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; 

    // Create the segmentation object for the planar model and set all the parameters
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::PCDWriter writer;
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (max_ransac_iterations);
    seg.setDistanceThreshold (inliers_treshold);

    int i=0, nr_points = (int) cloud_filtered->points.size ();
    while (cloud_filtered->points.size () > 0.3 * nr_points)
    {
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0)
        {
            std::cout << "--> Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }

        // Extract the planar inliers from the input cloud
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud (cloud_filtered);
        extract.setIndices (inliers);
        extract.setNegative (false);

        // Get the points associated with the planar surface
        extract.filter (*cloud_plane);
        //std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

        // Remove the planar inliers, extract the rest
        extract.setNegative (true);
        extract.filter (*cloud_f);
        *cloud_filtered = *cloud_f;
    }

    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (cloud_filtered);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (cluster_tolerance); // cm
    ec.setMinClusterSize (min_cluster_size);
    ec.setMaxClusterSize (max_cluster_size);
    ec.setSearchMethod (tree);
    ec.setInputCloud (cloud_filtered);
    ec.extract (cluster_indices);

    pcl::PointCloud<pcl::PointXYZ>::Ptr result (new pcl::PointCloud<pcl::PointXYZ>);

    int j = 0;
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit){
            
            result->points.push_back (cloud_filtered->points[*pit]);
            }

        result->width = result->points.size ();
        result->height = 1;
        result->is_dense = true;

        j++;
    }

        if(visualize){
            {
                PCLVisualizer v("Final Cloud");
                v.addPointCloud<pcl::PointXYZ>(cloud, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud, 255, 0, 0), "points under plane removed");
                v.addPointCloud<pcl::PointXYZ>(result, PointCloudColorHandlerCustom<pcl::PointXYZ>(result, 0, 255, 0), "original cloud");
                v.spin();
            }
        }

    // Save Point cloud to .pcd file:
    pcl::io::savePCDFileASCII ("Result.pcd", *result);
    std::cout << "--> Saved " << result->points.size () << " data points to Result.pcd." << std::endl<< std::endl;

    return (0);
}
