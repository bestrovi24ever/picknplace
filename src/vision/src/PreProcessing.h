#ifndef PreProcessing_H
#define PreProcessing_H

#include <iostream>
#include <string>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

using namespace std;

class PreProcessing
{
    private:

    public:
        PreProcessing();
        ~PreProcessing();
        pcl::PointCloud<pcl::PointXYZ>::Ptr Filter_XYZ(pcl::PointCloud<pcl::PointXYZ>::Ptr& original_cloud, bool visualize, bool save_data);
        pcl::PointCloud<pcl::PointXYZ>::Ptr fit_plane(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud,pcl::ModelCoefficients::Ptr, bool visualize, bool save_data,string filename);
        pcl::PointCloud<pcl::PointXYZ>::Ptr remove_small_clusters(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, bool visualize, bool save_data);
        void save_data(string filepath, double d1,double d2,double d3,double d4,double d5,double d6);


};

#endif