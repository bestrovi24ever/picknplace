#ifndef LOCALIZATION_H
#define LOCALIZATION_H

#include <pcl/conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/common/random.h>
#include <pcl/common/time.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/spin_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <string>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/io/ply_io.h>

using namespace std;
using namespace sensor_msgs;
using namespace pcl;
using namespace pcl::common;
using namespace pcl::io;
using namespace pcl::registration;
using namespace pcl::search;
using namespace pcl::visualization;
using namespace Eigen;

typedef pcl::Histogram<153> FeatureT;
typedef PointNormal PointT;
typedef pcl::PointCloud<PointT> PointCloud;

class Localization
{
    private:
        pcl::PointCloud<pcl::PointNormal>::Ptr* Object;
        inline float dist_sq(const FeatureT& query, const FeatureT& target);
        void nearest_feature(const FeatureT& query, const pcl::PointCloud<FeatureT>& target, int &idx, float &distsq,float threshold);
        
    public:
        
        Localization();
        ~Localization();
        Matrix4f global_alignment(pcl::PointCloud<pcl::PointNormal>::Ptr& scene_original, pcl::PointCloud<pcl::PointNormal>::Ptr& object_final,pcl::ModelCoefficients::Ptr coefficients_plane,const size_t iter,bool visualize,bool save_data,string filename);
        Matrix4f local_alignment(pcl::PointCloud<pcl::PointNormal>::Ptr& object_in,pcl::PointCloud<pcl::PointNormal>::Ptr& scene_in, pcl::PointCloud<pcl::PointNormal>::Ptr& object_out,const size_t &iter, bool visualize,bool save_data, string filename);
        pcl::PointCloud<pcl::PointNormal>::Ptr cloud_in_box(pcl::PointCloud<pcl::PointNormal>::Ptr& Scene,pcl::PointCloud<pcl::PointNormal>::Ptr& object_aligned,float cube_size, bool visualize);
        float Vectors_angle(Matrix4f T, pcl::ModelCoefficients::Ptr coefficients_plane);
        void  Display_result_RGB(pcl::PointCloud<PointXYZRGBA>::Ptr& Scene,Matrix4f T);
        void save_data(string filepath, double d1,double d2,double d3,double d4,double d5,double d6,double d7,double d8,double d9);


};


#endif