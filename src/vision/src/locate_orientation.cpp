#include <pcl/conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/common/random.h>
#include <pcl/common/time.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/spin_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/filter_indices.h>

// namspace
using namespace std;
using namespace sensor_msgs;
using namespace pcl;
using namespace pcl::common;
using namespace pcl::io;
using namespace pcl::registration;
using namespace pcl::search;
using namespace pcl::visualization;
using namespace Eigen;


typedef PointNormal PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef Histogram<153> FeatureT;
pcl::PointCloud<PointXYZ>::Ptr cloud(new pcl::PointCloud<PointXYZ>);
//PCLVisualizer v1("Print scene");
void nearest_feature(const FeatureT& query, const pcl::PointCloud<FeatureT>& target, int &idx, float &distsq,float &threshold);
void local_alignment(const pcl::PointCloud<PointT>::Ptr& object_in, const pcl::PointCloud<PointT>::Ptr& scene_in, pcl::PointCloud<PointT>::Ptr& object_out, const size_t &iterations);
void callback(const sensor_msgs::PointCloud2 pCloud)
{

}
static void show_usage(std::string name)
{
    std::cout << "Usage: " << "<Path to pcd file> <option(s)>"
              << "This program tries estimate the position of the piece in the scene."
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-v,--visualization \tEnable point cloud visualization"
              << std::endl;
}

int main( int argc, char *argv [] ){

    bool visualize=false;

    std::cout << "--> Locating piece in the scene:"<<std::endl;
    // Parameters to modify the behaviour of this program:
    // Set RANSAC parameters
    const size_t iter = 5000;// argc >= 4 ? std::stoi(argv[3]) : 5000;
    const float thressq = 0.01 * 0.01;
    const int ransac_model_min_points = 10;

    cout << "--> Ransac Parameters:" << endl;
    cout << "----> Num of iterations: " << iter << endl;
    cout << "----> Inliers treshold : " << thressq << endl;

    // Spin parameters:
    float spin_radius = 0.01;
    cout << "--> Spin Parameters:" << endl;
    cout << "----> Radius: " << spin_radius << endl;
    
    // Downsampling Parameters:
    float leaf_size = 0.002f;
    cout << "--> Downsampling Parameters:" << endl;
    cout << "----> Leaf size: " << leaf_size << endl;

    // Nearest feature threshold:
    float threshold_nearest = 0.02;
    cout << "--> Nearest Feature Parameters:" << endl;
    cout << "----> Threshod: " << threshold_nearest << endl;

    // Local alignment Parameters:
    float icc_iter  = 1000;
    cout << "--> Local alignment Parameters:" << endl;
    cout << "----> Num of iterations: " << icc_iter << endl;


    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            show_usage(argv[0]);
            return 0;
        } else if ((arg == "-v") || (arg == "--visualization")) {
            visualize = true;
            cout << "--> Visualization of the Point Cloud enabled" <<endl;
        }
    }
   
    pcl::PointCloud<PointT>::Ptr object(new pcl::PointCloud<PointT>);

    typedef pcl::PointCloud<pcl::PointNormal> CloudType2;
    CloudType2::Ptr scene(new CloudType2);
    CloudType2::Ptr sceneND(new CloudType2);
    CloudType2::Ptr sceneNaN(new CloudType2);
    CloudType2::Ptr objectFiltered(new CloudType2);
    CloudType2::Ptr object_out(new CloudType2);
    //copyPointCloud(*cloud, *scene);

    // Load the .pcd file into a Point cloud:
    if(!pcl::io::loadPCDFile(argv[1], *object)){
        std::cout << "--> File (.pcd) loaded correctly (Piece)" << std::endl;
        std::cout << "----> PointCloud has: " << object->points.size () << " data points." << std::endl;
    }else {
        std::cout << "--> Couldn't load file (object.pcd) " << std::endl;
    }

     // Load the .pcd file into a Point cloud:
    if(!pcl::io::loadPCDFile(argv[2], *sceneNaN)){
        std::cout << "--> File (.pcd) loaded correctly (Scene)" << std::endl;
        std::cout << "----> PointCloud has: " << scene->points.size () << " data points." << std::endl;
    }else {
        std::cout << "--> Couldn't load file (scene.pcd) " << std::endl;
    }

    std::vector< int > 	index ;
    pcl::removeNaNFromPointCloud(*sceneNaN,*sceneND,index);
    
    // Create the filtering object: downsample the dataset using a leaf size
    pcl::VoxelGrid<PointT> avg;
    avg.setInputCloud(object);
    avg.setLeafSize(leaf_size,leaf_size,leaf_size);
    avg.filter(*objectFiltered);
    std::cout << "----> PointCloud Object after Downsampling has: " << objectFiltered->points.size () << " data points." << std::endl;


    
    
    pcl::VoxelGrid<PointT> avg_s;
    avg_s.setInputCloud(sceneND);
    avg_s.setLeafSize(leaf_size,leaf_size,leaf_size);
    avg_s.filter(*scene);
    std::cout << "----> PointCloud Scene after Downsampling has: " << scene->points.size () << " data points." << std::endl;



    //searchPoint
    PointT searchPoint = objectFiltered->at(0) ;

    //result from radiusSearch()
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;

    //kdTree
    pcl::KdTreeFLANN<PointT> kdtree;
    kdtree.setInputCloud(objectFiltered);
    kdtree.setSortedResults(true);

    if ( kdtree.radiusSearch (searchPoint, 1, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0 )
    {
        //delete every point in target
        for (size_t j = 0; j < pointIdxRadiusSearch.size(); ++j)
        {
            //is this the way to erase correctly???
            object_out->push_back(objectFiltered->points[pointIdxRadiusSearch[j]]);
        }
    }

    {
        PCLVisualizer v("Object");
        v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 0, 255, 0), "scene");
        v.addPointCloud<PointT>(object_out, PointCloudColorHandlerCustom<PointT>(object_out, 0, 255,0 ),"object_out");
        v.spin();
    }
        
    ScopeTime t("Surface normals");
    NormalEstimation<PointT,PointT> ne;
    ne.setKSearch(10);
    std::cout << "--> K-searched" << std::endl;    
    ne.setInputCloud(object_out);
    ne.compute(*object_out);
    std::cout << "--> Object" << std::endl;    
    ne.setInputCloud(scene);
    ne.compute(*scene);
    std::cout << "--> Scene" << std::endl;
    
    pcl::PointCloud<FeatureT>::Ptr object_features(new pcl::PointCloud<FeatureT>);
    pcl::PointCloud<FeatureT>::Ptr scene_features(new pcl::PointCloud<FeatureT>);
    
    //ScopeTime t("Shape features");
        
    SpinImageEstimation<PointT,PointT,FeatureT> spin;
    spin.setRadiusSearch(spin_radius);
    std::cout << "--> Radius searched" << std::endl;
    spin.setInputCloud(object_out);
    spin.setInputNormals(object_out);
    spin.compute(*object_features);
    std::cout << "--> Object features computed" << std::endl;
    spin.setInputCloud(scene);
    spin.setInputNormals(scene);
    spin.compute(*scene_features);
    std::cout << "--> Scene features computed" << std::endl;
    
    // Find feature matches
    Correspondences corr(object_features->size());
    std::cout << "--> Corr" << std::endl;
    //ScopeTime t("Feature matches");
    std::cout << "--> Object Size:" << object_features->size() << std::endl;
    for(size_t i = 0; i < object_features->size(); ++i) {
        corr[i].index_query = i;
        //std::cout << "Count:" << i << std::endl;
        nearest_feature(object_features->points[i], *scene_features, corr[i].index_match, corr[i].distance,threshold_nearest);
    }
    std::cout << "--> Nearest feature done" << std::endl;
    
    /*if(visualize){
    // Show matches
    {
        PCLVisualizer v("Matches");
        v.addPointCloud<PointT>(object_out, PointCloudColorHandlerCustom<PointT>(object_out, 0, 255, 0), "object");
        v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 255, 0, 0),"scene");
        v.addCorrespondences<PointT>(object_out, scene, corr, 1);
        v.spin();
    }
    }*/

    // Create a k-d tree for scene
    search::KdTree<PointNormal> tree;
    tree.setInputCloud(scene);
     
    // Start RANSAC
    Matrix4f pose = Matrix4f::Identity();
    pcl::PointCloud<PointNormal>::Ptr object_aligned(new pcl::PointCloud<PointNormal>);
    float penalty = FLT_MAX;
    //ScopeTime t("RANSAC");
    cout << "--> Starting RANSAC..." << endl;
    UniformGenerator<int> gen(0, corr.size() - 1);
    for(size_t i = 0; i < iter; ++i) {
        if((i + 1) % 100 == 0)
            cout << "\t" << i+1 << endl;
        // Sample X random correspondences
        vector<int> idxobj(ransac_model_min_points);
        vector<int> idxscn(ransac_model_min_points);
        for(int j = 0; j < ransac_model_min_points; ++j) {
            int idx = gen.run();
            while(corr[idx].index_match==-1)
            {
                idx = gen.run();
            }
            idxobj[j] = corr[idx].index_query;
            idxscn[j] = corr[idx].index_match;
        }
            
        // Estimate transformation
        Matrix4f T;
        TransformationEstimationSVD<PointNormal,PointNormal> est;
        est.estimateRigidTransformation(*object_out, idxobj, *scene, idxscn, T);
            
        // Apply pose
        transformPointCloud(*object_out, *object_aligned, T);
            
        // Validate
        vector<vector<int> > idx;
        vector<vector<float> > distsq;
        tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);
            
        // Compute inliers and RMSE
        size_t inliers = 0;
        float rmse = 0;
        for(size_t j = 0; j < distsq.size(); ++j)
            if(distsq[j][0] <= thressq)
                ++inliers, rmse += distsq[j][0];
        rmse = sqrtf(rmse / inliers);
            
        // Evaluate a penalty function
        const float outlier_rate = 1.0f - float(inliers) / object_out->size();
        //const float penaltyi = rmse;
        const float penaltyi = outlier_rate;
            
        // Update result
        if(penaltyi < penalty) {
            cout << "----> Got a new model with " << inliers << " inliers!" << endl;
            penalty = penaltyi;
            pose = T;
        }
    }
        
    transformPointCloud(*object_out, *object_aligned, pose);
        
    // Compute inliers and RMSE
    vector<vector<int> > idx;
    vector<vector<float> > distsq;
    tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);
    size_t inliers = 0;
    float rmse = 0;
    for(size_t i = 0; i < distsq.size(); ++i)
        if(distsq[i][0] <= thressq)
            ++inliers, rmse += distsq[i][0];
    rmse = sqrtf(rmse / inliers);
    
    // Print pose
    cout << "----> Got the following pose:" << endl << pose << endl;
    cout << "------> Inliers: " << inliers << "/" << object_out->size() << endl;
    cout << "------> RMSE: " << rmse << endl;
    
    if(visualize){
    // Show result
    {
        PCLVisualizer v("After global alignment");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032,-0.128131, 0.122338, 0.0488766,-0.0385026, -0.94455, -0.326102);
        v.addPointCloud<PointT>(object_aligned, PointCloudColorHandlerCustom<PointT>(object_aligned, 0, 255, 0), "object_aligned");
        v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 255, 0, 0),"scene");
        v.spin();
    }
    }

    CloudType2::Ptr object_final(new CloudType2);
    local_alignment(object_aligned,scene,object_final,icc_iter);
    {
        PCLVisualizer v("After local alignment");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032,-0.128131, 0.122338, 0.0488766,-0.0385026, -0.94455, -0.326102);
        v.addPointCloud<PointT>(object_aligned, PointCloudColorHandlerCustom<PointT>(object_final, 0, 255, 0), "object_final");
        v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 255, 0, 0),"scene");
        v.spin();
    }


    return 0;
}

inline float dist_sq(const FeatureT& query, const FeatureT& target) {
    float result = 0.0;
    for(int i = 0; i < FeatureT::descriptorSize(); ++i) {
        const float diff = reinterpret_cast<const float*>(&query)[i] - reinterpret_cast<const float*>(&target)[i];
        result += diff * diff;
    }
    
    return result;
}

void nearest_feature(const FeatureT& query, const pcl::PointCloud<FeatureT>& target, int &idx, float &distsq,float &threshold ){
    idx = 0;
    distsq = dist_sq(query, target[0]);
       
    //std::cout << "Target:" << target.size() << std::endl;
    for(size_t i = 1; i < target.size(); ++i) {
        const float disti = dist_sq(query, target[i]);
        if(disti < distsq) {
            idx = i;
            distsq = disti;
        }

    }
    //cout << threshold << endl;
    if(distsq > threshold){       
            idx = -1;
            distsq = 0;            
    }else{
        cout<< "Matching accepted: "<< distsq <<endl;
    }
}

void local_alignment(const pcl::PointCloud<PointT>::Ptr& object_in, const pcl::PointCloud<PointT>::Ptr& scene_in, pcl::PointCloud<PointT>::Ptr& object_out, const size_t &iter)
{
     // Create a k-d tree for scene
    search::KdTree<PointNormal> tree;
    tree.setInputCloud(scene_in);
    cout << "--> Cloud input" << endl;
    // Set ICP parameters
    const float thressq = 0.01 * 0.01;
    
    // Start ICP
    Matrix4f pose = Matrix4f::Identity();
    pcl::PointCloud<PointNormal>::Ptr object_aligned(new pcl::PointCloud<PointNormal>(*object_in));
    cout << "--> create aligned object" << endl;
    cout << "--> Starting ICP..." << endl;
    for(size_t i = 0; i < iter; ++i) {
        // 1) Find closest points
        vector<vector<int> > idx;
        vector<vector<float> > distsq;
        tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);
            
        // Threshold and create indices for object/scene and compute RMSE
        vector<int> idxobj;
        vector<int> idxscn;
        for(size_t j = 0; j < idx.size(); ++j) {
            if(distsq[j][0] <= thressq) {
                idxobj.push_back(j);
                idxscn.push_back(idx[j][0]);
            }
        }
            
        // 2) Estimate transformation
        Matrix4f T;
        TransformationEstimationSVD<PointNormal,PointNormal> est;
        est.estimateRigidTransformation(*object_aligned, idxobj, *scene_in, idxscn, T);
            
        // 3) Apply pose
        transformPointCloud(*object_aligned, *object_aligned, T);
            
        // 4) Update result
        pose = T * pose;
    }

    
        
    // Compute inliers and RMSE
    vector<vector<int> > idx;
    vector<vector<float> > distsq;
    tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);
    size_t inliers = 0;
    float rmse = 0;
    for(size_t i = 0; i < distsq.size(); ++i)
        if(distsq[i][0] <= thressq)
            ++inliers, rmse += distsq[i][0];
    rmse = sqrtf(rmse / inliers);
    
    // Print pose
    cout << "--> Got the following pose:" << endl << pose << endl;
    cout << "----> Inliers: " << inliers << "/" << object_in->size() << endl;
    cout << "----> RMSE: " << rmse << endl;
    copyPointCloud(*object_aligned, *object_out);
}