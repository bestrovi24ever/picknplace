
/***************************************************************************************
*    Code from the following sources has been used in this project:
*
*    Title: Pose estimation excercise (Lecture 04)
*    Author: Anders Glent 
*
***************************************************************************************/

#include "localization.h"
#include <time.h>
#include <rw/math/Vector3D.hpp>
#include <rw/math/RPY.hpp>
#include "rw/rw.hpp"
#include <rw/kinematics/Frame.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Rotation3D.hpp>

using namespace std;
using namespace sensor_msgs;
using namespace pcl;
using namespace pcl::common;
using namespace pcl::io;
using namespace pcl::registration;
using namespace pcl::search;
using namespace pcl::visualization;
using namespace Eigen;

typedef PointNormal PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef Histogram<153> FeatureT;
typedef pcl::PointCloud<pcl::PointNormal> CloudType2;
Localization::Localization()
{
    Object = new pcl::PointCloud<pcl::PointNormal>::Ptr(new pcl::PointCloud<pcl::PointNormal>);
}

Localization::~Localization()
{
}
Matrix4f Localization::global_alignment(pcl::PointCloud<pcl::PointNormal>::Ptr &scene_original, pcl::PointCloud<pcl::PointNormal>::Ptr &object_final, pcl::ModelCoefficients::Ptr coefficients_plane, const size_t iter, bool visualize, bool save_data, string filename)
{

    std::cout << "--> Locating piece in the scene:" << std::endl;
    // Set RANSAC parameters
    //const size_t iter = 500;// argc >= 4 ? std::stoi(argv[3]) : 5000;
    const float thressq = 0.005 * 0.005;
    const int ransac_model_min_points = 7;

    cout << "--> Ransac Parameters:" << endl;
    cout << "----> Num of iterations: " << iter << endl;
    cout << "----> Inliers threshold : " << thressq << endl;

    // Parameters to modify the behaviour of this program:
    // Spin parameters:
    float spin_radius = 0.005;
    cout << "--> Spin Parameters:" << endl;
    cout << "----> Radius: " << spin_radius << endl;

    // Downsampling Parameters:
    float leaf_size = 0.005f;
    cout << "--> Downsampling Parameters:" << endl;
    cout << "----> Leaf size: " << leaf_size << endl;

    // Nearest feature threshold:
    float threshold_nearest = 10;
    cout << "--> Nearest Feature Parameters:" << endl;
    cout << "----> Threshod: " << threshold_nearest << endl;

    // Nearest neighbour Parameters:
    float NN_radius = 0.005;
    cout << "--> Nearest neighbour Parameters:" << endl;
    cout << "----> NN Radius: " << NN_radius << endl;

    pcl::PointCloud<PointT>::Ptr object(new pcl::PointCloud<PointT>);

    CloudType2::Ptr scene(new CloudType2);
    CloudType2::Ptr objectFiltered(new CloudType2);
    CloudType2::Ptr object_out(new CloudType2);
    //copyPointCloud(*cloud, *scene);

    // Load the .pcd file into a Point cloud:
    if (save_data)
    {
        if (!pcl::io::loadPCDFile("../Utilities/Piece.pcd", *object))
        {
            std::cout << "--> File (.pcd) loaded correctly (Piece)" << std::endl;
            std::cout << "----> PointCloud has: " << object->points.size() << " data points." << std::endl;
            copyPointCloud(*object, **Object);
        }
        else
        {
            std::cout << "--> Couldn't load file (object.pcd) " << std::endl;
        }
    }
    else
    {
        if (!pcl::io::loadPCDFile("./Utilities/Piece.pcd", *object))
        {
            std::cout << "--> File (.pcd) loaded correctly (Piece)" << std::endl;
            std::cout << "----> PointCloud has: " << object->points.size() << " data points." << std::endl;
            copyPointCloud(*object, **Object);
        }
        else
        {
            std::cout << "--> Couldn't load file (object.pcd) " << std::endl;
        }
    }

    // Create the filtering object: downsample the dataset using a leaf size
    pcl::VoxelGrid<PointT> avg;
    avg.setInputCloud(object);
    avg.setLeafSize(leaf_size, leaf_size, leaf_size);
    avg.filter(*objectFiltered);
    std::cout << "----> PointCloud Object after Downsampling has: " << objectFiltered->points.size() << " data points." << std::endl;

    pcl::VoxelGrid<PointT> avg_s;
    avg_s.setInputCloud(scene_original);
    avg_s.setLeafSize(leaf_size, leaf_size, leaf_size);
    avg_s.filter(*scene);
    std::cout << "----> PointCloud Scene after Downsampling has: " << scene->points.size() << " data points." << std::endl;

    //searchPoint
    PointT searchPoint = objectFiltered->at(0);

    //result from radiusSearch()
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;

    //kdTree
    pcl::KdTreeFLANN<PointT> kdtree;
    kdtree.setInputCloud(objectFiltered);
    kdtree.setSortedResults(true);

    if (kdtree.radiusSearch(searchPoint, 1, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
    {
        //delete every point in target
        for (size_t j = 0; j < pointIdxRadiusSearch.size(); ++j)
        {
            //is this the way to erase correctly???
            object_out->push_back(objectFiltered->points[pointIdxRadiusSearch[j]]);
        }
    }

    if (visualize)
    {
        {
            PCLVisualizer v("Object");
            v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
            v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 255, 0, 0), "scene");
            v.addPointCloud<PointT>(object_out, PointCloudColorHandlerCustom<PointT>(object_out, 0, 255, 0), "object_out");
            v.spinOnce();
        }
    }

    ScopeTime t("Surface normals");
    NormalEstimation<PointT, PointT> ne;
    //ne.setKSearch(8);
    ne.setRadiusSearch(NN_radius);
    std::cout << "--> K-searched" << std::endl;
    ne.setInputCloud(object_out);
    ne.compute(*object_out);
    std::cout << "--> Object" << std::endl;
    ne.setInputCloud(scene);
    ne.compute(*scene);
    std::cout << "--> Scene" << std::endl;

    pcl::PointCloud<FeatureT>::Ptr object_features(new pcl::PointCloud<FeatureT>);
    pcl::PointCloud<FeatureT>::Ptr scene_features(new pcl::PointCloud<FeatureT>);

    //ScopeTime t("Shape features");

    SpinImageEstimation<PointT, PointT, FeatureT> spin;
    spin.setRadiusSearch(spin_radius);
    std::cout << "--> Radius searched" << std::endl;
    spin.setInputCloud(object_out);
    spin.setInputNormals(object_out);
    spin.compute(*object_features);
    std::cout << "--> Object features computed" << std::endl;
    spin.setInputCloud(scene);
    spin.setInputNormals(scene);
    spin.compute(*scene_features);
    std::cout << "--> Scene features computed" << std::endl;

    // Find feature matches
    Correspondences corr(object_features->size());
    std::cout << "--> Corr" << std::endl;
    //ScopeTime t("Feature matches");
    std::cout << "--> Object Size:" << object_features->size() << std::endl;
    for (size_t i = 0; i < object_features->size(); ++i)
    {
        corr[i].index_query = i;
        //std::cout << "Count:" << i << std::endl;
        nearest_feature(object_features->points[i], *scene_features, corr[i].index_match, corr[i].distance, threshold_nearest);
    }
    std::cout << "--> Nearest feature done" << std::endl;

    // Show matches
    // if(visualize){
    // {
    //     PCLVisualizer v("Matches");
    //     v.addPointCloud<PointT>(object_out, PointCloudColorHandlerCustom<PointT>(object_out, 0, 255, 0), "object");
    //     v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 255, 0, 0),"scene");
    //     v.addCorrespondences<PointT>(object_out, scene, corr, 1);
    //     v.spinOnce();
    // }
    // }

    // Create a k-d tree for scene
    search::KdTree<PointNormal> tree;
    tree.setInputCloud(scene);

    // Start RANSAC
    Matrix4f pose = Matrix4f::Identity();
    pcl::PointCloud<PointNormal>::Ptr object_aligned(new pcl::PointCloud<PointNormal>);
    float penalty = FLT_MAX;
    //ScopeTime t("RANSAC");
    cout << "--> Starting RANSAC..." << endl;
    size_t inliers = 0;
    size_t i = 0;
    UniformGenerator<int> gen(0, corr.size() - 1, time(NULL));
    cout << "Downsampled object size: " << 0.27 * object_out->size() << endl;

    float angle = 0.0;
    Matrix4f T;

    while (inliers < 0.27 * object_out->size() && i < iter)
    {
        // for(size_t i = 0; i < iter; ++i) {
        if ((i + 1) % 100 == 0)
            cout << "\t" << i + 1 << endl;

        //do{
        // Sample X random correspondences
        vector<int> idxobj(ransac_model_min_points);
        vector<int> idxscn(ransac_model_min_points);
        for (int j = 0; j < ransac_model_min_points; ++j)
        {
            int idx = gen.run();
            while (corr[idx].index_match == -1)
            {
                idx = gen.run();
            }
            idxobj[j] = corr[idx].index_query;
            idxscn[j] = corr[idx].index_match;
        }

        // Estimate transformation
        TransformationEstimationSVD<PointNormal, PointNormal> est;
        est.estimateRigidTransformation(*object_out, idxobj, *scene, idxscn, T);

        angle = Vectors_angle(T, coefficients_plane);

        //}while(angle > 0.2);
        //}while(angle < 3);

        // Apply pose
        transformPointCloud(*object_out, *object_aligned, T);

        // Validate
        vector<vector<int>> idx;
        vector<vector<float>> distsq;
        tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);

        // Compute inliers and RMSE
        inliers = 0;
        float rmse = 0;
        for (size_t j = 0; j < distsq.size(); ++j)
            if (distsq[j][0] <= thressq)
                ++inliers, rmse += distsq[j][0];
        rmse = sqrtf(rmse / inliers);

        // Evaluate a penalty function
        const float outlier_rate = 1.0f - float(inliers) / object_out->size();
        //const float penaltyi = rmse;
        const float penaltyi = outlier_rate;

        // Update result
        if (penaltyi < penalty)
        {
            cout << "----> Got a new model with " << inliers << " inliers!" << endl;
            cout << "--> Angle between the plane estimated and the pose found: " << angle << endl;
            penalty = penaltyi;
            pose = T;
        }
        i = i + 1;
    }

    transformPointCloud(*object_out, *object_aligned, pose);

    // Compute inliers and RMSE
    vector<vector<int>> idx;
    vector<vector<float>> distsq;
    tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);
    inliers = 0;
    float rmse = 0;
    for (size_t i = 0; i < distsq.size(); ++i)
        if (distsq[i][0] <= thressq)
            ++inliers, rmse += distsq[i][0];
    rmse = sqrtf(rmse / inliers);

    // Print pose
    cout << "----> Got the following pose:" << endl
         << pose << endl;
    cout << "------> Inliers: " << inliers << "/" << object_out->size() << endl;
    cout << "------> RMSE: " << rmse << endl;

    // Show result
    if (visualize)
    {
        {
            PCLVisualizer v("After global alignment");
            v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
            v.addPointCloud<PointT>(object_aligned, PointCloudColorHandlerCustom<PointT>(object_aligned, 0, 255, 0), "object_aligned");
            v.addPointCloud<PointT>(scene, PointCloudColorHandlerCustom<PointT>(scene, 255, 0, 0), "scene");
            v.spinOnce();
        }
    }
    copyPointCloud(*object_aligned, *object_final);
    //pose_final = pose;
    if (save_data)
    {
        Localization::save_data("../Matlab/Vision/" + filename + "/Global_" + filename + ".m", rmse, inliers, 0.0, angle, 0.0, 0.0, 0.0, 0.0, 0.0);
        Localization::save_data("../Matlab/Vision/" + filename + "/Global_T_rotation" + filename + ".m", pose(0, 0), pose(0, 1), pose(0, 2), pose(1, 0), pose(1, 1), pose(1, 2), pose(2, 0), pose(2, 1), pose(2, 2));
        Localization::save_data("../Matlab/Vision/" + filename + "/Global_T_position" + filename + ".m", pose(0, 3), pose(1, 3), pose(2, 3), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    }

    return pose;
}
Matrix4f Localization::local_alignment(pcl::PointCloud<pcl::PointNormal>::Ptr &object_in, pcl::PointCloud<pcl::PointNormal>::Ptr &scene_in, pcl::PointCloud<pcl::PointNormal>::Ptr &object_out, const size_t &iter, bool visualize, bool save_data, string filename)
{

    // Create a k-d tree for scene
    search::KdTree<PointNormal> tree;
    tree.setInputCloud(scene_in);
    cout << "--> Cloud input" << endl;
    // Set ICP parameters
    const float thressq = 0.0001;

    // Start ICP
    Matrix4f pose = Matrix4f::Identity();
    pcl::PointCloud<PointNormal>::Ptr object_aligned(new pcl::PointCloud<PointNormal>(*object_in));
    cout << "--> create aligned object" << endl;
    cout << "--> Starting ICP..." << endl;
    for (size_t i = 0; i < iter; ++i)
    {
        if ((i + 1) % 100 == 0)
           cout << "\t" << i + 1 << endl;

        // 1) Find closest points
        vector<vector<int>> idx;
        vector<vector<float>> distsq;
        tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);

        // Threshold and create indices for object/scene and compute RMSE
        vector<int> idxobj;
        vector<int> idxscn;
        for (size_t j = 0; j < idx.size(); ++j)
        {
            if (distsq[j][0] <= thressq)
            {
                idxobj.push_back(j);
                idxscn.push_back(idx[j][0]);
            }
        }

        // 2) Estimate transformation
        Matrix4f T;
        TransformationEstimationSVD<PointNormal, PointNormal> est;
        est.estimateRigidTransformation(*object_aligned, idxobj, *scene_in, idxscn, T);

        // 3) Apply pose
        transformPointCloud(*object_aligned, *object_aligned, T);

        // 4) Update result
        pose = T * pose;
    }

    // Compute inliers and RMSE
    vector<vector<int>> idx;
    vector<vector<float>> distsq;
    tree.nearestKSearch(*object_aligned, std::vector<int>(), 1, idx, distsq);
    size_t inliers = 0;
    float rmse = 0;
    for (size_t i = 0; i < distsq.size(); ++i)
        if (distsq[i][0] <= thressq)
            ++inliers, rmse += distsq[i][0];
    rmse = sqrtf(rmse / inliers);

    // Print pose
    cout << "----> Got the following pose:" << endl
         << pose << endl;
    cout << "------> Inliers: " << inliers << "/" << object_in->size() << endl;
    cout << "------> RMSE: " << rmse << endl;
    copyPointCloud(*object_aligned, *object_out);
    cout << "Copied pointcloud" << endl;
    if (visualize)
    {
        {
            PCLVisualizer v("After local alignment");
            v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
            v.addPointCloud<PointT>(object_aligned, PointCloudColorHandlerCustom<PointT>(object_aligned, 0, 255, 0), "object_final");
            v.addPointCloud<PointT>(scene_in, PointCloudColorHandlerCustom<PointT>(scene_in, 255, 0, 0), "scene");
            v.spinOnce();
        }
    }

    if (save_data)
    {
        Localization::save_data("../Matlab/Vision/" + filename + "/Local_" + filename + ".m", rmse, inliers, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        Localization::save_data("../Matlab/Vision/" + filename + "/Local_T_rotation_" + filename + ".m", pose(0, 0), pose(0, 1), pose(0, 2), pose(1, 0), pose(1, 1), pose(1, 2), pose(2, 0), pose(2, 1), pose(2, 2));
        Localization::save_data("../Matlab/Vision/" + filename + "/Local_T_position_" + filename + ".m", pose(0, 3), pose(1, 3), pose(2, 3), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    }

    return pose;
}


pcl::PointCloud<pcl::PointNormal>::Ptr Localization::cloud_in_box(pcl::PointCloud<pcl::PointNormal>::Ptr &Scene, pcl::PointCloud<pcl::PointNormal>::Ptr &object_aligned, float cube_size, bool visualize)
{
    float leaf_size = 0.002f;
    pcl::PointCloud<pcl::PointXYZ>::Ptr result(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointNormal>::Ptr result_n(new pcl::PointCloud<pcl::PointNormal>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr Scene_b(new pcl::PointCloud<pcl::PointXYZ>);
    copyPointCloud(*Scene, *Scene_b);
    std::cout << "--> Selecting the part of the PointCloud where the piece was located." << std::endl;
    float x = object_aligned->at(1).x;
    float y = object_aligned->at(1).y;
    float z = object_aligned->at(1).z;

    float x_min_c = x - cube_size + 10 * leaf_size;
    float x_max_c = x + cube_size + 10 * leaf_size;
    float y_min_c = y - cube_size + 10 * leaf_size;
    float y_max_c = y + cube_size + 10 * leaf_size;
    float z_min_c = z - cube_size + 10 * leaf_size;
    float z_max_c = z + cube_size + 10 * leaf_size;

    float x_min = x - cube_size + 10 * leaf_size;
    float x_max = x + cube_size + 10 * leaf_size;
    float y_min = y - cube_size + 10 * leaf_size;
    float y_max = y + cube_size + 10 * leaf_size;
    float z_min = z - cube_size + 10 * leaf_size;
    float z_max = z + cube_size + 10 * leaf_size;

    // build the condition
    pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond(new pcl::ConditionAnd<pcl::PointXYZ>());
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZ>("z", pcl::ComparisonOps::GT, z_min)));
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZ>("z", pcl::ComparisonOps::LT, z_max)));
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZ>("y", pcl::ComparisonOps::GT, y_min)));
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZ>("y", pcl::ComparisonOps::LT, y_max)));
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZ>("x", pcl::ComparisonOps::GT, x_min)));
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZ>("x", pcl::ComparisonOps::LT, x_max)));

    // build the filter
    pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
    condrem.setCondition(range_cond);
    condrem.setInputCloud(Scene_b);
    condrem.setKeepOrganized(true);

    // apply filter
    condrem.filter(*result);

    if (visualize)
    {
        {
            PCLVisualizer v("Selected region");
            v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
            v.addPointCloud<PointXYZ>(Scene_b, PointCloudColorHandlerCustom<PointXYZ>(Scene_b, 255, 0, 0), "scene");
            v.addCube(x_min_c, x_max_c, y_min_c, y_max_c, z_min_c, z_max_c, 0, 1.0, 0, "cube", 0);
            v.spinOnce();
        }
    }
    if (visualize)
    {
        {
            PCLVisualizer v("Final Result");
            v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
            v.addPointCloud<pcl::PointXYZ>(result, PointCloudColorHandlerCustom<pcl::PointXYZ>(result, 255, 0, 0), "scene");
            v.spinOnce();
        }
    }
    copyPointCloud(*result, *result_n);
    return result_n;
}

void Localization::nearest_feature(const FeatureT &query, const pcl::PointCloud<FeatureT> &target, int &idx, float &distsq, float threshold)
{
    idx = 0;
    distsq = dist_sq(query, target[0]);

    //std::cout << "Target:" << target.size() << std::endl;
    for (size_t i = 1; i < target.size(); ++i)
    {
        const float disti = dist_sq(query, target[i]);
        if (disti < distsq)
        {
            idx = i;
            distsq = disti;
        }
    }
    //cout << threshold << endl;
    if (distsq > threshold || isnan(distsq))
    {
        idx = -1;
        distsq = 0;
    }
    else
    {

        //cout<< "Matching accepted: "<< distsq <<endl;
    }
}
inline float Localization::dist_sq(const FeatureT &query, const FeatureT &target)
{
    float result = 0.0;
    for (int i = 0; i < FeatureT::descriptorSize(); ++i)
    {
        const float diff = reinterpret_cast<const float *>(&query)[i] - reinterpret_cast<const float *>(&target)[i];
        result += diff * diff;
    }

    return result;
}

float Localization::Vectors_angle(Matrix4f T, pcl::ModelCoefficients::Ptr coefficients_plane)
{

    rw::math::Vector3D<float> Plane_normal(coefficients_plane->values[0], coefficients_plane->values[1], coefficients_plane->values[2]);
    rw::math::Vector3D<float> Piece_Z_axis(T(0, 2), T(1, 2), T(2, 2));

    float angle = rw::math::angle(Plane_normal, Piece_Z_axis);

    return angle;
}

void Localization::Display_result_RGB(pcl::PointCloud<PointXYZRGBA>::Ptr &Scene, Matrix4f T)
{

    // Apply transformation to the object:
    pcl::PointCloud<pcl::PointNormal>::Ptr Object_T(new pcl::PointCloud<pcl::PointNormal>);
    transformPointCloud(**Object, *Object_T, T);

    // Visualization:
    {
        PCLVisualizer v("Result");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        v.addPointCloud<PointXYZRGBA>(Scene, "scene");
        v.addPointCloud<pcl::PointNormal>(Object_T, PointCloudColorHandlerCustom<pcl::PointNormal>(Object_T, 0, 255, 0), "object_T");
        //v.spinOnce();
        v.spin();
    }
}

void Localization::save_data(string filepath, double d1, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9)
{
    std::ofstream file;
    //can't enable exception now because of gcc bug that raises ios_base::failure with useless message
    //file.exceptions(file.exceptions() | std::ios::failbit);
    file.open(filepath, std::ios::out | std::ios::app);
    if (file.fail())
        throw std::ios_base::failure(std::strerror(errno));

    //make sure write fails with exception if something is wrong
    file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file.seekp(0, ios::end);
    size_t size = file.tellp();
    if (size == 0)
    {

        cout << "File: " << filepath << " wasn't initialized, initializing now." << endl;
        file << " Data =[";
    }

    file << d1 << "," << d2 << "," << d3 << "," << d4 << "," << d5 << "," << d6 << "," << d7 << "," << d8 << "," << d9 << ";"
         << "\n";
}