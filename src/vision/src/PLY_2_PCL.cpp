﻿#include <iostream>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>


static void show_usage(std::string name)
{
    std::cout << "Usage: " << "<Path to ply file>  <option(s)>"
              << "This program converts a mesh file (.ply) to Point cloud (.pcl)."
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-v,--visualization \tEnable point cloud visualization"
              << std::endl;
}

int main( int argc, char *argv [] ){

    std::cout << "--> Converting .ply file to .pcd file." << std::endl;
    bool visualize=false;

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            show_usage(argv[0]);
            return 0;
        } else if ((arg == "-v") || (arg == "--visualization")) {
            visualize = true;
            cout << "-->  Visualization of the Point Cloud enabled." <<endl;
        }
    }

    // Load the .ply file into a Point cloud:
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    if(!pcl::io::loadPLYFile (argv[1], *cloud)){
        std::cout << "--> File (.ply) loaded correctly." << std::endl;
    }else {
        std::cout << "--> Couldn't load file (.ply) " << std::endl;
    }

    if(visualize){
    // Visualize the Cloud:
    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer("Inital"));
    viewer->setBackgroundColor(0, 0, 0);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> scene_color (cloud, 0, 255, 0);
    viewer->addPointCloud<pcl::PointXYZ>(cloud, scene_color, "scene cloud");

          while (!viewer->wasStopped ())
        {
            viewer->spinOnce (100);
            boost::this_thread::sleep (boost::posix_time::microseconds (100000));
        }
    }

    // Save Point cloud to .pcd file:
    pcl::io::savePCDFileASCII ("Result.pcd", *cloud);
    std::cout << "--> Saved " << cloud->points.size () << " data points to Result.pcd." << std::endl<< std::endl;

    return (0);
}
