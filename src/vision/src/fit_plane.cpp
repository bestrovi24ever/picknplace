﻿#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

typedef pcl::PointXYZ PointT;
using namespace pcl::visualization;
static void show_usage(std::string name)
{
    std::cout << "Usage: " << "<Path to pcd file> <option(s)>"
              << "This program tries to fit a plane to find the table surface."
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-v,--visualization \tEnable point cloud visualization"
              << std::endl;
}

int main( int argc, char *argv [] ){

    cout << "--> Fitting a Plane to find the table surface. " <<std::endl;
    // Parameters to modify the behaviour of this program:
    int ransac_iteration = 100;
    float inliers_treshold = 0.06;
    float point_to_plane_minimum_distance = 0.01;
    cout << "--> Ransac Parameters:" << endl;
    cout << "----> Num of iterations: " << ransac_iteration << endl;
    cout << "----> Inliers treshold : " << inliers_treshold << endl;

    
    bool visualize=false;

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            show_usage(argv[0]);
            return 0;
        } else if ((arg == "-v") || (arg == "--visualization")) {
            visualize = true;
            cout << "--> Visualization of the Point Cloud enabled." <<endl;
        }
    }

    // All the objects needed
    pcl::PCDReader reader;
    pcl::PassThrough<PointT> pass;
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
    pcl::PCDWriter writer;
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    // Datasets
    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered1 (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals1 (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices);

    // Load the .pcd file into a Point cloud:
    if(!pcl::io::loadPCDFile(argv[1], *cloud)){
        std::cout << "--> File (.pcd) loaded correctly." << std::endl;
        std::cout << "----> PointCloud has: " << cloud->points.size () << " data points." << std::endl;
    }else {
        std::cout << "--> Couldn't load file (.pcd). " << std::endl;
    }

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);

    // Create the segmentation object for the planar model and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
    seg.setNormalDistanceWeight (0.1);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (ransac_iteration);
    seg.setDistanceThreshold (inliers_treshold);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);

    // Obtain the plane inliers and coefficients
    seg.segment (*inliers_plane, *coefficients_plane);
    std::cout << "--> Plane fitted. " <<std::endl;
    std::cout << "----> Plane coefficients: " << *coefficients_plane << std::endl;
   
    // Extract the planar inliers from the input cloud
    std::cout << "--> Removing plane inliers. " <<std::endl;
    extract.setInputCloud (cloud);
    extract.setIndices (inliers_plane);
    extract.setNegative (false);

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_filtered1);
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals);
    extract_normals.setIndices (inliers_plane);
    extract_normals.filter (*cloud_normals1);

    std::cout << "----> PointCloud after removing plane inliers has: " << cloud_filtered1->points.size () << " data points." << std::endl;

    if(visualize){
        {
            PCLVisualizer v("Plane inliers removal");
            v.addPointCloud<PointT>(cloud, PointCloudColorHandlerCustom<PointT>(cloud, 0, 255, 0), "original cloud");
            v.addPointCloud<PointT>(cloud_filtered1, PointCloudColorHandlerCustom<PointT>(cloud_filtered1, 255, 0, 0), "points under plane removed");
            v.spin();
        }
    }

    // Remove all the points under the plane:
    std::cout << "--> Removing points under the plane. " <<std::endl;
    pcl::PointIndices::Ptr points_under_plane (new pcl::PointIndices);
    for(int i = 0; i <cloud_filtered1->size() ;i++){

        float x = cloud_filtered1->at(i).x;
        float y = cloud_filtered1->at(i).y;
        float z = cloud_filtered1->at(i).z;

        float a =coefficients_plane->values[0];
        float b =coefficients_plane->values[1];
        float c =coefficients_plane->values[2];
        float d =coefficients_plane->values[3];

        // Distance point to plane:
        float Distance = (a*x+b*y+c*z+d)/(std::sqrt(a*a+b*b+c*c));

        // Distance < 0.0 to remove points under the plane, Distance > 0.1 this is done for later segmentation steps, we would like small clusters to be separated from each other
        if(Distance < point_to_plane_minimum_distance){
            points_under_plane->indices.push_back(i);
        }
    }

    //Remove points from Point cloud:
    extract.setInputCloud (cloud_filtered1);
    extract.setIndices (points_under_plane);
    extract.filter (*cloud_filtered2);
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals1);
    extract_normals.setIndices (points_under_plane);
    extract_normals.filter (*cloud_normals2);

    std::cout << "----> PointCloud after removing under-plane points has: " << cloud_filtered2->points.size () << " data points." << std::endl;

    if(visualize){
        // Show result:
        {
            PCLVisualizer v("Points under plane removal");
            v.addPointCloud<PointT>(cloud, PointCloudColorHandlerCustom<PointT>(cloud, 0, 255, 0), "original cloud");
            v.addPointCloud<PointT>(cloud_filtered2, PointCloudColorHandlerCustom<PointT>(cloud_filtered2, 255, 0, 0), "points under plane removed");
            v.spin();
        }
    }

    // Save Point cloud to .pcd file:
    pcl::io::savePCDFileASCII ("Result.pcd", *cloud_filtered2);
    std::cout << "--> Saved " << cloud_filtered2->points.size () << " data points to result.pcd." << std::endl<< std::endl;

    return (0);
}
