/***************************************************************************************
*    Code from the following sources has been used in this project:
*
*    Title: Filtering a PointCloud using a PassThrough filter
*    Availability: http://pointclouds.org/documentation/tutorials/passthrough.php
*
*    Title: Cylinder model segmentation
*    Availability: http://pointclouds.org/documentation/tutorials/cylinder_segmentation.php
*
*    Title: Euclidean Cluster Extraction
*    Availability: http://www.pointclouds.org/documentation/tutorials/cluster_extraction.php
*
***************************************************************************************/

#include "PreProcessing.h"

typedef pcl::PointXYZ PointT;
using namespace pcl::visualization;

PreProcessing::PreProcessing()
{
    //pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_xyz = Filter_XYZ()
}

PreProcessing::~PreProcessing()
{
}

pcl::PointCloud<pcl::PointXYZ>::Ptr PreProcessing::Filter_XYZ(pcl::PointCloud<pcl::PointXYZ>::Ptr &original_cloud, bool visualize, bool save_data)
{
    std::cout << "--> Applying Pass trough filter in XYZ:" << std::endl;
    // Parameters to modify the behaviour of this program:
    // Filter Treshold values:        X         Y          Z
    std::vector<float> lim = {-0.3, 0.1, -0.2, 0.1, 0, 0.7};

    // Variables declaration>
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass_z(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass_y(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass_x(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> original_cloud_color(original_cloud, 255, 0, 0);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_pass_z_color(cloud_pass_z, 0, 255, 0);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_pass_x_color(cloud_pass_x, 255, 0, 0);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_pass_y_color(cloud_pass_y, 0, 255, 0);

    if (visualize)
    {
        // Visualize the Original Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_original_PCL(new pcl::visualization::PCLVisualizer("Original Cloud"));
        viewer_original_PCL->setBackgroundColor(0, 0, 0);
        viewer_original_PCL->setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        viewer_original_PCL->addPointCloud<pcl::PointXYZ>(original_cloud, original_cloud_color, "Original Cloud");
    }

    //Filtering in Z:
    pcl::PassThrough<pcl::PointXYZ> pass_z;
    pass_z.setInputCloud(original_cloud);
    pass_z.setFilterFieldName("z");
    pass_z.setFilterLimits(lim[4], lim[5]);
    pass_z.filter(*cloud_pass_z);

    std::cout << "--> Filtering in Z, keeping points between: " << lim[4] << " and " << lim[5] << std::endl;
    std::cout << "----> Point cloud has " << cloud_pass_z->points.size() << " data points." << std::endl;

    if (visualize)
    {
        // Visualize the Filtered_Z_Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_pass_z(new pcl::visualization::PCLVisualizer("Filtered Z Cloud"));
        viewer_pass_z->setBackgroundColor(0, 0, 0);
        viewer_pass_z->setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        viewer_pass_z->addPointCloud<pcl::PointXYZ>(original_cloud, original_cloud_color, "Original Cloud");
        viewer_pass_z->addPointCloud<pcl::PointXYZ>(cloud_pass_z, cloud_pass_z_color, "Cloud Pass Z");
    }

    //Filtering in Y:
    pcl::PassThrough<pcl::PointXYZ> pass_y;
    pass_y.setInputCloud(cloud_pass_z);
    pass_y.setFilterFieldName("y");
    pass_y.setFilterLimits(lim[2], lim[3]);
    pass_y.filter(*cloud_pass_y);

    std::cout << "--> Filtering in Y, keeping points between: " << lim[2] << " and " << lim[3] << std::endl;
    std::cout << "----> Point cloud has " << cloud_pass_y->points.size() << " data points." << std::endl;

    if (visualize)
    {
        // Visualize the Filtered_Y_Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_pass_y(new pcl::visualization::PCLVisualizer("Filtered Y Cloud"));
        viewer_pass_y->setBackgroundColor(0, 0, 0);
        viewer_pass_y->setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        viewer_pass_y->addPointCloud<pcl::PointXYZ>(cloud_pass_z, PointCloudColorHandlerCustom<PointT>(cloud_pass_z, 255, 0, 0), "Cloud Pass Z");
        viewer_pass_y->addPointCloud<pcl::PointXYZ>(cloud_pass_y, PointCloudColorHandlerCustom<PointT>(cloud_pass_y, 0, 255, 0), "Cloud Pass Y");
    }

    //Filtering in X:
    pcl::PassThrough<pcl::PointXYZ> pass_x;
    pass_x.setInputCloud(cloud_pass_y);
    pass_x.setFilterFieldName("x");
    pass_x.setFilterLimits(lim[0], lim[1]);
    pass_x.filter(*cloud_pass_x);

    std::cout << "--> Filtering in X, keeping points between: " << lim[1] << " and " << lim[2] << std::endl;
    std::cout << "----> Point cloud has " << cloud_pass_x->points.size() << " data points." << std::endl;

    if (visualize)
    {
        // Visualize the Filtered_X_Cloud:
        pcl::visualization::PCLVisualizer::Ptr viewer_pass_x(new pcl::visualization::PCLVisualizer("Filtered X"));
        viewer_pass_x->setBackgroundColor(0, 0, 0);
        viewer_pass_x->setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        //viewer_pass_x->addPointCloud<pcl::PointXYZ>(cloud_pass_z, cloud_pass_z_color, "Cloud Pass Z");
        viewer_pass_x->addPointCloud<pcl::PointXYZ>(cloud_pass_y, PointCloudColorHandlerCustom<PointT>(cloud_pass_y, 255, 0, 0), "Cloud Pass Y");
        viewer_pass_x->addPointCloud<pcl::PointXYZ>(cloud_pass_x, PointCloudColorHandlerCustom<PointT>(cloud_pass_x, 0, 255, 0), "Cloud Pass X");
        viewer_pass_x->spinOnce();
    }

    // Save Point cloud to .pcd file:
    if (save_data)
    {
        std::string filename = "Number_of_points";
        PreProcessing::save_data("../Matlab/Vision/" + filename + ".m", original_cloud->points.size(), cloud_pass_z->points.size(), cloud_pass_y->points.size(), cloud_pass_x->points.size(), 0.0, 0.0);
    }

    return cloud_pass_x;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr PreProcessing::fit_plane(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, pcl::ModelCoefficients::Ptr coefficients_plane, bool visualize, bool save_data, string filename)
{
    std::cout << "--> Applying Plane Fitting:" << std::endl;
    // Parameters to modify the behaviour of this program:
    int ransac_iteration = 100;
    float inliers_treshold = 0.09;
    float point_to_plane_minimum_distance = 0.0;
    cout << "--> Ransac Parameters:" << endl;
    cout << "----> Num of iterations: " << ransac_iteration << endl;
    cout << "----> Inliers treshold : " << inliers_treshold << endl;

    // All the objects needed
    pcl::PassThrough<PointT> pass;
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>());

    // Datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered1(new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals1(new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered2(new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2(new pcl::PointCloud<pcl::Normal>);
    pcl::PointIndices::Ptr inliers_plane(new pcl::PointIndices);

    // Estimate point normals
    ne.setSearchMethod(tree);
    ne.setInputCloud(cloud);
    ne.setKSearch(50);
    ne.compute(*cloud_normals);

    // Create the segmentation object for the planar model and set all the parameters
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_NORMAL_PLANE);
    seg.setNormalDistanceWeight(0.1);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setMaxIterations(ransac_iteration);
    seg.setDistanceThreshold(inliers_treshold);
    seg.setInputCloud(cloud);
    seg.setInputNormals(cloud_normals);

    // Obtain the plane inliers and coefficients
    seg.segment(*inliers_plane, *coefficients_plane);
    std::cout << "--> Plane fitted. " << std::endl;
    std::cout << "----> Plane coefficients: " << *coefficients_plane << std::endl;

    // Extract the planar inliers from the input cloud
    std::cout << "--> Removing plane inliers. " << std::endl;
    extract.setInputCloud(cloud);
    extract.setIndices(inliers_plane);
    extract.setNegative(false);

    // Remove the planar inliers, extract the rest
    extract.setNegative(true);
    extract.filter(*cloud_filtered1);
    extract_normals.setNegative(true);
    extract_normals.setInputCloud(cloud_normals);
    extract_normals.setIndices(inliers_plane);
    extract_normals.filter(*cloud_normals1);

    std::cout << "----> PointCloud after removing plane inliers has: " << cloud_filtered1->points.size() << " data points." << std::endl;

    if (visualize)
    {
        PCLVisualizer v("Plane inliers removal");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        v.addPointCloud<PointT>(cloud, PointCloudColorHandlerCustom<PointT>(cloud, 255, 0, 0), "original cloud");
        v.addPointCloud<PointT>(cloud_filtered1, PointCloudColorHandlerCustom<PointT>(cloud_filtered1, 0, 255, 0), "points under plane removed");
        v.spinOnce();
    }

    // Plane coefficients:
    float a = coefficients_plane->values[0];
    float b = coefficients_plane->values[1];
    float c = coefficients_plane->values[2];
    float d = coefficients_plane->values[3];

    // Remove all the points under the plane:
    std::cout << "--> Removing points under the plane. " << std::endl;
    pcl::PointIndices::Ptr points_under_plane(new pcl::PointIndices);

    for (uint i = 0; i < cloud_filtered1->size(); i++)
    {

        float x = cloud_filtered1->at(i).x;
        float y = cloud_filtered1->at(i).y;
        float z = cloud_filtered1->at(i).z;

        // Distance point to plane:
        float Distance = (a * x + b * y + c * z + d) / (std::sqrt(a * a + b * b + c * c));

        // Distance < 0.0 to remove points under the plane, Distance > 0.1 this is done for later segmentation steps, we would like small clusters to be separated from each other
        if (Distance < point_to_plane_minimum_distance)
        {
            points_under_plane->indices.push_back(i);
        }
    }

    //Remove points from Point cloud:
    extract.setInputCloud(cloud_filtered1);
    extract.setIndices(points_under_plane);
    extract.filter(*cloud_filtered2);
    extract_normals.setNegative(true);
    extract_normals.setInputCloud(cloud_normals1);
    extract_normals.setIndices(points_under_plane);
    extract_normals.filter(*cloud_normals2);

    std::cout << "----> PointCloud after removing under-plane points has: " << cloud_filtered2->points.size() << " data points." << std::endl;

    if (visualize)
    {
        // Show result:
        PCLVisualizer v("Points under plane removal");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        v.addPointCloud<PointT>(cloud, PointCloudColorHandlerCustom<PointT>(cloud, 255, 0, 0), "original cloud");
        v.addPointCloud<PointT>(cloud_filtered2, PointCloudColorHandlerCustom<PointT>(cloud_filtered2, 0, 255, 0), "points under plane removed");
        v.spinOnce();
    }

    if (save_data)
    {
        PreProcessing::save_data("../Matlab/Vision/" + filename + "/Fit_plane_" + filename + ".m", a, b, c, d, 0.0, 0.0);
        filename = "Number_of_points";
        PreProcessing::save_data("../Matlab/Vision/" + filename + ".m", cloud->points.size(), cloud_filtered1->points.size(), cloud_filtered2->points.size(), 0.0, 0.0, 0.0);
    }

    return cloud_filtered2;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr PreProcessing::remove_small_clusters(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, bool visualize, bool save_data)
{
    std::cout << "--> Remove the small clusters:" << std::endl;
    // Parameters to modify the behaviour of this program:

    //float leaf_size = 0.0005f;
    float leaf_size = 0.001f;
    cout << "--> Downsampling Parameters:" << endl;
    cout << "----> Leaf size: " << leaf_size << endl;

    // Clustering Parameters:
    int min_cluster_size = 600;
    int max_cluster_size = 25000;
    float cluster_tolerance = 0.01; // cm

    cout << "--> Cluster Parameters:" << endl;
    cout << "----> Cluster size, Max: " << max_cluster_size << " , Min: " << min_cluster_size << endl;
    cout << "----> Cluster tolerance : " << cluster_tolerance << endl;

    // Load the .pcd file into a Point cloud:
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f(new pcl::PointCloud<pcl::PointXYZ>);

    if (visualize)
    {
        {
            PCLVisualizer v("Original Cloud");
            v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
            v.addPointCloud<pcl::PointXYZ>(cloud, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud, 0, 255, 0), "original cloud");
            v.spinOnce();
        }
    }

    // Create the filtering object: downsample the dataset using a leaf size of X cm
    std::cout << "--> Downsampling the scene with leaf size: " << leaf_size << endl;
    pcl::VoxelGrid<pcl::PointXYZ> vg;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
    vg.setInputCloud(cloud);
    vg.setLeafSize(leaf_size, leaf_size, leaf_size);
    vg.filter(*cloud_filtered);
    std::cout << "----> PointCloud after filtering has: " << cloud_filtered->points.size() << " data points." << std::endl;

    if (visualize)
    {
        PCLVisualizer v("Downsampled Pointcloud");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        //v.addPointCloud<pcl::PointXYZ>(cloud, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud, 255, 0, 0), "points under plane removed");
        v.addPointCloud<pcl::PointXYZ>(cloud_filtered, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud_filtered, 0, 255, 0), "original cloud");
        v.spinOnce();
    }

    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloud_filtered);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance(cluster_tolerance); // cm
    ec.setMinClusterSize(min_cluster_size);
    ec.setMaxClusterSize(max_cluster_size);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_filtered);
    ec.extract(cluster_indices);

    pcl::PointCloud<pcl::PointXYZ>::Ptr result(new pcl::PointCloud<pcl::PointXYZ>);

    int j = 0;
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);

        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
        {
            result->points.push_back(cloud_filtered->points[*pit]);
        }

        result->width = result->points.size();
        result->height = 1;
        result->is_dense = true;

        j++;
    }

    if (visualize)
    {
        PCLVisualizer v("Final Cloud Comparision");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        v.addPointCloud<pcl::PointXYZ>(cloud, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud, 255, 0, 0), "points under plane removed");
        v.addPointCloud<pcl::PointXYZ>(result, PointCloudColorHandlerCustom<pcl::PointXYZ>(result, 0, 255, 0), "original cloud");
        v.spinOnce();
    }

    if (visualize)
    {
        PCLVisualizer v("Final Cloud");
        v.setCameraPosition(-0.252034, 0.341164, -0.57032, -0.128131, 0.122338, 0.0488766, -0.0385026, -0.94455, -0.326102);
        //v.addPointCloud<pcl::PointXYZ>(cloud, PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud, 255, 0, 0), "points under plane removed");
        v.addPointCloud<pcl::PointXYZ>(result, PointCloudColorHandlerCustom<pcl::PointXYZ>(result, 0, 255, 0), "original cloud");
        v.spinOnce();
    }

    if (save_data)
    {
        std::string filename = "Number_of_points";
        PreProcessing::save_data("../Matlab/Vision/" + filename + ".m", cloud->points.size(), cloud_filtered->points.size(), result->points.size(), 0.0, 0.0, 0.0);
    }

    return result;
}

void PreProcessing::save_data(string filepath, double d1, double d2, double d3, double d4, double d5, double d6)
{
    std::ofstream file;
    //can't enable exception now because of gcc bug that raises ios_base::failure with useless message
    //file.exceptions(file.exceptions() | std::ios::failbit);
    file.open(filepath, std::ios::out | std::ios::app);
    if (file.fail())
        throw std::ios_base::failure(std::strerror(errno));

    //make sure write fails with exception if something is wrong
    file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file.seekp(0, ios::end);
    size_t size = file.tellp();
    if (size == 0)
    {
        cout << " File: " << filepath << " wasn't initialized, initializing now." << endl;
        file << " Data =[";
    }

    file << d1 << "," << d2 << "," << d3 << "," << d4 << "," << d5 << "," << d6 << ";"  << "\n";
}