﻿#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

typedef pcl::PointXYZ PointT;
using namespace pcl::visualization;

int main(int argc, char**argv) {

    if(argc < 2) {
        cout << "Usage: " << argv[0] << " <Point Cloud>" << endl;
        return 0;
    }

    // All the objects needed
    pcl::PCDReader reader;
    pcl::PassThrough<PointT> pass;
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
    pcl::PCDWriter writer;
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    // Datasets
    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
    //pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered1 (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals1 (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices);

    // Load
    pcl::io::loadPCDFile(argv[1], *cloud);
    std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);

    // Create the segmentation object for the planar model and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
    seg.setNormalDistanceWeight (0.1);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (100);
    seg.setDistanceThreshold (0.1);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);

    // Obtain the plane inliers and coefficients
    seg.segment (*inliers_plane, *coefficients_plane);
    std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;

    // Extract the planar inliers from the input cloud
    extract.setInputCloud (cloud);
    extract.setIndices (inliers_plane);
    extract.setNegative (false);

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_filtered1);
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals);
    extract_normals.setIndices (inliers_plane);
    extract_normals.filter (*cloud_normals1);

    std::cerr << "PointCloud after removing plane inliers has: " << cloud_filtered1->points.size () << " data points." << std::endl;

    {
        PCLVisualizer v("Points in plane removal");
        v.addPointCloud<PointT>(cloud, PointCloudColorHandlerCustom<PointT>(cloud, 0, 255, 0), "original cloud");
        v.addPointCloud<PointT>(cloud_filtered1, PointCloudColorHandlerCustom<PointT>(cloud_filtered1, 255, 0, 0), "points under plane removed");
        v.spin();
    }

    // Remove all the points under the plane:
    pcl::PointIndices::Ptr points_under_plane (new pcl::PointIndices);
    for(int i = 0; i <cloud_filtered1->size() ;i++){

        float x = cloud_filtered1->at(i).x;
        float y = cloud_filtered1->at(i).y;
        float z = cloud_filtered1->at(i).z;

        float a =coefficients_plane->values[0];
        float b =coefficients_plane->values[1];
        float c =coefficients_plane->values[2];
        float d =coefficients_plane->values[3];

        // Distance point to plane:
        float Distance = (a*x+b*y+c*z+d)/(std::sqrt(a*a+b*b+c*c));

        // Distance < 0.0 to remove points under the plane, Distance > 0.1 this is done for later segmentation steps, we would like small clusters to be separated from each other
        if(Distance < 0.0 || Distance > 0.1){
            points_under_plane->indices.push_back(i);
        }
    }

    //Remove points from Point cloud:
    extract.setInputCloud (cloud_filtered1);
    extract.setIndices (points_under_plane);
    extract.filter (*cloud_filtered2);
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals1);
    extract_normals.setIndices (points_under_plane);
    extract_normals.filter (*cloud_normals2);

    std::cerr << "PointCloud after removing under-plane points has: " << cloud_filtered2->points.size () << " data points." << std::endl;

    // Show result:
    {
        PCLVisualizer v("Points under plane removal");
        v.addPointCloud<PointT>(cloud, PointCloudColorHandlerCustom<PointT>(cloud, 0, 255, 0), "original cloud");
        v.addPointCloud<PointT>(cloud_filtered2, PointCloudColorHandlerCustom<PointT>(cloud_filtered2, 255, 0, 0), "points under plane removed");
        v.spin();
    }

    // Save Point cloud to .pcd file:
       pcl::io::savePCDFileASCII ("Result.pcd", *cloud_filtered2);
       std::cout << "Saved " << cloud_filtered2->points.size () << " data points to result.pcd." << std::endl;

    return (0);
}
