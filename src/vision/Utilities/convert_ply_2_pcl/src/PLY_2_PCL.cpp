﻿#include <iostream>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>

int main (int argc, char** argv)
{
    if (argc != 2) {
        std::cout << "To convert a mesh file (.ply) to Point cloud (.pcl):" << '\n';
        std::cout << "Usage:" << '\n';
        std::cout << argv[0] << "Filte2Convert.ply";
        return -1;
    }

    // Load the .ply file into a Point cloud:
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    if(!pcl::io::loadPLYFile (argv[1], *cloud)){
        std::cout << "File (.ply) loaded correctly" << std::endl;
    }else {
        std::cerr << "Couldn't load file (.ply) " << std::endl;
    }

    // Visualize the Cloud:
    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer("Inital"));
    viewer->setBackgroundColor(0, 0, 0);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> scene_color (cloud, 0, 255, 0);
    viewer->addPointCloud<pcl::PointXYZ>(cloud, scene_color, "scene cloud");

    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }

    // Save Point cloud to .pcd file:
    pcl::io::savePCDFileASCII ("Result.pcd", *cloud);
    std::cout << "Saved " << cloud->points.size () << " data points to test_pcd.pcd." << std::endl;

    return (0);
}
