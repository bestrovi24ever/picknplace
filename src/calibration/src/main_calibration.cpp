#include <ros/ros.h>
#include <rw/math/Q.hpp>
#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/models/WorkCell.hpp>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include "rw/rw.hpp"
#include <rw/kinematics/Frame.hpp>
#include <rw/math/Quaternion.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/VelocityScrew6D.hpp>
#include <rw/math/LinearAlgebra.hpp>
#include "caros/serial_device_si_proxy.h"
#include <vector>
#include "ros/package.h"
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "wsg_50_common/Status.h"
#include "wsg_50_common/Move.h"
#include "wsg_50_common/Conf.h"
#include "wsg_50_common/Incr.h"
#include "wsg_50_common/Cmd.h"
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include <iostream>
#include <thread>  // NOLINT(build/c++11)
#include <chrono>  // NOLINT(build/c++11)
#include <rw/invkin/InvKinSolver.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>
#include <fstream>
using namespace std;
using namespace rw::math;
using namespace rw::common;
using namespace rw::graphics;
using namespace rw::kinematics;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::sensor;
using namespace rw;
using rw::invkin::JacobianIKSolver;
using rw::kinematics::State;
using rw::models::Device;

std::vector<rw::math::Transform3D<double>> vec_camTMarker;
std::vector<rw::math::Transform3D<double>> vec_baseTMarker;
std::vector<rw::math::Transform3D<double>> vec_baseTcam;
class Gripper {

private:
    ros::NodeHandle n;
    ros::ServiceClient gripper_grasp_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/grasp");
    ros::ServiceClient gripper_move_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/move");
    ros::ServiceClient gripper_homing_client = n.serviceClient<std_srvs::Empty>("/wsg_50_driver/homing");

public:
    Gripper()
    {
        ros::spinOnce();
    }

    bool gripper_grasp(double width, double speed)
    {
        std::cout << "Grasping start" << std::endl;
        ros::spinOnce();
        wsg_50_common::Move grasp;
        grasp.request.width = width; //20.0
        grasp.request.speed = speed; //100.0
        gripper_grasp_client.call(grasp);
        std::cout << "Grasp end" << std::endl;
        return true;
    }
    bool gripper_move(double width, double speed)
    {
        std::cout << "Gripper move start" << std::endl;
        ros::spinOnce();
        wsg_50_common::Move move;
        move.request.width = width; //20.0
        move.request.speed = speed; //100.0
        gripper_move_client.call(move);
        std::cout << "Gripper move end" << std::endl;
        return true;
    }
    bool gripper_homing()
    {
        std::cout << "Gripper homing start" << std::endl;
        ros::spinOnce();
        std_srvs::Empty homing;
        gripper_homing_client.call(homing);
        std::cout << "Gripper homing end" << std::endl;
        return true;
    }

};

class URRobot {
    using Q = rw::math::Q;

private:
    ros::NodeHandle nh;
    rw::models::WorkCell::Ptr wc;
    rw::models::Device::Ptr device;
    rw::kinematics::State state;
    caros::SerialDeviceSIProxy* robot;
    Frame* TCP;
    Frame* Marker;
    Frame* gripper_frame;


public:
    URRobot()
    {
        auto packagePath = ros::package::getPath("ur_caros_example");
        wc = rw::loaders::WorkCellLoader::Factory::load("./WorkCell/Scene.wc.xml");
        device = wc->findDevice("UR5");
        state = wc->getDefaultState();
        robot = new caros::SerialDeviceSIProxy(nh, "caros_universalrobot");
        TCP = wc->findFrame("UR5.TCP");
        Marker = wc->findFrame("UR5.marker");
        gripper_frame = wc->findFrame("UR5.GG");
        // Wait for first state message, to make sure robot is ready
        ros::topic::waitForMessage<caros_control_msgs::RobotState>("/caros_universalrobot/caros_serial_device_service_interface/robot_state", nh);
        ros::spinOnce();
    }

    bool forceStart(const rw::math::Transform3D<> refToffset, const rw::math::Q selection, const rw::math::Wrench6D<> wrench_target, int type, const rw::math::Q limits)
    {
        ros::spinOnce();
        if (robot->moveForceModeStart(refToffset, selection, wrench_target, type, limits)) {
            ROS_INFO("Force was successfull");
            return true;
        } else {
            ROS_INFO("Force was NOT successfull");
            return false;
        }
    }

    bool forceStop()
    {
        ros::spinOnce();
        if (robot->moveForceModeStop()) {
            ROS_INFO("STOP was successfull");
            return true;
        } else {
            ROS_INFO("STOP was NOT successfull");
            return false;
        }
    }


    Q getQ()
    {
        ros::spinOnce();
        Q q = robot->getQ();
        device->setQ(q, state);
        return q;
    }

    bool setQ(Q q)
    {
        float speed = 0.7;
        if (robot->movePtp(q, speed)) {
            device->setQ(q,state);
            return true;
        } else
            return false;
    }

    rw::math::Wrench6D<> getTCP_force()
    {
        ros::spinOnce();
        rw::math::Wrench6D<> TCP_force = robot->getTCPForce();
        return TCP_force;
    }

    bool velQ(Q q) {
        if (robot->moveVelQ(q)) {
            device->setQ(q,state);
            return true;
        } else
            return false;
    }

    bool velT(rw::math::VelocityScrew6D<> v) {
        robot->moveVelT(v);
        return true;
    }

    bool startServoMode(Q q) {
        double time = 1.0;
        double lookahead_time = 0.1;
        double gain = 200;
        device->setQ(q,state);
        //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
        //rwhw::URRTDE ur_rtde("localhost");
        if (robot->moveServoQ(q, time, lookahead_time, gain)) {
            //if (ur_rtde.servoJ(q,1.0,1.0,time,lookahead_time,gain)) {
            device->setQ(q,state);
            return true;
        } else return false;
    }

    bool updateServoMode(Q q) {
        if (robot->moveServoUpdate(q)) {
            device->setQ(q,state);
            return true;
        } else return false;
    }

    bool stopServoMode() {
        if (robot->moveServoStop()) {
            return true;
        } else return false;
    }

    //center: center position of the spiral; length: number of circles; width: distance between spiral lines; resolution: number of waypoints per circle
    vector<rw::math::Vector3D<double>> calcSpiral(double length, double width, double resolution) {
        vector<rw::math::Vector3D<double>> spiralPos;
        rw::math::Vector3D<double> lastPos(0.0,0.0,0.0);
        rw::math::Vector3D<double> newPos;
        for (double i=0.0; i<=length*rw::math::Pi; i+=rw::math::Pi/resolution) {
            rw::math::Vector3D<double> pos2(width*i*cos(i), width*i*sin(i),0);
            if (i != 0.0) {
                newPos = lastPos-pos2;
                spiralPos.push_back(newPos);
                lastPos = pos2;
            }
        }
        return spiralPos;
    }


    //spiral with equidistant movements (not really good)
    vector<rw::math::Vector3D<double>> spiral(double spacing, double turns)
    {
        vector<rw::math::Vector3D<double>> sequence;
        double pi = 3.14159;
        double vertspacing = spacing;
        double horzspacing = spacing;
        double thetamax = turns*pi;
        double b = ((vertspacing/2)/pi);
        double smax = 0.5*b*thetamax*thetamax;
        double xi;
        double yi;
        double prev_xi = 0.0;
        double prev_yi = 0.0;
        for ( double s = 0.0; s < smax; s = s + horzspacing){
            double thetai = sqrt((2*s)/b);
            xi = b*thetai*cos(thetai);
            yi = b*thetai*sin(thetai);
            rw::math::Vector3D<double> deltaP( xi - prev_xi, yi - prev_yi, 0.0 );
            prev_xi = xi;
            prev_yi = yi;
            sequence.push_back(deltaP);
        }
        return sequence;
    }

    Q calcQ(rw::math::Vector3D<double> deltaPos) {
        rw::math::Rotation3D<double> rot(1,0,0,0,1,0,0,0,1);
        rw::math::Transform3D<double> delta_transf(deltaPos,rot);
        rw::math::VelocityScrew6D<double> dU = rw::math::VelocityScrew6D<>(delta_transf);
        //rw::math::Jacobian J = device->baseJframe(TCP, state);
        rw::math::Jacobian baseJend = device->baseJend(state);
        rw::math::Jacobian Jinv(baseJend.e().inverse());
        rw::math::Q deltaQ = Jinv*dU;
        rw::math::Q q_out = device->getQ(state) + deltaQ;
        return q_out;
    }

    std::vector<Q> inverseKinematics(const Transform3D<>& target)
    {
        JacobianIKSolver solver(device, state);
        ///Transform3D<> bidi = device->baseTend(state);
        std::cout<<"Target: " << target <<std::endl;
        std::vector<Q> solutions = solver.solve(target, state);
        std::cout<<"Number of solutions = " << solutions.size() <<std::endl;
        for(Q q : solutions) {
            std::cout<<"Solution = "<<q<<std::endl;
        }
        return solutions;
    }

    Transform3D<double> baseTOend()
    {
        return device->baseTend(state);
    }
    Transform3D<double> baseTOMarker()
    {
        return device->baseTframe(Marker,state);
    }

};
void callback(const ar_track_alvar_msgs::AlvarMarkers &msg)
{
    std::cout << msg.markers[0].pose.pose.position.x << " " << msg.markers[0].pose.pose.position.y << " " << msg.markers[0].pose.pose.position.z<< std::endl;
    rw::math::Quaternion<double> tempQ(msg.markers[0].pose.pose.orientation.x,msg.markers[0].pose.pose.orientation.y,msg.markers[0].pose.pose.orientation.z,msg.markers[0].pose.pose.orientation.w);
    rw::math::Vector3D<double> tempP(msg.markers[0].pose.pose.position.x,msg.markers[0].pose.pose.position.y,msg.markers[0].pose.pose.position.z);
    rw::math::Rotation3D<double> tempR = tempQ.toRotation3D();
    rw::math::Transform3D<double> tempT(tempP,tempR);
    vec_camTMarker.push_back(tempT);
    

}

int main(int argc, char** argv) {

    ros::init(argc, argv, "calibration_node");

    ros::NodeHandle nh;
    ros::Rate rate(10);// frequency of operation
    Gripper gripper;
    URRobot robot;
    // subscribe
    //ros::Subscriber sub = nh.subscribe("/ar_pose_marker", 1, callback);
    std::vector<rw::math::Q> q_all;
    //q_all.resize(11);
    //ofstream bTm,cTm,bTc;
    // Inital Q for gripping object: (1.570,-1.397,1.657,-1.833,-1.573,1.57)
    // Initial Q for marker testing: (1.57,-1.397,1.745,-0.351,1.57,1.57)


   // (0.435928,0.1459,0.041081,0.851281,0.0901422,-0.460050,0.235568);
   // (0.4386,-0.8265,0.2005,-0.2296,0.0832,0.8536,0.4601);
    rw::math::Q q_initial_grip(6,1.570,-1.397,1.657,-1.833,-1.573,1.57);
    rw::math::Q q_initial_marker(6,1.57,-1.397,1.745,-0.351,1.57,1.57);
     rw::math::Q q_0(6, 0,-1.5707,0,-1.5707,0,0);

    
     robot.setQ(q_0);
   

    robot.setQ(q_initial_grip);
    cout << q_initial_grip << endl;
    cout << robot.getQ() << endl;
    //Marker
    rw::math::Quaternion<double> tempQM(0.662711446759,-0.534743376708,-0.163114877701,0.498253545972);
    rw::math::Vector3D<double> tempPM(0.442091219126,0.138280083763,0.0424272378463);
    rw::math::Rotation3D<double> tempRM =  tempQM.toRotation3D();
    rw::math::Transform3D<double> tempTM(tempPM,tempRM);
    //Object
   // rw::math::Rotation3D<double> tempRO(0.920354,0.385124,-0.0680256,0.209203,-0.63178,-0.746383,0.209203,-0.63178,-0.746383);
    rw::math::Rotation3D<double> tempRO(1,0,0,0,1,0,0,0,1);
    rw::math::Vector3D<double> tempPO(0.103599,0.0282093,0.382005);
    rw::math::Transform3D<double> tempTO(tempPO,tempRO);
    rw::math::Transform3D<double> ObjectTcam = rw::math::inverse(tempTO);
    //BaseTCam
    rw::math::Quaternion<double> tempQ(-0.2383,0.1978,0.7710,0.5565);
    rw::math::Vector3D<double> tempP(0.3183,-0.8519,0.2657);
    rw::math::Rotation3D<double> tempR =  tempQ.toRotation3D();
    rw::math::Transform3D<double> tempT(tempP,tempR);
    std::cout << "Calibration movement sequence start" << std::endl;

    rw::math::Transform3D<double> transformation = tempT*ObjectTcam;
    cout << "Base To Object" << transformation << endl;
    std::vector<Q> listQ = robot.inverseKinematics(transformation);
    robot.setQ(listQ[0]);
    cout << "SetQ : "<< listQ[0] << endl;


    /// CALIBRATION MOVEMENT
    // rw::math::Q q_1(6, 5.279, -1.748, -1.676, 3.506, -1.51, -0.097);
      
    // //q0: Q[6]{5.282, -1.748, -2.766, 4.577, -1.507, -0.094}
    // rw::math::Q q_c_0(6, 5.282, -1.748, -2.766, 4.577, -1.507, -0.094);
    // q_all.push_back(q_c_0);
    // //q1: Q[6]{5.282, -1.748, -2.766, 4.577, -1.626, -0.094}
    // rw::math::Q q_c_1(6, 5.282, -1.748, -2.766, 4.577, -1.626, -0.094);
    // q_all.push_back(q_c_1);
    // //q2: Q[6]{5.282, -1.748, -2.766, 4.577, -1.704, -0.094}
    // rw::math::Q q_c_2(6, 5.282, -1.748, -2.766, 4.577, -1.704, -0.094);
    // q_all.push_back(q_c_2);
    // //q3: Q[6]{5.282, -1.748, -2.766, 4.577, -1.391, 0.016}
    // rw::math::Q q_c_3(6, 5.282, -1.748, -2.766, 4.577, -1.391, 0.016);
    // q_all.push_back(q_c_3);
    // //q4: Q[6]{5.276, -1.817, -2.679, 4.674, -1.501, 0.047}
    // rw::math::Q q_c_4(6, 5.276, -1.817, -2.679, 4.674, -1.501, 0.047);
    // q_all.push_back(q_c_4);
    // //q5: Q[6]{5.276, -1.817, -2.741, 4.727, -1.451, -0.683}
    // rw::math::Q q_c_5(6, 5.276, -1.817, -2.741, 4.727, -1.451, -0.683);
    // q_all.push_back(q_c_5);
    // //q6: Q[6]{5.276, -1.817, -2.748, 4.712, -1.466, 0.573}
    // rw::math::Q q_c_6(6, 5.276, -1.817, -2.748, 4.712, -1.466, 0.573);
    // q_all.push_back(q_c_6);
    // //q7: Q[6]{6.212, -1.87, -2.55, 4.746, -0.229, -0.141}
    // rw::math::Q q_c_7(6, 6.212, -1.87, -2.55, 4.746, -0.229, -0.141);
    // q_all.push_back(q_c_7);
    // //q8: Q[6]{6.125, -1.87, -2.531, 4.518, -0.489, -0.053}
    // rw::math::Q q_c_8(6, 6.125, -1.87, -2.531, 4.518, -0.489, -0.053);
    // q_all.push_back(q_c_8);
    // //q9: Q[6]{6.125, -1.87, -2.531, 4.762, -0.473, 0.417}
    // rw::math::Q q_c_9(6, 6.125, -1.87, -2.531, 4.762, -0.473, 0.417);
    // q_all.push_back(q_c_9);
    // //q10: Q[6]{6.194, -1.886, -2.271, 4.38, 0.182, -0.141}
    // rw::math::Q q_c_10(6, 6.194, -1.886, -2.271, 4.38, 0.182, -0.141);
    // q_all.push_back(q_c_10);





    //NEW QS WITHOUT GRIPPER
    // rw::math::Q q_c_0(6, 0.774, -1.419, 2.506, 4.928, 0.128, 0.285);
    // rw::math::Q q_c_1(6, 0.545, -1.372, 2.516, 4.004, -0.081, -1.657);
    // rw::math::Q q_c_2(6, 0.686, -1.034, 2.503, 3.214, 0.019, -1.654);
    // rw::math::Q q_c_3(6, 1.219, -1.278, 2.212, 3.763, -0.611, -2.209);
    // rw::math::Q q_c_4(6, 1.219, -1.103, 2.647, 3.684, 1.031, -1.541);

    // rw::math::Q q_c_5(6, 1.272, -0.971, 2.03, 4.395, 1.028, -1.548);
    // rw::math::Q q_c_6(6, 1.344, -1.122, 1.999, 5.082, 0.789, -2.738);
    // rw::math::Q q_c_7(6, 1.313, -1.125, 2.036, 5.041, 1.281, -2.945);
    // rw::math::Q q_c_8(6, 1.394, -0.912, 2.008, 4.107, 1.137, -2.588);
    // rw::math::Q q_c_9(6, 1.513, -0.93, 1.786, 4.643, 1.147, -3.196);

    // rw::math::Q q_c_10(6, 2.061, -0.836, 1.664, 4.812, 1.974, -3.196);
    // rw::math::Q q_c_11(6, 1.939, -0.874, 1.996, 3.963, 1.501, -3.192);
    // rw::math::Q q_c_12(6, 1.936, -1.181, 1.67, 6.156, 1.77, -3.192);
    // rw::math::Q q_c_13(6, 1.861, -1.247, 2.049, 5.235, 1.585, -2.343);
    // rw::math::Q q_c_14(6, 1.861, -1.256, 1.902, 6.159, 1.585, -2.343);

    // rw::math::Q q_c_15(6, 2.253, -1.256, 1.902, 6.156, 1.585, -2.343);
    // rw::math::Q q_c_16(6, 2.287, -1.156, 1.905, 5.802, 1.607, -3.017);
    // rw::math::Q q_c_17(6, 2.065, -1.479, 2.556, 4.962, 1.623, -3.02);
    // rw::math::Q q_c_18(6, 2.065, -1.56, 2.475, 6.153, 1.623, -3.105);
    // rw::math::Q q_c_19(6, 1.761, -1.566, 2.641, 4.853, 1.454, -3.697);

    // NEW Qs 3rd try
    // rw::math::Q q_c_0(6, 1.306, -1.081, 2.187, -1.388, 0.934, 0.163);
    // rw::math::Q q_c_1(6, 1.438, -1.021, 2.243, -1.814, 1.178, 0.508);
    // rw::math::Q q_c_2(6, 1.529, -0.968, 2.249, -2.071, 1.388, 0.811);
    // rw::math::Q q_c_3(6, 1.604, -1.056, 2.152, -1.485, 1.413, 1.476);
    // rw::math::Q q_c_4(6, 1.717, -1.1, 2.259, -1.507, 1.657, 1.476);
    // rw::math::Q q_c_5(6, 1.911, -1.046, 2.227, -1.617, 1.864, 1.288);
    // rw::math::Q q_c_6(6, 2.027, -0.946, 2.074, -1.723, 1.579, 0.993);
    // rw::math::Q q_c_7(6, 1.958, -0.789, 1.823, -2.021, 1.576, -0.279);
    // rw::math::Q q_c_8(6, 1.789, -0.899, 1.889, -1.692, 1.429, -0.73);
    // rw::math::Q q_c_9(6, 1.516, -1.087, 1.999, -1.1, 1.109, -1.454);
    // rw::math::Q q_c_10(6, 1.385, -1.184, 1.989, -0.329, 0.968, -1.889);
    // rw::math::Q q_c_11(6, 1.172, -1.122, 1.958, -0.122, 0.689, -2.265);
    // //q1: Q[6]{5.269, -2.061,-2.309,4.712,-1.306,0.57}
    
    // q_all.push_back(q_c_0);
    // //q2: Q[6]{5.495,-2.171,-1.961,4.464,-1.046,0.742}

    // q_all.push_back(q_c_1);
    // //q3 Q[6]{5.555,-2.177,-2.162,4.953,-1.046,1.638}

    // q_all.push_back(q_c_2);
  
    // q_all.push_back(q_c_4);
  
    // q_all.push_back(q_c_5);
   
    // q_all.push_back(q_c_6);

    // q_all.push_back(q_c_7);
  
    // q_all.push_back(q_c_8);

    // q_all.push_back(q_c_9);
  
    // q_all.push_back(q_c_10);
  
    // q_all.push_back(q_c_11);
    
    // // q_all.push_back(q_c_12);
  
    // // q_all.push_back(q_c_13);
  
    // // q_all.push_back(q_c_14);
   
    // // q_all.push_back(q_c_14);
  
    // // q_all.push_back(q_c_15);
   
    // // q_all.push_back(q_c_16);

    // // q_all.push_back(q_c_17);
    
    // // q_all.push_back(q_c_18);
    
    // // q_all.push_back(q_c_19);
    
   
    // //robot.setQ(q_1); //HOME
    // //robot.setQ(q_all[3]);
    // for(unsigned int i=0;i<q_all.size();i++)
    // {
        
    //     robot.setQ(q_all[i]);
    //     cout << "SetQ : "<< q_all[i] << endl;
    //     rw::math::Transform3D<double> transTemp = robot.baseTOMarker();
    //     vec_baseTMarker.push_back(transTemp);
    //     std::this_thread::sleep_for(std::chrono::milliseconds(15000));
    //     robot.setQ(q_all[i]);
    //     std::this_thread::sleep_for(std::chrono::milliseconds(15000));
    //     ros::spinOnce();
    //     rate.sleep();
    //     cout << "Transformation: " << vec_camTMarker[i] << endl;
    //     cout << "Transformation robot: " << vec_baseTMarker[i] << endl;
        
    // }
    // bTc.open("BaseTCamera.txt");
    // // cTm.open("CameraTMarker.txt");
    // // bTm.open("BaseTMarker.txt");
    
    // std::vector<std::vector<double>> posrot_list;
    // for(unsigned int j=0;j<vec_baseTMarker.size();j++)
    // {
    //     rw::math::Transform3D<double> MarkerTcam = rw::math::inverse(vec_camTMarker[j]);
    //     rw::math::Transform3D<double> BaseTcam = vec_baseTMarker[j]*MarkerTcam;
    //     vec_baseTcam.push_back(BaseTcam);
    //     rw::math::Rotation3D<double> tempR = BaseTcam.R();
    //     //rw::math::RPY<double> temprpy(tempR);
    //     rw::math::Quaternion<double> tempQ(tempR);
    //     rw::math::Vector3D<double> tempP = BaseTcam.P();
    //     std::vector<double> posrot;
    //     posrot.push_back(tempP[0]);
    //     posrot.push_back(tempP[1]);
    //     posrot.push_back(tempP[2]);
    //     posrot.push_back(tempQ.getQx());
    //     posrot.push_back(tempQ.getQy());
    //     posrot.push_back(tempQ.getQz());
    //     posrot.push_back(tempQ.getQw());
        
    //     posrot_list.push_back(posrot);

    //     cout << "Transformation Base to Camera: " << BaseTcam << endl;
    // }
    // // float SumX,SumY,SumZ,SumR,SumP,SumYaw;
    // // SumX = 0.0;
    // // SumY = 0.0;
    // // SumZ = 0.0;
    // // SumR = 0.0;
    // // SumP = 0.0;
    // // SumYaw = 0.0;
    // bTc << "data=[" ;
    // for(unsigned int l=0;l<posrot_list.size();l++)
    // {
    //     std::vector<double> tempV = posrot_list.at(l);
    //     bTc << tempV[0] << "," << tempV[1] << "," << tempV[2] << "," << tempV[3] << "," << tempV[4] << "," << tempV[5] << "," << tempV[6] << ";" << endl;
    
    //     // SumX += tempV[0];
    //     // SumY += tempV[1];
    //     // SumZ += tempV[2];
    //     // SumR += tempV[3];
    //     // SumP += tempV[4];
    //     // SumYaw += tempV[5];
    // }
    // bTc << "]" ;
    // // SumX = SumX/posrot_list.size();
    // // SumY = SumY/posrot_list.size();
    // // SumZ = SumZ/posrot_list.size();
    // // SumR = SumR/posrot_list.size();
    // // SumP = SumP/posrot_list.size();
    // // SumYaw = SumY/posrot_list.size();
    // // cout << SumX << " " << SumY << " " << SumZ << " " << SumR << " " << SumP << " " << SumYaw << endl;
    // bTc.close();
    // //cTm.close();
    // //bTm.close();
    return 0;
    
    

}