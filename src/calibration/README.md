## Calibration Package
* **main_calibration** : Runs the robot for a series of Q-confs, stops for specified time and stores marker pose at each Q, then calculates transformation from camera to base for each Q

## Running Calibration
Before starting:

* Make sure all directories are sourced in ALL terminals
```
source ~/catkin_ws/devel/setup.bash
source ./picknplace/devel/setup.bash

```
* Run calibration WITHOUT Gripper for better results.
* For connecting to robot VIA ethernet make sure IPV4 settings are same as described in /src/robotics/README.md

Run realsense camera node:
```
roslaunch realsense2_camera rs_rgbd.launch
```
Run AR tracker node for marker poses

```
roslaunch ar_track_alvar pr2_bundle.launch
```
Run caros node for robot connection:
```
roslaunch caros_universalrobot caros_universalrobot.launch device_ip:="192.168.100.1"
```
Run calibration node:
```
rosrun calibration calibration
```

## Ar_track_alvar Installation:

* [wiki ROS](http://wiki.ros.org/ar_track_alvar#ar_track_alvar.2BAC8-post-fuerte.Usecase_per_number_of_tags) - ar_track_alvar package.

Installation (Make sure that the version of ROS is correct): 

```
sudo apt-get install ros-melodic-ar-track-alvar
```

First, we need to modify the launch file:
```
cd /opt/ros/melodic/share/ar_track_alvar/launch
sudo gedit pr2_bundle.launch
```
Substitute the nodes which the ar_track_alvar node subscribes to.

From:

```
<arg name="cam_image_topic" default="/kinect_head/depth_registered/points" />
<arg name="cam_info_topic" default="/kinect_head/rgb/camera_info" />
```

To:

```
<arg name="cam_image_topic" default="/camera/depth_registered/points" />
<arg name="cam_info_topic" default="/camera/color/camera_info" />	
```
Set marker size:
```
<arg name="marker_size" default="5.3" />
```
Set reference frame:
```
<arg name="output_frame" default="camera_link" />
```
Set The bundle file (definition of the positions of each QR in the marker):

Copy the Marker.xml file to bundles folder:
```
sudo cp Utilities/Markers.xml  /opt/ros/melodic/share/ar_track_alvar/bundles/
```
In the launch file, set the correct file:
```
<arg name="bundle_files" default="$(find ar_track_alvar)/bundles/Marker.xml" />
```

Now we need to modify the rs_rgbd.launch file:

```
cd /catkin_ws/src/realsense/realsense2_camera/launch
gedit rs_rgbd.launch
```
In line 79, we need to change the option enable_pointcloud to **true**:
```
<arg name="enable_pointcloud"   default="true"/>
```
We need to change the resolution:
```
  <arg name="fisheye_width"       default="1280"/>
  <arg name="fisheye_height"      default="720"/>
```
```
  <arg name="depth_width"         default="1280"/>
  <arg name="depth_height"        default="720"/>
```
```
  <arg name="infra1_width"        default="1280"/>
  <arg name="infra1_height"       default="720"/>
```
```
  <arg name="infra2_width"        default="1280"/>
  <arg name="infra2_height"       default="720"/>
```
```
  <arg name="color_width"         default="1280"/>
  <arg name="color_height"        default="720"/>
```

## Running ar_track_alvar:

Launching the node:
```
roslaunch ar_track_alvar pr2_bundle.launch
```
Launching the Realsense Camera node:
```
roslaunch realsense2_camera rs_rgbd.launch
```
Now we can run rviz to visualize the result:
```
rosrun rviz rviz
```
* Set: Global options -> fixed frame -> camera_link
* Add -> By topic -> /camera/depth_registered/points/PointCloud2
* Add -> By topic -> /visualization_marker/Marker

The identified markers should appear on the image and in the world representation.

We can run a realsense visualization tool to see the frames related with the camera:
```
roslaunch realsense2_camera  view_d435_model.launch
```
Finally, the topic /ar_pose_marker publishes the estimated transformation from camera to marker.
```
rostopic echo /ar_pose_marker
```