
Run caros node for robot connection:
```
roslaunch caros_universalrobot caros_universalrobot.launch device_ip:="192.168.100.1"
```

## Servo and force mode patch
To apply this patch, save the fix_servo_bug.patch (found in ./Utilities) to your home folder (~/fix_servo_bug.patch). Then run the following:

```
cd ~/RobWork
patch -p0 -i ~/fix_servo_bug.patch
cd ~/RobWork/Build/RWHardware
cmake -DCMAKE_BUILD_TYPE=Release ../../RobWorkHardware
make -j4
cd ~/catkin_ws/
catkin_make --force-cmake or catkin build --force-cmake

```
## Damping configuration
First we need to modify the damping parameters:
```
cd RobWork/RobWorkHardware/ext/ur_rtde/scripts/
gedit ./rtde_control.script 

```
In line 278 we have to modify the values from:

```
#Initialize gain and damping for forcemode
force_mode_set_damping(0.5)
force_mode_set_gain_scaling(0.025)

```
to

```
# Initialize gain and damping for forcemode
force_mode_set_damping(0.25)
force_mode_set_gain_scaling(0.5)
```
Finnally, Execute the script damping_change.sh to update the damping parameters in RobWork:
```
./Utilities/damping_change.sh
```

## Ethernet configuration

Setting the correct configuration to connect with the UR:

```
Settings -> Network
               |-----> Wired -> Settings
                                    |-----> IPv4
                                            |-----> IPv4 Method -> Manual
                                            |-----> Adresses -> Adress: 192.168.100.51
                                            |-----> Adresses -> Netmask: 255.255.255.0
```

