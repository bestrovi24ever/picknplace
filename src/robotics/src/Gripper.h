#ifndef GRIPPER_H
#define GRIPPER_H

#include "wsg_50_common/Status.h"
#include "wsg_50_common/Move.h"
#include "wsg_50_common/Conf.h"
#include "wsg_50_common/Incr.h"
#include "wsg_50_common/Cmd.h"
#include "ros/package.h"
#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"

class Gripper {

private:
    ros::NodeHandle nh;
    ros::ServiceClient gripper_grasp_client;
    ros::ServiceClient gripper_move_client;
    ros::ServiceClient gripper_homing_client;

public:
    Gripper();
    ~Gripper();

    bool gripper_grasp(double width, double speed);
    bool gripper_move(double width, double speed);
    bool gripper_homing();

};

#endif 