#include "rw/rw.hpp"
#include "ros/package.h"
#include <ros/ros.h>
#include "Gripper.h"
#include "URRobot.h"
#include <geometry_msgs/TransformStamped.h>
#include "tf2_msgs/TFMessage.h"

using namespace std;
rw::math::Transform3D<double> tempTO;
int call_stuff = 1;

void callback(const tf2_msgs::TFMessage static_tf)
{
    cout << "entered callback" << endl;
    cout << static_tf.transforms.size() << endl;
    for(unsigned int i=0;i<static_tf.transforms.size();i++)
    {
        cout << static_tf.transforms[i].child_frame_id << endl;
        if(static_tf.transforms[i].child_frame_id=="Pose_for_robot")
        {
            cout << static_tf.transforms[i].transform.translation.x << " " << static_tf.transforms[i].transform.translation.y << " " << static_tf.transforms[i].transform.translation.z << endl;
            rw::math::Quaternion<double> tempQ(static_tf.transforms[i].transform.rotation.x,static_tf.transforms[i].transform.rotation.y,static_tf.transforms[i].transform.rotation.z,static_tf.transforms[i].transform.rotation.w);
            rw::math::Vector3D<double> tempP(static_tf.transforms[i].transform.translation.x,static_tf.transforms[i].transform.translation.y,static_tf.transforms[i].transform.translation.z);
            rw::math::Rotation3D<double> tempR =  tempQ.toRotation3D();
            tempTO.P() =tempP;
            tempTO.R() = tempR;
            call_stuff = 0;

        }
        else
        {
            cout << "WRONG : " << static_tf.transforms[i].transform.translation.x << " " << static_tf.transforms[i].transform.translation.y << " " << static_tf.transforms[i].transform.translation.z << endl;
        }
        
    }
    
    
}

bool horizontalSpiralSearch(URRobot robot, Gripper gripper) {

    //rw::math::Q q_hovering_platform(6, 2.033, -1.059, 1.416, -1.939, -1.573, -0.407);
    //rw::math::Q q_hovering_platform2(6, 1.986, -0.946, 1.181, -1.823, -1.576, -0.454);
    bool endstate = false;
    rw::math::Q q_random(robot.calcRandomPos());
    rw::math::Q q_home(6, 0,-1.5707,0,-1.5707,0,0);
    //rw::math::Q startingPosQ(6, 2.011, -0.999, 1.231, -1.808, -1.554, 0.429);
    //rw::math::Q q_random(startingPosQ);


    rw::math::Rotation3D<> rotation_force(1,0,0,0,1,0,0,0,1);
    rw::math::Vector3D<double> position_force(0.0,0.0,0.0);
    const rw::math::Transform3D<> refToffset(position_force,rotation_force);
    const rw::math::Q selection(6,0.0,0.0,1.0,0.0,0.0,0.0);
    rw::math::Wrench6D<> wrench_target(0.0,0.0,-40.0,0.0,0.0,0.0);
    int type = 2;
    const rw::math::Q limits(6,100.0,100.0,150.0,10.0,10.0,10.0);

    cout<<"Is it taking the correct changes?"<<endl;
    robot.moveUp(0.1,0.5);
    robot.setQ(q_random,0.5);
    
    wrench_target[2]=-50.0;

    robot.forceStart(refToffset, selection, wrench_target, type, limits);
    while (endstate == false) {
        while (robot.getTCP_force()[2] < 50) {
            cout<<robot.getTCP_force()<<endl;
        }
        endstate = true;
    }
    wrench_target[2]=0.0;
    robot.forceUpdate(wrench_target);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    robot.forceStop();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    if (robot.checkEndstate()) {
        cout<<"NO spiral necessary"<<endl;
        //robot.forceStop();
        gripper.gripper_homing();
        robot.moveUp(0.2,0.5);
        robot.setQ(q_home,0.5);
        robot.robotStop();
        return true;
    }
    
    //Calculate the spiral q-configurations movements
    //vector<rw::math::Vector3D<double>> seq = robot.calcSpiral(20.0,0.001,64);
    //12,0.001,0.0005
    vector<rw::math::Vector3D<double>> seq = robot.spiral(50,0.00065,0.0004);//rotations = 3, width = 0.001, resolution = 0.0005 (width/2)
    rw::math::Q q_spiral_init = robot.calcQ(seq.at(1));
    //robot.setQ(q_spiral_init);

    //-8.0
    wrench_target[2]=robot.user_force;
    robot.forceStart(refToffset, selection, wrench_target, type, limits);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    //0.05
    robot.startServoMode(q_spiral_init,robot.user_time);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));                   
    bool spiralEndstate = false;

    for (int i = 1; i < seq.size(); i++){
        rw::math::Q q_spi = robot.calcQ(seq.at(i));
        std::cout << "robot.getTCP_force()[2] " << robot.getTCP_force()[2] << std::endl;
        robot.updateServoMode(q_spi);
        //2
        for (int j=0; j<robot.user_time*40; j++) {
            robot.saveData();
            if (robot.getTCP_force()[2] < 0.8) {
                robot.stopServoMode();
                std::cout << "robot.forceStop() OVER THRESHOLD! " << std::endl;
                spiralEndstate = true;
                break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        if (spiralEndstate == true) break;
    }
    robot.forceStop();

    wrench_target[2] = -30.0;
    const rw::math::Q selection2(6,1.0,1.0,1.0,0.0,0.0,0.0);
    robot.forceStart(refToffset, selection2, wrench_target, type, limits);
    endstate = false;
    while (endstate == false) {
        while (robot.getTCP_force()[2] < 30) {
            //cout<<robot.getTCP_force()<<endl;
        }
        endstate = true;
    }

    wrench_target[2]=0.0;
    robot.forceUpdate(wrench_target);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    robot.forceStop();

    if (robot.checkEndstate()) {
        cout<<"Endstate successful"<<endl;
    }

    gripper.gripper_homing();
    robot.moveUp(0.2,0.5);
    robot.setQ(q_home,0.5);

    robot.writeToFile();
    robot.printMaxForce();

    return true;

}

bool Pick(URRobot robot, Gripper gripper) {

    rw::math::Q q_initial_grip(6,1.570,-1.397,1.657,-1.833,-1.573,1.57);
    rw::math::Q q_initial_marker(6,1.57,-1.397,1.745,-0.351,1.57,1.57);
    rw::math::Q q_0(6, 0,-1.5707,0,-1.5707,0,0);
    gripper.gripper_homing();
    
    robot.setQ(q_0,0.5);


    robot.setQ(q_initial_grip,0.5);
    cout << q_initial_grip << endl;
    cout <<"GetQ: "<< robot.getQ() << endl;
    //Object
    // rw::math::Rotation3D<double> tempRO(0.920354,0.385124,-0.0680256,0.209203,-0.63178,-0.746383,0.209203,-0.63178,-0.746383);
    // rw::math::Rotation3D<double> tempRO(1,0,0,0,1,0,0,0,1);
    // rw::math::Vector3D<double> tempPO(0.103599,0.0282093,0.382005);
    // rw::math::Transform3D<double> tempTO(tempPO,tempRO);
    // rw::math::Transform3D<double> ObjectTcam = rw::math::inverse(tempTO);
    //BaseTCam
    rw::math::Quaternion<double> tempQ(-0.2383,0.1978,0.7710,0.5565);
    rw::math::Vector3D<double> tempP(0.3183,-0.8519,0.2657);
    rw::math::Rotation3D<double> tempR =  tempQ.toRotation3D();
    rw::math::Transform3D<double> tempT(tempP,tempR);

    ///TRASFORMATION OF CAMERS INTERNAL FRAMES
    rw::math::RPY<double> R(1.57,-1.57,0);
    rw::math::Rotation3D<double> tempRC = R.toRotation3D();    
    rw::math::Vector3D<double> tempPC(0.015,0.0,0.0);
    rw::math::Transform3D<double> tempTC(tempPC,tempRC);



    ///CALCULATE TRANSFORMATIONBASE TO OBJECT FOR THE INVERSE KINEMATICS
    //rw::math::Transform3D<double> basTcam = robot.baseTOfingers();
    rw::math::Transform3D<double> BaseTObj = tempT*inverse(tempTC)*tempTO;
    cout << "Annoying message, remember to kill me!!!" << tempT*inverse(tempTC) <<endl;
    rw::math::Transform3D<double> hoverObj(BaseTObj);
    rw::math::Vector3D<double> deltaPos(0.0,0.0,0.05);
    hoverObj.P() += deltaPos;

    bool success = false;
    int i = 0;
    while (!success && i < 20) {
        std::vector<rw::math::Q> hoverQ = robot.inverseKinematics(hoverObj);
        ///CALCULATE INVERSE KINEMATICS USING GRIPPER FINGERS AS END OF THE DEVICE
        std::vector<rw::math::Q> listQ = robot.inverseKinematics(BaseTObj);

        if (listQ.size() != 0){
            success = robot.setQ(hoverQ.at(0),0.5);
            std::cout << "Move to solution " << std::endl;
            success = robot.setQ(listQ.at(0),0.5);
        }
        else{
            std::cout << "No solutions " << std::endl;
        }
        i++;
    }
    if (i >= 20) {
        throw "No possible solution with inverse Kinematics found";
        return false;
    }

    //Close the gripper to grasp the object
    gripper.gripper_grasp(35.0, 100.0); //Disabled for testing calbration grasping error
    // std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    //gripper.gripper_homing();

    //robot.setQ(q_initial_grip,0.5);
    return true;
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "robotics_node");
    cout << "Start Program" << endl;
    Gripper gripper;
    cout << "Initalize gripper" << endl;
    URRobot robot;
    cout << "Initialize robot" << endl;

    //Picking without Vision
    bool robotic_test = false;

    if (robotic_test) {
        cout << "Enter force for spiral (positive value): ";
        //cin >> robot.user_force;
        robot.user_force = 10;
        robot.user_force = robot.user_force*(-1);
        cout << "Enter time for servo mode: ";
        //cin >> robot.user_time;
        robot.user_time = 0.05;
        //cout << "Filename for force data: ";
        //cin >> robot.user_force_file;
        robot.user_force_file = "Force_data_f10v2rand_";
        //cout << "Filename for spiral data: ";
        //cin >> robot.user_spiral_file;
        robot.user_spiral_file = "Position_data_f10v2rand_";
    }
    else {
        robot.user_force = -10.0;
        robot.user_time = 0.05;
        robot.user_force_file = "Force_completeTest_f10v2_";
        robot.user_spiral_file = "Position_completeTest_f10v2_";
    }

    ros::NodeHandle nh;
    ros::Rate rate(30);
    cout << "start robotics node" << endl;
    
    if (robotic_test == false) {
        ros::Subscriber sub = nh.subscribe ("/tf_static", 1, callback);
        cout << "subscribed" << endl;
            
        while(ros::ok() && call_stuff==1)
        {
            ros::spinOnce();
            rate.sleep();
        }
        sub.shutdown();
    }
    try {
        if (robotic_test) {
            cout<<"Picking without Vision"<<endl;
            // rw::math::Q q_initial_grip(6,1.570,-1.397,1.657,-1.833,-1.573,1.57);
            // robot.setQ(q_initial_grip,0.5);
            // Q[6]{1.181, -1.159, 1.482, -1.859, -1.569, -0.414}
            rw::math::Q q_hover(6,1.181, -1.159, 1.482, -1.859, -1.569, -0.414);
            robot.setQ(q_hover,0.5);
            rw::math::Q q_pick(6,1.181, -1.115, 1.554, -1.974, -1.57, -0.414);
            robot.setQ(q_pick,0.5);
            gripper.gripper_grasp(35,100);
        }
        else {
            cout<<"Picking using Vision"<<endl;
            Pick(robot, gripper);
        }

        horizontalSpiralSearch(robot, gripper);

        return 0;
    }
    catch (const char* msg) {
        cerr<<msg<<endl;
        robot.robotStop();
    }
}
