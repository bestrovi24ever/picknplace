#include "Gripper.h"

Gripper::Gripper()
{
    gripper_grasp_client = nh.serviceClient<wsg_50_common::Move>("/wsg_50_driver/grasp");
    gripper_move_client = nh.serviceClient<wsg_50_common::Move>("/wsg_50_driver/move");
    gripper_homing_client = nh.serviceClient<std_srvs::Empty>("/wsg_50_driver/homing");
}

Gripper::~Gripper()
{

}

bool Gripper::gripper_grasp(double width, double speed)
{
    std::cout << "Grasping start" << std::endl;
    ros::spinOnce();
    wsg_50_common::Move grasp;
    grasp.request.width = width; //20.0
    grasp.request.speed = speed; //100.0
    //gripper_grasp_client.call(grasp);
    if (gripper_grasp_client.call(grasp) == false) {
        throw "Gripper connection error";
        return false;
    }
    if (grasp.response.error == 255) {
            gripper_homing();
            throw "Gripper grasp error";
            return false;
    }
    std::cout << "Grasp end" << std::endl;
    return true;
}

bool Gripper::gripper_move(double width, double speed)
{
    std::cout << "Gripper move start" << std::endl;
    ros::spinOnce();
    wsg_50_common::Move move;
    move.request.width = width; //20.0
    move.request.speed = speed; //100.0
    if (gripper_move_client.call(move) == false) {
        throw "Gripper connection error";
        return false;
    }
    if (move.response.error == 255) {
            gripper_homing();
            throw "Gripper move error";
            return false;
    }
    std::cout << "Gripper move end" << std::endl;
    return true;
}
bool Gripper::gripper_homing()
{
    std::cout << "Gripper homing start" << std::endl;
    ros::spinOnce();
    std_srvs::Empty homing;
    if (gripper_homing_client.call(homing) == false) {
        throw "Gripper homing error";
        return false;
    }
    std::cout << "Gripper homing end" << std::endl;
    return true;
}