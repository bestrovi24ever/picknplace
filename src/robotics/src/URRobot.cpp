#include "URRobot.h"

using namespace std;
using namespace rw::math;
using namespace rw::common;
using namespace rw::graphics;
using namespace rw::kinematics;

using namespace rw::loaders;
using namespace rw::models;
using namespace rw::sensor;
using namespace rw;
using rw::invkin::JacobianIKSolver;
using rw::kinematics::State;
using rw::models::Device;
using namespace rw::proximity;
using namespace rwlibs::proximitystrategies;

URRobot::URRobot()
{
    string packagePath = ros::package::getPath("robotics");
    cout << "got package path" << endl;
    wc = rw::loaders::WorkCellLoader::Factory::load("./WorkCell/Scene.wc.xml");
    device = wc->findDevice("UR5");
    state = wc->getDefaultState();
    cout << "got workcell stuff" << endl;
    robot = new caros::SerialDeviceSIProxy(nh, "caros_universalrobot");
    cout << "got caros node" << endl;
    TCP = wc->findFrame("UR5.TCP");
    cout << "got tcp" << endl;
    gripper_frame = wc->findFrame("UR5.GG");
    cout << "got GG" << endl;
    detector = new CollisionDetector(wc, ProximityStrategyFactory::makeDefaultCollisionStrategy());
    // Wait for first state message, to make sure robot is ready
    ros::topic::waitForMessage<caros_control_msgs::RobotState>("/caros_universalrobot/caros_serial_device_service_interface/robot_state", nh);

    ros::spinOnce();
    cout << "got Caros robot state" << endl;
}

URRobot::~URRobot()
{

}

bool URRobot::forceStart(const rw::math::Transform3D<> refToffset, const rw::math::Q selection, const rw::math::Wrench6D<> wrench_target, int type, const rw::math::Q limits)
{
    ros::spinOnce();
    if (robot->moveForceModeStart(refToffset, selection, wrench_target, type, limits)) {
        ROS_INFO("Force was successfull");
        return true;
    } else {
        ROS_INFO("Force was NOT successfull");
        return false;
    }
}
bool URRobot::forceUpdate(const rw::math::Wrench6D<>& wrench_target) {
    ros::spinOnce();
    if (robot->moveForceModeUpdate(wrench_target)) {
        ROS_INFO("Force update was successfull");
        return true;
    } else {
        ROS_INFO("Force update was NOT successfull");
        return false;
    }
}

bool URRobot::forceStop()
{
    ros::spinOnce();
    if (robot->moveForceModeStop()) {
        ROS_INFO("STOP was successfull");
        return true;
    } else {
        ROS_INFO("STOP was NOT successfull");
        return false;
    }
}

Q URRobot::getQ()
{
    ros::spinOnce();
    Q q = robot->getQ();
    device->setQ(q, state);
    return q;
}

bool URRobot::setQ(Q q, float speed)
{
     if (!checkCollisions(q)){
         cerr << "Configuration in collision: " << q << endl;
        return false;
     }
         
    //float speed = speedQ;
    if (robot->movePtp(q, speed)) {
        device->setQ(q,state);
        return true;
    } else
        return false;

    
}

bool URRobot::setQ(Transform3D<>& target, float speed)
{
    //float speed = speedQ;
    if (robot->movePtpT(target, speed)) {
        device->setQ(URRobot::getQ(),state);
        return true;
    } else
        return false;
}

rw::math::Wrench6D<> URRobot::getTCP_force()
{
    ros::spinOnce();
    rw::math::Wrench6D<> TCP_force = robot->getTCPForce();
    return TCP_force;
}

ros::Time URRobot::getTime_Stamp()
{
    ros::spinOnce();
    ros::Time timeStamp = robot->getTimeStamp();
    return timeStamp;
}

bool URRobot::velQ(Q q) {
    if (robot->moveVelQ(q)) {
        device->setQ(q,state);
        return true;
    } else
        return false;
}

bool URRobot::velT(rw::math::VelocityScrew6D<> v) {
    robot->moveVelT(v);
    return true;
}

bool URRobot::startServoMode(Q q, double _time) {
    double time = _time;
    double lookahead_time = 0.1;
    double gain = 200;
    device->setQ(q,state);
    //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    //rwhw::URRTDE ur_rtde("localhost");
    if (robot->moveServoQ(q, time, lookahead_time, gain)) {
        //if (ur_rtde.servoJ(q,1.0,1.0,time,lookahead_time,gain)) {
        device->setQ(q,state);
        return true;
    } else return false;
}

bool URRobot::updateServoMode(Q q) {
    if (robot->moveServoUpdate(q)) {
        device->setQ(q,state);
        return true;
    } else return false;
}

bool URRobot::stopServoMode() {
    if (robot->moveServoStop()) {
        return true;
    } else return false;
}

bool URRobot::robotStop() {
    if (robot->stop()) {
        return true;
    } else return false;
}

vector<rw::math::Vector3D<double>> URRobot::calcSpiral(double length, double width, double resolution) {
    vector<rw::math::Vector3D<double>> spiralPos;
    rw::math::Vector3D<double> lastPos(0.0,0.0,0.0);
    rw::math::Vector3D<double> newPos;
    for (double i=0.0; i<=length*rw::math::Pi; i+=rw::math::Pi/resolution) {
        rw::math::Vector3D<double> pos2(width*i*cos(i), width*i*sin(i),0);
        if (i != 0.0) {
            newPos = lastPos-pos2;
            spiralPos.push_back(newPos);
            lastPos = pos2;
        }
    }
    return spiralPos;
}

vector<rw::math::Vector3D<double>> URRobot::spiral(double rotations, double width, double resolution) {
//rotations = 3, width = 0.001, resolution = 0.0005 (width/2)
    vector<rw::math::Vector3D<double>> spiralPos;
    rw::math::Vector3D<double> lastPos(0.0,0.0,0.0);
    rw::math::Vector3D<double> newDisp;
    for (double i=0.0; i<=rotations*2*rw::math::Pi; i+=0.01) {
        rw::math::Vector3D<double> newPos(width*i*cos(i), width*i*sin(i),0);
        double dist = sqrt((lastPos[0]-newPos[0])*(lastPos[0]-newPos[0])+(lastPos[1]-newPos[1])*(lastPos[1]-newPos[1]));
        if (dist>=resolution) {
            newDisp = lastPos-newPos;
            spiralPos.push_back(newDisp);
            lastPos = newPos;
        }
    }
    return spiralPos;
}

Q URRobot::calcQ(rw::math::Vector3D<double> deltaPos) {
    rw::math::Rotation3D<double> rot(1,0,0,0,1,0,0,0,1);
    rw::math::Transform3D<double> delta_transf(deltaPos,rot);
    rw::math::VelocityScrew6D<double> dU = rw::math::VelocityScrew6D<>(delta_transf);
    //rw::math::Jacobian J = device->baseJframe(TCP, state);
    rw::math::Jacobian baseJend = device->baseJend(state);
    rw::math::Jacobian Jinv(baseJend.e().inverse());
    rw::math::Q deltaQ = Jinv*dU;
    rw::math::Q q_out = device->getQ(state) + deltaQ;
    return q_out;
}

std::vector<Q> URRobot::inverseKinematics(const Transform3D<>& target)
{
    JacobianIKSolver solver(device,gripper_frame, state);
    ///Transform3D<> bidi = device->baseTend(state);
    std::cout<<"Target: " << target <<std::endl;
    std::vector<Q> solutions = solver.solve(target, state);
    std::cout<<"Number of solutions = " << solutions.size() <<std::endl;
    for(Q q : solutions) {
        std::cout<<"Solution = "<<q<<std::endl;
    }
    return solutions;
}

rw::math::Q URRobot::calcRandomPos()
{
    srand(time(NULL));
    rw::math::Q startingPosQ(6, 2.011, -0.999, 1.231, -1.808, -1.554, 0.429);
    //Q[6]{2.011, -0.999, 1.231, -1.808, -1.554, 0.429}
    device->setQ(startingPosQ,state);
    
    Transform3D<double> startingPosT = device->baseTframe(gripper_frame,state);
    std::cout << "Starting position transformation: " << startingPosT << std::endl;
    double dX = (((double)(rand() % 101))/1000)-0.05;
    double dY = (((double)(rand() % 101))/1000)-0.05;
    // for (int i=0; i<30; i++) {
    //     dX = (((double)(rand() % 101))/1000)-0.05;
    //     dY = (((double)(rand() % 101))/1000)-0.05;
    //     cout<<dX<<", "<<dY<<endl;
    // }
    rw::math::Vector3D<double> deltaPos(dX,dY,0); // for random postions
    //rw::math::Vector3D<double> deltaPos(0.02,0.02,0); //used for different speed and forces
    
    std::cout << "New position: " << deltaPos << std::endl;
    Transform3D<double> newPosT(startingPosT);
    newPosT.P() += deltaPos;
    std::vector<Q> newPosQ = inverseKinematics(newPosT);

    device->setQ(URRobot::getQ(), state);
    return newPosQ.at(0);
}

Transform3D<> URRobot::getCurrentT()
{
    device->setQ(URRobot::getQ(),state);   
    Transform3D<double> PosT = device->baseTframe(gripper_frame,state);
    //std::cout << "Final position transformation: " << PosT << std::endl;
    return PosT;
}

bool URRobot::moveUp(double moveZ, double speed)
{
    bool success = false;
    int i=0;
    device->setQ(URRobot::getQ(),state);
    Transform3D<double> PosT = device->baseTframe(gripper_frame,state);
    rw::math::Vector3D<double> deltaPos(0,0,moveZ);
    PosT.P() += deltaPos;
    while (success==false && i<20) {
        std::vector<Q> newPosQ = inverseKinematics(PosT);
        success = URRobot::setQ(newPosQ.at(0), speed);
        i++;
    }
    if (i >=20) {
        throw "No possible solution with inverse Kinematics found";
        return false;
    }
    return true;
}

bool URRobot::checkEndstate()
{
    device->setQ(URRobot::getQ(),state);
    Transform3D<double> PosT = device->baseTframe(gripper_frame,state);
    cout<<"Postion correct?:"<<PosT.P()[2]<<endl;
    if (PosT.P()[2] <= 0.0) {
        return true;
    }
    return false;
}

Transform3D<double> URRobot::baseTOend()
{
    return device->baseTend(state);
}


bool  URRobot::checkCollisions(const Q &q) {
    State testState;
    CollisionDetector::QueryResult data;
    bool colFrom;

    testState = state;
    device->setQ(q,testState);
    // Check if the configurations is coliding.
    colFrom = detector->inCollision(testState,&data);

    if (colFrom) {
        cerr << "Configuration in collision: " << q << endl;
        cerr << "Colliding frames: " << endl;
        FramePairSet fps = data.collidingFrames;
        for (FramePairSet::iterator it = fps.begin(); it != fps.end(); it++) {
            cerr << (*it).first->getName() << " " << (*it).second->getName() << endl;
        }
        return false;
    }
    return true;
}

bool URRobot::saveData()
{
    //Timestamp & Force
    ros::Time timestamp = URRobot::getTime_Stamp();
    rw::math::Wrench6D<> force = URRobot::getTCP_force();
    force_vec.push_back(force);
    double z_force = force[2];
    boost::posix_time::ptime my_posix_time = timestamp.toBoost();
    std::string iso_time_str = boost::posix_time::to_iso_extended_string(my_posix_time);

    //Calculating timestamp to seconds
    double hour = atof(iso_time_str.substr(11,2).c_str());
    double minute = atof(iso_time_str.substr(14,2).c_str());
    double seconds = atof(iso_time_str.substr(17,9).c_str());
    seconds += minute*60.0 + hour*60.0*60.0;

    vector<double> time_force;
    time_force.push_back(seconds);
    time_force.push_back(z_force);
    force_data.push_back(time_force);
    //*force_file << to_string(seconds)<<";";
    //*force_file << to_string(z_force)<<"\n";

    //Vector3D<double> currPos = getCurrentT().P();
    ros::spinOnce();
    spiral_q.push_back(robot->getQ());
    // vector<double> pos;
    // pos.push_back(currPos[0]);
    // pos.push_back(currPos[1]);
    // pos.push_back(currPos[2]);
    // spiral_data.push_back(pos);
    // *spiral_file << to_string(currPos[0])<<";";
    // *spiral_file << to_string(currPos[1])<<";";
    // *spiral_file << to_string(currPos[2])<<"\n";
    return true;
}

bool URRobot::writeToFile() 
{
    //File saving section
    force_file = new ofstream("./src/robotics/testdata/"+this->user_force_file+".txt");
    //force_file->open("example.txt",ios::in);
    *force_file << "Time;Z-Force\n";

    for(unsigned int i=0; i<force_data.size();i++) {
        *force_file << to_string(force_data[i][0])<<";";
        *force_file << to_string(force_data[i][1])<<"\n";
    }

    force_file->close();

    spiral_file = new ofstream("./src/robotics/testdata/"+this->user_spiral_file+".txt");
    *spiral_file << "x;y;z\n";
    for(unsigned int i=0; i<spiral_q.size(); i++) {
        device->setQ(spiral_q[i],state);
        Transform3D<double> PosT = device->baseTframe(gripper_frame,state);
        *spiral_file << to_string(PosT.P()[0])<<";";
        *spiral_file << to_string(PosT.P()[1])<<";";
        *spiral_file << to_string(PosT.P()[2])<<"\n";
    }
    spiral_file->close();

    // spiral_file = new ofstream("./src/robotics/testdata/spiral_data.txt");
    // *spiral_file << "x;y;z\n";
    // for(int i=0; i<force_data.size();i++) {
    //     *spiral_file << to_string(spiral_data[i][0])<<";";
    //     *spiral_file << to_string(spiral_data[i][1])<<";";
    //     *spiral_file << to_string(spiral_data[i][2])<<"\n";
    // }
    

    return true;
}

bool URRobot::printMaxForce()
{
    double force_max = 0;
    double curr_force = 0;

    for (unsigned int i=0; i<force_vec.size(); i++) {
        curr_force = sqrt(pow(force_vec[i][0],2) + pow(force_vec[i][1],2));
        if (curr_force > force_max) force_max = curr_force;
    }

    cout<<"Maximum force during spiral execution: "<<force_max<<endl;

    return true;
}

