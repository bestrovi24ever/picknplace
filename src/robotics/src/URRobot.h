#ifndef URRobot_H
#define URRobot_H

#include "rw/rw.hpp"
#include <rw/kinematics/Frame.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/VelocityScrew6D.hpp>
#include <rw/math/LinearAlgebra.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include "caros/serial_device_si_proxy.h"
//#include "caros_universalrobot/UrServiceServoQ.h"
//#include "caros_universalrobot/UrServiceServoUpdate.h"
#include "ros/package.h"
#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float32.h"
#include <time.h>
#include "std_msgs/Bool.h"
#include <iostream>
#include <fstream>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <thread>  // NOLINT(build/c++11)
#include <chrono>  // NOLINT(build/c++11)
#include <rw/invkin/InvKinSolver.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>

using namespace std;
using namespace rw::math;
using namespace rw::common;
using namespace rw::graphics;
using namespace rw::kinematics;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::sensor;
using namespace rw;
using rw::invkin::JacobianIKSolver;
using rw::kinematics::State;
using rw::models::Device;
using namespace rw::proximity;
using namespace rwlibs::proximitystrategies;

class URRobot{
    private:
        

    public:
        ros::NodeHandle nh;
        rw::models::WorkCell::Ptr wc;
        rw::models::Device::Ptr device;
        rw::kinematics::State state;
        caros::SerialDeviceSIProxy* robot;
        CollisionDetector* detector;
        Frame* TCP;
        Frame* gripper_frame;
        ofstream* force_file;
        ofstream* spiral_file;
        vector<vector<double>> force_data;
        vector<rw::math::Wrench6D<>> force_vec;
        vector<Q> spiral_q;
        //vector<vector<double>> spiral_data;
        double user_force;
        double user_time;
        string user_force_file;
        string user_spiral_file;

        URRobot();
        ~URRobot();

        bool forceStart(const rw::math::Transform3D<> refToffset, const rw::math::Q selection, const rw::math::Wrench6D<> wrench_target, int type, const rw::math::Q limits);
        bool forceUpdate(const rw::math::Wrench6D<>& wrench_target);
        bool forceStop();

        Q getQ();
        bool setQ(Q q, float speed);
        bool setQ(Transform3D<>& target, float speed);

        rw::math::Wrench6D<> getTCP_force();
        ros::Time getTime_Stamp();
    
        bool velQ(Q q);

        bool velT(rw::math::VelocityScrew6D<> v);
        bool startServoMode(Q q, double _time);
        bool updateServoMode(Q q);
        bool stopServoMode();

        bool robotStop();
        bool checkCollisions(const Q &q);

        rw::math::Q calcRandomPos();
        Transform3D<> getCurrentT();
        bool moveUp(double moveZ, double speed);
        bool checkEndstate();

        bool saveData();
        bool writeToFile();
        bool printMaxForce();

        //center: center position of the spiral; length: number of circles; width: distance between spiral lines; resolution: number of waypoints per circle
        vector<rw::math::Vector3D<double>> calcSpiral(double length, double width, double resolution);


        //spiral with equidistant movements (not really good)
        vector<rw::math::Vector3D<double>> spiral(double rotations, double width, double resolution);

        Q calcQ(rw::math::Vector3D<double> deltaPos);

        std::vector<Q> inverseKinematics(const Transform3D<>& target);

        Transform3D<double> baseTOend();
};
#endif