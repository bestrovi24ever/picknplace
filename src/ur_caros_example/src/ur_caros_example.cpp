#include "rw/rw.hpp"
#include <rw/kinematics/Frame.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/VelocityScrew6D.hpp>
#include <rw/math/LinearAlgebra.hpp>
#include "caros/serial_device_si_proxy.h"
//#include "caros_universalrobot/UrServiceServoQ.h"
//#include "caros_universalrobot/UrServiceServoUpdate.h"
#include "ros/package.h"
#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "wsg_50_common/Status.h"
#include "wsg_50_common/Move.h"
#include "wsg_50_common/Conf.h"
#include "wsg_50_common/Incr.h"
#include "wsg_50_common/Cmd.h"
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include <iostream>
#include <thread>  // NOLINT(build/c++11)
#include <chrono>  // NOLINT(build/c++11)
#include <rw/invkin/InvKinSolver.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>

using namespace std;
using namespace rw::math;
using namespace rw::common;
using namespace rw::graphics;
using namespace rw::kinematics;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::sensor;
using namespace rw;
using rw::invkin::JacobianIKSolver;
using rw::kinematics::State;
using rw::models::Device;

class Gripper {

private:
    ros::NodeHandle n;
    ros::ServiceClient gripper_grasp_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/grasp");
    ros::ServiceClient gripper_move_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/move");
    ros::ServiceClient gripper_homing_client = n.serviceClient<std_srvs::Empty>("/wsg_50_driver/homing");

public:
    Gripper()
    {
        ros::spinOnce();
    }

    bool gripper_grasp(double width, double speed)
    {
        std::cout << "Grasping start" << std::endl;
        ros::spinOnce();
        wsg_50_common::Move grasp;
        grasp.request.width = width; //20.0
        grasp.request.speed = speed; //100.0
        //gripper_grasp_client.call(grasp);
        if (gripper_grasp_client.call(grasp) == false) {
            throw "Gripper grasp error";
            return false;
        }
        std::cout << "Grasp end" << std::endl;
        return true;
    }
    bool gripper_move(double width, double speed)
    {
        std::cout << "Gripper move start" << std::endl;
        ros::spinOnce();
        wsg_50_common::Move move;
        move.request.width = width; //20.0
        move.request.speed = speed; //100.0
        if (gripper_move_client.call(move) == false) {
            throw "Gripper move error";
            return false;
        }
        std::cout << "Gripper move end" << std::endl;
        return true;
    }
    bool gripper_homing()
    {
        std::cout << "Gripper homing start" << std::endl;
        ros::spinOnce();
        std_srvs::Empty homing;
        if (gripper_homing_client.call(homing) == false) {
            throw "Gripper homing error";
            return false;
        }
        std::cout << "Gripper homing end" << std::endl;
        return true;
    }

};


class URRobot {
    using Q = rw::math::Q;

private:
    ros::NodeHandle nh;
    rw::models::WorkCell::Ptr wc;
    rw::models::Device::Ptr device;
    rw::kinematics::State state;
    caros::SerialDeviceSIProxy* robot;
    Frame* TCP;
    Frame* gripper_frame;


public:
    URRobot()
    {
        auto packagePath = ros::package::getPath("ur_caros_example");
        wc = rw::loaders::WorkCellLoader::Factory::load(packagePath + "/WorkCell/Scene.wc.xml");
        device = wc->findDevice("UR5");
        state = wc->getDefaultState();
        robot = new caros::SerialDeviceSIProxy(nh, "caros_universalrobot");
        TCP = wc->findFrame("UR5.TCP");
        gripper_frame = wc->findFrame("UR5.GG");

        // Wait for first state message, to make sure robot is ready
        ros::topic::waitForMessage<caros_control_msgs::RobotState>("/caros_universalrobot/caros_serial_device_service_interface/robot_state", nh);
        ros::spinOnce();
    }

    bool forceStart(const rw::math::Transform3D<> refToffset, const rw::math::Q selection, const rw::math::Wrench6D<> wrench_target, int type, const rw::math::Q limits)
    {
        ros::spinOnce();
        if (robot->moveForceModeStart(refToffset, selection, wrench_target, type, limits)) {
            ROS_INFO("Force was successfull");
            return true;
        } else {
            ROS_INFO("Force was NOT successfull");
            return false;
        }
    }

    bool forceUpdate(const rw::math::Wrench6D<>& wrench_target) {
        ros::spinOnce();
        if (robot->moveForceModeUpdate(wrench_target)) {
            ROS_INFO("Force update was successfull");
            return true;
        } else {
            ROS_INFO("Force update was NOT successfull");
            return false;
        }
    }

    bool forceStop()
    {
        ros::spinOnce();
        if (robot->moveForceModeStop()) {
            ROS_INFO("STOP was successfull");
            return true;
        } else {
            ROS_INFO("STOP was NOT successfull");
            return false;
        }
    }


    Q getQ()
    {
        ros::spinOnce();
        Q q = robot->getQ();
        device->setQ(q, state);
        return q;
    }

    bool setQ(Q q)
    {
        float speed = 0.5;
        if (robot->movePtp(q, speed)) {
            device->setQ(q,state);
            return true;
        } else
            return false;
    }

    rw::math::Wrench6D<> getTCP_force()
    {
        ros::spinOnce();
        rw::math::Wrench6D<> TCP_force = robot->getTCPForce();
        return TCP_force;
    }

    bool velQ(Q q) {
        if (robot->moveVelQ(q)) {
            device->setQ(q,state);
            return true;
        } else
            return false;
    }

    bool velT(rw::math::VelocityScrew6D<> v) {
        robot->moveVelT(v);
        return true;
    }

    bool startServoMode(Q q) {
        double time = 1.0;
        double lookahead_time = 0.1;
        double gain = 200;
        device->setQ(q,state);
        //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
        //rwhw::URRTDE ur_rtde("localhost");
        if (robot->moveServoQ(q, time, lookahead_time, gain)) {
            //if (ur_rtde.servoJ(q,1.0,1.0,time,lookahead_time,gain)) {
            device->setQ(q,state);
            return true;
        } else return false;
    }

    bool updateServoMode(Q q) {
        if (robot->moveServoUpdate(q)) {
            device->setQ(q,state);
            return true;
        } else return false;
    }

    bool stopServoMode() {
        if (robot->moveServoStop()) {
            return true;
        } else return false;
    }

    bool robotStop() {
        if (robot->stop()) {
            return true;
        } else return false;
    }

    //center: center position of the spiral; length: number of circles; width: distance between spiral lines; resolution: number of waypoints per circle
    vector<rw::math::Vector3D<double>> calcSpiral(double length, double width, double resolution) {
        vector<rw::math::Vector3D<double>> spiralPos;
        rw::math::Vector3D<double> lastPos(0.0,0.0,0.0);
        rw::math::Vector3D<double> newPos;
        for (double i=0.0; i<=length*rw::math::Pi; i+=rw::math::Pi/resolution) {
            rw::math::Vector3D<double> pos2(width*i*cos(i), width*i*sin(i),0);
            if (i != 0.0) {
                newPos = lastPos-pos2;
                spiralPos.push_back(newPos);
                lastPos = pos2;
            }
        }
        return spiralPos;
    }


    //spiral with equidistant movements (not really good)
    vector<rw::math::Vector3D<double>> spiral(double spacing, double turns)
    {
        vector<rw::math::Vector3D<double>> sequence;
        double pi = 3.14159;
        double vertspacing = spacing;
        double horzspacing = spacing;
        double thetamax = turns*pi;
        double b = ((vertspacing/2)/pi);
        double smax = 0.5*b*thetamax*thetamax;
        double xi;
        double yi;
        double prev_xi = 0.0;
        double prev_yi = 0.0;
        for ( double s = 0.0; s < smax; s = s + horzspacing){
            double thetai = sqrt((2*s)/b);
            xi = b*thetai*cos(thetai);
            yi = b*thetai*sin(thetai);
            rw::math::Vector3D<double> deltaP( xi - prev_xi, yi - prev_yi, 0.0 );
            prev_xi = xi;
            prev_yi = yi;
            sequence.push_back(deltaP);
        }
        return sequence;
    }

    Q calcQ(rw::math::Vector3D<double> deltaPos) {
        rw::math::Rotation3D<double> rot(1,0,0,0,1,0,0,0,1);
        rw::math::Transform3D<double> delta_transf(deltaPos,rot);
        rw::math::VelocityScrew6D<double> dU = rw::math::VelocityScrew6D<>(delta_transf);
        //rw::math::Jacobian J = device->baseJframe(TCP, state);
        rw::math::Jacobian baseJend = device->baseJend(state);
        rw::math::Jacobian Jinv(baseJend.e().inverse());
        rw::math::Q deltaQ = Jinv*dU;
        rw::math::Q q_out = device->getQ(state) + deltaQ;
        return q_out;
    }

    std::vector<Q> inverseKinematics(const Transform3D<>& target)
    {
        JacobianIKSolver solver(device,gripper_frame, state);
        ///Transform3D<> bidi = device->baseTend(state);
        std::cout<<"Target: " << target <<std::endl;
        std::vector<Q> solutions = solver.solve(target, state);
        std::cout<<"Number of solutions = " << solutions.size() <<std::endl;
        for(Q q : solutions) {
            std::cout<<"Solution = "<<q<<std::endl;
        }
        return solutions;
    }

    Transform3D<double> baseTOend()
    {
        return device->baseTend(state);
    }

};


int main(int argc, char** argv)
{	
    ros::init(argc, argv, "WSGGripper");
    Gripper gripper;
    URRobot robot;

    bool calibration = false;
    bool testmovement = false;
    bool endstatetest = true;

    ///GRIPPER COMMANDS
    //gripper.gripper_move(50.0, 100.0);
    //gripper.gripper_grasp(20.0, 100.0);
    //gripper.gripper_homing();

    /// Q CONFIGURATIONS
    //Q[6]{3.139, -1.573, 0, -1.573, 0, 0}
    //Q[6]{1.19066, -1.24459, 1.27985, -1.63468, -1.5427, -0.400379}
    //Q[6]{1.165, -1.115, 1.582, -2.005, -1.554, -0.448}
    //Q[6]{1.178, -1.118, 1.657, -2.137, -1.576, -0.423}
    rw::math::Q q_1(6, 3.139, -1.573, 0, -1.573, 0, 0);
    rw::math::Q q_2(6, 1.19066, -1.24459, 1.27985, -1.63468, -1.5427, -0.400379);
    //rw::math::Q q_3(6, 1.178, -1.118, 1.657, -2.137, -1.576, -0.423);
    rw::math::Q q_3(6, 1.165, -1.128, 1.610, -2.021, -1.545, -0.407);
    //rw::math::Q q_4(6, 2.008, -0.99, 1.3, -1.883, -1.563, -1.147);
    rw::math::Q q_4(6, 2.033, -1.059, 1.416, -1.939, -1.573, -0.407);

    /// FORCE MODE
    rw::math::Rotation3D<> rotation_force(1,0,0,0,1,0,0,0,1);
    rw::math::Vector3D<double> position_force(0.0,0.0,0.0);
    const rw::math::Transform3D<> refToffset(position_force,rotation_force);
    const rw::math::Q selection(6,1.0,1.0,1.0,0.0,0.0,0.0);
    rw::math::Wrench6D<> wrench_target(0.0,0.0,-10.0,0.0,0.0,0.0);
    int type = 2;
    const rw::math::Q limits(6,100.0,100.0,150.0,10.0,10.0,10.0);

    /// CALIBRATION MOVEMENTS
    if (calibration == true){

        std::cout << "Calibration movement sequence start" << std::endl;
        /// CALIBRATION MOVEMENT
        //q0: Q[6]{5.282, -1.748, -2.766, 4.577, -1.507, -0.094}
        rw::math::Q q_c_0(6, 5.282, -1.748, -2.766, 4.577, -1.507, -0.094);
        //q1: Q[6]{5.282, -1.748, -2.766, 4.577, -1.626, -0.094}
        rw::math::Q q_c_1(6, 5.282, -1.748, -2.766, 4.577, -1.626, -0.094);
        //q2: Q[6]{5.282, -1.748, -2.766, 4.577, -1.704, -0.094}
        rw::math::Q q_c_2(6, 5.282, -1.748, -2.766, 4.577, -1.704, -0.094);
        //q3: Q[6]{5.282, -1.748, -2.766, 4.577, -1.391, 0.016}
        rw::math::Q q_c_3(6, 5.282, -1.748, -2.766, 4.577, -1.391, 0.016);
        //q4: Q[6]{5.276, -1.817, -2.679, 4.674, -1.501, 0.047}
        rw::math::Q q_c_4(6, 5.276, -1.817, -2.679, 4.674, -1.501, 0.047);
        //q5: Q[6]{5.276, -1.817, -2.741, 4.727, -1.451, -0.683}
        rw::math::Q q_c_5(6, 5.276, -1.817, -2.741, 4.727, -1.451, -0.683);
        //q6: Q[6]{5.276, -1.817, -2.748, 4.712, -1.466, 0.573}
        rw::math::Q q_c_6(6, 5.276, -1.817, -2.748, 4.712, -1.466, 0.573);
        //q7: Q[6]{6.212, -1.87, -2.55, 4.746, -0.229, -0.141}
        rw::math::Q q_c_7(6, 6.212, -1.87, -2.55, 4.746, -0.229, -0.141);
        //q8: Q[6]{6.125, -1.87, -2.531, 4.518, -0.489, -0.053}
        rw::math::Q q_c_8(6, 6.125, -1.87, -2.531, 4.518, -0.489, -0.053);
        //q9: Q[6]{6.125, -1.87, -2.531, 4.762, -0.473, 0.417}
        rw::math::Q q_c_9(6, 6.125, -1.87, -2.531, 4.762, -0.473, 0.417);
        //q10: Q[6]{6.194, -1.886, -2.271, 4.38, 0.182, -0.141}
        rw::math::Q q_c_10(6, 6.194, -1.886, -2.271, 4.38, 0.182, -0.141);

        //robot.setQ(q_1); //HOME

        robot.setQ(q_c_0);
        robot.setQ(q_c_1);
        robot.setQ(q_c_2);
        robot.setQ(q_c_3);
        robot.setQ(q_c_4);
        robot.setQ(q_c_5);
        robot.setQ(q_c_6);
        robot.setQ(q_c_7);
        robot.setQ(q_c_8);
        robot.setQ(q_c_9);
        robot.setQ(q_c_10);
        robot.setQ(q_c_0);

        //robot.setQ(q_1); //HOME

        return 0;
    }

    //rw::math::Transform3D<double> baseTend_fromcamera;
    //    rw::math::Vector3D<double> deltaPos(-0.000799058, -0.284, 0.99963);
    //    rw::math::Rotation3D<double> rot(0.0, 0.89, -0.002, -1, 0.0, 0.0, 0.0, 0.002, 0.89);
    //    rw::math::Transform3D<double> baseTend_fromcamera(deltaPos,rot);

    //    robot.inverseKinematics(baseTend_fromcamera);




    //    std::vector<rw::math::Q> possible_q = robot.inverseKinematics(baseTend_fromcamera);
    //    for (int i=0; i<possible_q.size();i++){
    //        std::cout << "Q: " << possible_q.at(i) << std::endl;
    //    }
    cout<<"CORRECT UR_CAROS_EXAMPLE"<<endl;
    /// END-STATE TEST
    if (endstatetest) {
        try{
            bool endstate = false;
            //robot.setQ(q_1);
            //gripper.gripper_homing();
            robot.setQ(q_2);
            robot.setQ(q_3);
            gripper.gripper_grasp(25.0, 100.0);

            robot.forceStart(refToffset, selection, wrench_target, type, limits);
            //robot.getTCP_force()[2] < 20.0 && robot.getTCP_force()[2] > -20.0
            while (endstate == false) {
                //if (robot.getTCP_force()[2] > 2.00) {
                    while (robot.getTCP_force()[2] < 9.8) {
                        cout<<robot.getTCP_force()<<endl;
                    }
                    endstate = true;
                //}

                //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            }
            robot.forceStop();
            endstate = false;
            robot.setQ(q_2);
            robot.setQ(q_4);

            robot.forceStart(refToffset, selection, wrench_target, type, limits);
            //robot.getTCP_force()[2] < 20.0 && robot.getTCP_force()[2] > -20.0
            while (endstate == false) {
                while (robot.getTCP_force()[2] < 9.8) {
                    cout<<robot.getTCP_force()<<endl;
                }
                endstate = true;

                //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            }
            //robot.forceStop();

            //Calculate the spiral q-configurations movements
            vector<rw::math::Vector3D<double>> seq = robot.calcSpiral(5.0,0.005,360);
            rw::math::Q q_spiral_init = robot.calcQ(seq.at(1));
            //robot.setQ(q_spiral_init);


            //robot.forceStart(refToffset, selection, wrench_target, type, limits);
            wrench_target[2] = -5.00;
            cout<<wrench_target<<endl;
            robot.forceUpdate(wrench_target);
            robot.startServoMode(q_spiral_init);
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));

            //int i = 0;
            for (int i = 0; i < seq.size(); i++){
                rw::math::Q q_spi = robot.calcQ(seq.at(i));
                std::cout << "robot.getTCP_force()[2] " << robot.getTCP_force()[2] << std::endl;
                robot.updateServoMode(q_spi);
                if (robot.getTCP_force()[2] < 2.0) {
                    robot.stopServoMode();
                    //robot.forceStop();
                    std::cout << "robot.forceStop() OVER THRESHOLD! " << std::endl;
                    break;
                }
            }
            endstate = false;
            while (endstate == false) {
                while (robot.getTCP_force()[2] < 4.8) {
                    cout<<robot.getTCP_force()<<endl;
                }
                endstate = true;

                //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            }
            robot.forceStop();
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            gripper.gripper_homing();
            robot.setQ(q_4);

        }
        catch (const char* msg) {
            cerr<<msg<<endl;
            robot.robotStop();
        }

    }


    /// TEST MOVEMENT
    if (testmovement)   {
        //Move above the object area
        robot.setQ(q_1);
        robot.setQ(q_2);

        //Calculate q-configuration from Transform3D of the object position and move there
        rw::math::Vector3D<double> po(-0.132884, -0.523793, 0.105755);
        rw::math::Rotation3D<double> ro(0.0154721, 0.999673, 0.0203547, -0.0369899, 0.0209155, -0.999097, -0.999196, 0.0147052, 0.0373014);
        rw::math::Transform3D<double> bas(po,ro);
        std::vector<rw::math::Q> po_q = robot.inverseKinematics(bas);
        if (po_q.size() != 0){
            std::cout << "Move to solution " << std::endl;
            robot.setQ(po_q.at(0));
        }else{
            std::cout << "No solutions " << std::endl;
        }

        //Close the gripper to grasp the object
        gripper.gripper_move(50.0, 100.0);
        gripper.gripper_grasp(20.0, 100.0);

        //Move above the insertion area
        //robot.setQ(q_3);
        robot.setQ(q_4);

        //Calculate the spiral q-configurations movements
        vector<rw::math::Vector3D<double>> seq = robot.calcSpiral(5.0,0.005,360);
        rw::math::Q q_spiral_init = robot.calcQ(seq.at(1));
        robot.setQ(q_spiral_init);



        //Start force mode and the spiral movement
        robot.startServoMode(q_spiral_init);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        robot.forceStart(refToffset, selection, wrench_target, type, limits);

        int i = 0;
        for (i = 1; i < seq.size(); i++){
            rw::math::Q q_spi = robot.calcQ(seq.at(i));
            std::cout << "robot.getTCP_force()[2] " << robot.getTCP_force()[2] << std::endl;
            robot.updateServoMode(q_spi);
            if (robot.getTCP_force()[2] > 4.0) {
                robot.stopServoMode();
                robot.forceStop();
                std::cout << "robot.forceStop() OVER THRESHOLD! " << std::endl;
                break;
            }

        }
        if ( i >= seq.size() - 1){
            robot.stopServoMode();
            robot.forceStop();
            std::cout << "robot.forceStop() " << std::endl;
        }

        //Open the gripper to release the object
        gripper.gripper_homing();

        //Move the robot in home position
        robot.setQ(q_1);
    }
    return 0;

    /////////////////////////////////////

    /*
    //for (int i=0; i<10;i++){



    robot.setQ(q_1);

    robot.setQ(q_2);


    //center: center position of the spiral; length: number of circles; width: distance between spiral lines; resolution: number of waypoints per circle
    vector<rw::math::Vector3D<double>> seq = robot.calcSpiral(2.0,0.005,360);
    //vector<rw::math::Vector3D<double>> seq = robot.spiral(0.05,40);
    rw::math::Q q_spiral_init = robot.calcQ(seq.at(1));
    robot.setQ(q_spiral_init);

    //ur_rtde.moveJ(q_spiral_init, 0.2, 0.1);
    //std::this_thread::sleep_for(std::chrono::milliseconds(3000));






    //    ur_rtde.servoJ(q_spiral_init, 0.1, 0.1, 5.0, 0.1, 300);

    //    for (int i = 1; i < seq.size(); i++){
    //        rw::math::Q q_spi = robot.calcQ(seq.at(i));
    //        std::cout << q_spi << std::endl;
    //        ur_rtde.servoUpdate(q_spi);
    //    }
    //    ur_rtde.servoStop();




    robot.startServoMode(q_spiral_init);


    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    for (int i = 1; i < seq.size(); i++){

        //rw::math::Transform3D<double> meh = seq[i];
        //std::cout << meh << std::endl;
        //rw::math::Q q_in = robot.getQ();
        rw::math::Q q_spi = robot.calcQ(seq.at(i));
        std::cout << "spiral " << q_spi << std::endl;
        //std::cout << "base to end " << robot.baseTOend() << std::endl;

        robot.updateServoMode(q_spi);
    }
    robot.stopServoMode();





    return 0;

    for (int i = 1; i < seq.size(); i++){

        //rw::math::Transform3D<double> meh = seq[i];
        //std::cout << meh << std::endl;
        //rw::math::Q q_in = robot.getQ();
        rw::math::Q q_spi = robot.calcQ(seq.at(i));
        std::cout << q_spi << std::endl;
        robot.setQ(q_spi);
    }

    return 0;
    std::cout << "robot.getQ().norm2 " << robot.getQ().norm2() << std::endl;


    if (robot.getQ().norm2() <= q_2.norm2() + 0.1 && robot.getQ().norm2() >= q_2.norm2() - 0.1){
        robot.forceStart(refToffset, selection, wrench_target, type, limits);
        std::cout << "FORCE MODE " << std::endl;
    }
*/

    //std::this_thread::sleep_for (std::chrono::seconds(4));


    //    if (robot.getQ().norm2() <= q_2.norm2() + 1.5 && robot.getQ().norm2() >= q_2.norm2() - 1.5){
    //        robot.forceStart(refToffset, selection, wrench_target, type, limits);

    //        while (true) {
    //            //std::cout << "robot.getTCP_force() " << robot.getTCP_force() << std::endl;
    //            std::cout << "robot.getTCP_force()[2] " << robot.getTCP_force()[2] << std::endl;
    //            if (robot.getTCP_force().force()[2] > 4) {
    //                robot.forceStop();
    //                std::cout << "robot.forceStop() " << std::endl;
    //                break;
    //            }
    //        }
    //    }







    //std::cout << "robot.getTCP_force() " << robot.getTCP_force() << std::endl;

    /*
            robot.setQ(q_3);
        if (gripper_grasp_client.call(grasp)) {
            ROS_INFO("Grasp was successfull");
        }
        else {
            ROS_INFO("GRASP WAS NOT SUCCESSFULL");
        }
            robot.setQ(q_2);
        robot.setQ(q_4);
        gripper_move_client.call(move);
        robot.setQ(q_1);
        gripper_homing_client.call(homing);
    */

    //just wrong here//caros::SerialDeviceSIProxy::moveForceModeStart(const rw::math::Transform3D<>& refToffset, const rw::math::Q& selection, const rw::math::Wrench6D<>& wrench_target, int type, const rw::math::Q& limits);

    //*/





    //ros::init(argc, argv, "URRobot");


    /*
    std::cout << "Current joint config:" << std::endl << robot.getQ() << std::endl << std::endl;

    std::cout << "Input destination joint config in radians:" << std::endl;
    float q1, q2, q3, q4, q5, q6;
    std::cin >> q1 >> q2 >> q3 >> q4 >> q5 >> q6;
    rw::math::Q q(6, q1, q2, q3, q4, q5, q6);

    if (robot.setQ(q))
        std::cout << std::endl << "New joint config:" << std::endl << robot.getQ() << std::endl;
    else
        std::cout << std::endl << "Failed to move robot" << std::endl;
    */




    return 0;

}
