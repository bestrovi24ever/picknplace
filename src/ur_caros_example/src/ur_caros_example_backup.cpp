#include "rw/rw.hpp"
#include "caros/serial_device_si_proxy.h"
#include "ros/package.h"
//#include "wsg_50/common.h"
//#include "wsg_50/cmd.h"
//#include "wsg_50/msg.h"
//#include "wsg_50/functions.h"

#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "wsg_50_common/Status.h"
#include "wsg_50_common/Move.h"
#include "wsg_50_common/Conf.h"
#include "wsg_50_common/Incr.h"
#include "wsg_50_common/Cmd.h"

#include "sensor_msgs/JointState.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include <iostream>


class URRobot {
	using Q = rw::math::Q;

private:
	ros::NodeHandle nh;
	rw::models::WorkCell::Ptr wc;
	rw::models::Device::Ptr device;
	rw::kinematics::State state;
	caros::SerialDeviceSIProxy* robot;

public:
	URRobot()
	{
		auto packagePath = ros::package::getPath("ur_caros_example");
		wc = rw::loaders::WorkCellLoader::Factory::load(packagePath + "/WorkCell/Scene.wc.xml");
		device = wc->findDevice("UR5");
		state = wc->getDefaultState();
		robot = new caros::SerialDeviceSIProxy(nh, "caros_universalrobot");

		// Wait for first state message, to make sure robot is ready
		ros::topic::waitForMessage<caros_control_msgs::RobotState>("/caros_universalrobot/caros_serial_device_service_interface/robot_state", nh);
	    ros::spinOnce();
	}

	Q getQ()
	{
		// spinOnce processes one batch of messages, calling all the callbacks
	    ros::spinOnce();
	    Q q = robot->getQ();
		device->setQ(q, state);
	    return q;
	}

	bool setQ(Q q)
	{
		// Tell robot to move to joint config q
        float speed = 0.5;
		if (robot->movePtp(q, speed)) { 
			return true;
		} else
			return false;
	}
};


int main(int argc, char** argv)
{	
	ros::init(argc, argv, "WSGGripper");
	URRobot robot;

	//Q[6]{3.139, -1.573, 0, -1.573, 0, 0}
    	//Q[6]{1.19066, -1.24459, 1.27985, -1.63468, -1.5427, -0.400379}
    	//Q[6]{1.165, -1.115, 1.582, -2.005, -1.554, -0.448}
	//Q[6]{1.178, -1.118, 1.657, -2.137, -1.576, -0.423}

    	rw::math::Q q_1(6, 3.139, -1.573, 0, -1.573, 0, 0);
    	rw::math::Q q_2(6, 1.19066, -1.24459, 1.27985, -1.63468, -1.5427, -0.400379);
    	rw::math::Q q_3(6, 1.178, -1.118, 1.657, -2.137, -1.576, -0.423);
	rw::math::Q q_4(6, 2.008, -0.99, 1.3, -1.883, -1.563, -1.147);
	
	ros::NodeHandle n;
	//ros::ServiceClient gripper_move_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/move");
	ros::ServiceClient gripper_grasp_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/grasp");
	//ROS_INFO("Test");

	wsg_50_common::Move grasp;
	grasp.request.width = 20.0;
	grasp.request.speed = 100.0;
	//ROS_INFO("Test3");
	
	/*
	if (client.call(srv)) {
		ROS_INFO("Test2");
	}
	else {
		ROS_INFO("Test4");
	}
	*/
	
	ros::ServiceClient gripper_move_client = n.serviceClient<wsg_50_common::Move>("/wsg_50_driver/move");
	wsg_50_common::Move move;
	move.request.width = 40.0;
	move.request.speed = 100.0;
	
	
	//ros::NodeHandle n2;	
	ros::ServiceClient gripper_homing_client = n.serviceClient<std_srvs::Empty>("/wsg_50_driver/homing");
	//ROS_INFO("Test");

	std_srvs::Empty homing;
	//client2.call(srv2);

	//for (int i=0; i<10;i++){

           	robot.setQ(q_1);
           	robot.setQ(q_2);
           	robot.setQ(q_3);
		if (gripper_grasp_client.call(grasp)) {
			ROS_INFO("Grasp was successfull");
		}
		else {
			ROS_INFO("GRAS WAS NOT SUCCESSFULL");
		}
           	robot.setQ(q_2);
		robot.setQ(q_4);
		gripper_move_client.call(move);
		robot.setQ(q_1);
		gripper_homing_client.call(homing);
		



    	//}

	
	
	//ros::init(argc, argv, "URRobot");
	

	/*	
	std::cout << "Current joint config:" << std::endl << robot.getQ() << std::endl << std::endl;

	std::cout << "Input destination joint config in radians:" << std::endl;
	float q1, q2, q3, q4, q5, q6;
	std::cin >> q1 >> q2 >> q3 >> q4 >> q5 >> q6;
	rw::math::Q q(6, q1, q2, q3, q4, q5, q6);

	if (robot.setQ(q))
		std::cout << std::endl << "New joint config:" << std::endl << robot.getQ() << std::endl;
	else
		std::cout << std::endl << "Failed to move robot" << std::endl;
	*/


	

	return 0;
}
