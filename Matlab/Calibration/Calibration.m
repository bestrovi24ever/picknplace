close all; clear all;  clc;

%% This script computes the average rotation and translation from the values measured
% in the calibration ROS node.
%% Add paths:
addpath("..\Utilities")
addpath("..\Quaternion_tools")

%% Loading the data
Calibration_data;

Position = data(:,1:3);
Quaternion = data(:,4:7);

%% Compute the average value for position:

Position_mean= mean(Position);
Position_median  = median(Position);

[m,n] = size(Position);
Position_sorted = sort(Position);
Position_mean_corrected = mean(Position_sorted(4:(m-4),:));

%% Plot results:

figure(1);
hold on;
plot3(Position(:,1),Position(:,2),Position(:,3),'o');
plot3(Position_mean(:,1),Position_mean(:,2),Position_mean(:,3),'xr');
plot3(Position_median(:,1),Position_median(:,2),Position_median(:,3),'xg');
plot3(Position_mean_corrected(:,1),Position_mean_corrected(:,2),Position_mean_corrected(:,3),'xb');
hold off;
legend('Cal pos','Mean','Median','Mean C');
title('Calibration: Translation estimation');
grid on;

%% Compute the average value for rotation:

Avg_Quaternion =  avg_quaternion_markley(Quaternion);

%% Plotting the estimated transformation:

fig_num = 2;
figure(fig_num);

ax = axes();
param_3d = [0.05,10,6,50,0.2,10,50];

% % Origin frame:
% origin_frame =[];
% % Calculate transformation:
% T_origin = makehgtform('translate',[0,0,0]);
% % Plot result:
% plot3dframe(fig_num,ax,origin_frame,T_origin,param_3d);
% txt = {'Origin Frame'};
% text(0,-0.05,0,txt)

% Plot Base frame:
% Calculate transformation:
world_T_base = T_matrix_RPY([0,0,0.81],[1.57,0,0]);
% Plot result:
plot3dframe(fig_num,ax,world_T_base,param_3d);
txt = {'Base Frame'};
text(0,-0.02 ,0.83,txt,'FontSize',14)


% Plot camera frame:
% Calculate transformation:
base_T_cam = T_matrix_Quaternions(Position_mean_corrected,Avg_Quaternion);


world_T_cam = world_T_base * base_T_cam;

plot3dframe(fig_num,ax,world_T_cam,param_3d);
txt = {'Camera Frame'};
text(world_T_cam(1,4),world_T_cam(2,4)-0.02,world_T_cam(3,4)+0.05,txt,'FontSize',14)

hold on
xd=linspace(-2,2,40);
yd=linspace(-2,2,40);
[x,y]=meshgrid(xd,yd);

% Table plane
xd=linspace(-0.1,0.85,40);
yd=linspace(-0.1,0.4,40);
[x,y]=meshgrid(xd,yd);

a3=0;
b3=0;
c3=1;
d3=0.79;
z3=-a3/c3*x-b3/c3*y+d3/c3;
hold on
mesh(x,y,z3,'EdgeColor', 'green')
hold off

title('Estimated transformation Base to Camera')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

%% Plotting the measurements:

figure(3)
ax = axes();
param_3d = [0.05,3,250,1600,0.2,600,3000];

for i=1:m   

    T = T_matrix_Quaternions(Position(i,:),Quaternion(i,:)');
    plot3dframe(3,ax,T,param_3d);
    
end

param_3d = [0.02,3,50,900,0.2,200,980];
plot3dframe(3,ax,base_T_cam,param_3d);

title('Calibration measurements & Average')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');


