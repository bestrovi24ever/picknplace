
% This function plots a 3d frame in a given position and quaternions.

% Parameters:
% figure_handle:   handle of the figure in which we want to plot the frame.
% axes_handle:     handle of the axes used as a reference for the transformation matrices.
% Transformation:  Transformation that we want to apply to the frame. (referenced to the axes defined by axes_handle)



function plotPiece(figure_handle,axes_handle,Transformation)

% figure handlers:
figure(figure_handle);
ax = axes_handle;
frame_handle = [];
% 3d view
view([1,1,1])
grid on;

% Creating the 3d frame surfaces:

% Cyclinder for the axes:
[xrod,yrod,zrod] = cylinder(0.2,10);
axis_ratio =8.5;
zrod = zrod/(axis_ratio -0.5 );
yrod = yrod/axis_ratio;
xrod = xrod/axis_ratio;
z_L =  max(zrod(:,1));

temp_surface(1) = surface(xrod,yrod,zrod,'FaceColor','c');

zrod = zrod*0.6;
temp_surface(2) = surface(xrod,yrod,zrod,'FaceColor','c');
temp_surface(3) = surface(xrod,yrod,zrod,'FaceColor','c');
rotate(temp_surface(2),[0 1 0],-90,[0,0,0]);
rotate(temp_surface(3),[0 1 0],+90,[0,0,0]);


% Disc at the begining of the cones:
[xdisc,ydisc,zdisc] = sphere();
disc_ratio = 45;
zdisc = zdisc/(disc_ratio*4);
ydisc = ydisc/disc_ratio;
xdisc = xdisc/disc_ratio;

temp_surface(4)  = surface(xdisc,ydisc,zdisc,'FaceColor','c');
temp_surface(5)  = surface(xdisc,ydisc,zdisc,'FaceColor','c');
temp_surface(6) = surface(xdisc,ydisc,zdisc,'FaceColor','c');

% Rotate the discs at x and y axes:
rotate(temp_surface(4),[0 1 0],-90,[0,0,0]);
rotate(temp_surface(5),[0 1 0],-90,[0,0,0]);
rotate(temp_surface(6),[0 0 1], 90,[0,0,0]);



% Calculated the desired transformation for the axis based on the input position and quaternion.
t = hgtransform('Parent',ax);
set(t,'Matrix',Transformation)

% Create a copy of the temporary 3d_axes to the input frame handle.
frame_handle = copyobj(temp_surface,t);

% Apply desired transformation to the frame.
set(frame_handle ,'Parent',t)

tz = hgtransform('Parent',ax);
Tz = makehgtform('translate',[0,0,z_L+0.02]);
set(tz,'Matrix',Tz)

set(tz,'Matrix',(Transformation*Tz))
set(frame_handle(6) ,'Parent',tz)


tz = hgtransform('Parent',ax);
Tz = makehgtform('translate',[0,0,0.02]);
set(tz,'Matrix',Tz)

set(tz,'Matrix',(Transformation*Tz))
set(frame_handle(1) ,'Parent',tz)
Tz = makehgtform('translate',[0,0,0.02]);
set(tz,'Matrix',Tz)
set(tz,'Matrix',(Transformation*Tz))
set(frame_handle(2) ,'Parent',tz)
set(frame_handle(3) ,'Parent',tz)


tx = hgtransform('Parent',ax);
Tx = makehgtform('translate',[max(zrod(:,1)),0,0.02]);
set(tx,'Matrix',Tx)

set(tx,'Matrix',(Transformation*Tx))
set(frame_handle(4)  ,'Parent',tx)


tx = hgtransform('Parent',ax);
Tx = makehgtform('translate',[-max(zrod(:,1)),0,0.02]);
set(tx,'Matrix',Tx)

set(tx,'Matrix',(Transformation*Tx))
set(frame_handle(5)  ,'Parent',tx)


% Remove the temporary surface.
delete(temp_surface)

end