function [T_inv] = T_matrix_inverse(T)

rotm = [];
pos = [];

% obtain rotation and translation:
for i=1:3
    for j=1:3
        rotm(i,j) =  T(i,j);

    end
end

rotm_inv = rotm';
T_inv = [];

% calculate the inverse of the rotation
for i=1:3
    for j=1:3
        T_inv(i,j) =  rotm_inv(i,j);
    end
end
% this will be tru opnly for this case!!!!!!!!!!!
pos(1) = T(3,4);
pos(2) = -T(1,4);
pos(3) = -T(2,4);
pos_inv =  ((-1) * rotm_inv) * pos' ;

for i=1:3
     T_inv(i,4) = pos(i);
     T_inv(4,i) = 0;
end

  T_inv(4,4) = 1;
         disp(T)
        disp(rotm)
        disp(pos)
                disp(T_inv)
        disp(rotm_inv)
end
