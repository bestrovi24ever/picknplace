
% This function plots a 3d frame in a given position and quaternions.

% Parameters:
% figure_handle:   handle of the figure in which we want to plot the frame.
% axes_handle:     handle of the axes used as a reference for the transformation matrices.
% Transformation:  Transformation that we want to apply to the frame. (referenced to the axes defined by axes_handle)
% 3d_param:     Parameters for the design of the 3d frame.
%                  - param(1): Diameter of the axes
%                  - param(2): Number of faces for the axes 
%                  - param(3): Axis reduction ratio, since he cylinder crates has lenght one, we may want to modify it,                      
%                  - param(4): Sphere reduction ratio, since he cylinder crates has lenght one, we may want to modify it,   
%                  - param(5): Diameter of the cones
%                  - param(6): Cone reduction ratio, since he cylinder crates has lenght one, we may want to modify it,   
%                  - param(7): Disc reduction ratio, since he cylinder crates has lenght one, we may want to modify it,   



function plot3dframe(figure_handle,axes_handle,Transformation,param_3d)

% figure handlers:
figure(figure_handle);
ax = axes_handle;
frame_handle = [];
% 3d view
view([1,1,1])
grid on;

% Creating the 3d frame surfaces:

% Cyclinder for the axes:
[xrod,yrod,zrod] = cylinder(param_3d(1),param_3d(2));
axis_ratio = param_3d(3);
zrod = zrod/axis_ratio;
yrod = yrod/axis_ratio;
xrod = xrod/axis_ratio;

temp_surface(1) = surface(xrod,yrod,zrod,'FaceColor','r');
temp_surface(2) = surface(xrod,yrod,zrod,'FaceColor','g');
temp_surface(3) = surface(xrod,yrod,zrod,'FaceColor','b');

% Rotate the cylinders at x and y axes:
rotate(temp_surface(2),[1 0 0],-90,[0,0,0]);
rotate(temp_surface(1),[0 1 0], 90,[0,0,0]);

% Sphere at the origin of the axes:
[xshepere,yshepere,zshepere] = sphere();
sphere_ratio = param_3d(4);
zshepere = zshepere/sphere_ratio;
yshepere = yshepere/sphere_ratio;
xshepere = xshepere/sphere_ratio;

temp_surface(4) = surface(xshepere,yshepere,zshepere,'FaceColor','k');

% Cones at the end of the axes:
[xcone,ycone,zcone] = cylinder([param_3d(5); 0]);
cone_ratio = param_3d(6);
zcone = zcone/cone_ratio;
ycone = ycone/cone_ratio;
xcone = xcone/cone_ratio;

temp_surface(5) = surface(xcone,ycone,zcone,'FaceColor','b');
temp_surface(6) = surface(xcone,ycone,zcone,'FaceColor','g');
temp_surface(7) = surface(xcone,ycone,zcone,'FaceColor','r');

% Rotate the cones at x and y axes:
rotate(temp_surface(6),[1 0 0],-90,[0,0,0]);
rotate(temp_surface(7),[0 1 0], 90,[0,0,0]);

% Disc at the begining of the cones:
[xdisc,ydisc,zdisc] = sphere();
disc_ratio = param_3d(7);
zdisc = zdisc/(disc_ratio*4);
ydisc = ydisc/disc_ratio;
xdisc = xdisc/disc_ratio;

temp_surface(8)  = surface(xdisc,ydisc,zdisc,'FaceColor','b');
temp_surface(9)  = surface(xdisc,ydisc,zdisc,'FaceColor','g');
temp_surface(10) = surface(xdisc,ydisc,zdisc,'FaceColor','r');

% Rotate the discs at x and y axes:
rotate(temp_surface(9),[1 0 0],-90,[0,0,0]);
rotate(temp_surface(10),[0 1 0], 90,[0,0,0]);

% Calculated the desired transformation for the axis based on the input position and quaternion.
t = hgtransform('Parent',ax);
set(t,'Matrix',Transformation)

% Create a copy of the temporary 3d_axes to the input frame handle.
frame_handle = copyobj(temp_surface,t);

% Apply desired transformation to the frame.
set(frame_handle ,'Parent',t)

% Move the cones and discs at the end of the axis.
tz = hgtransform('Parent',ax);
Tz = makehgtform('translate',[0,0,max(zrod(:,1))]);
set(tz,'Matrix',Tz)

set(tz,'Matrix',(Transformation*Tz))
set(frame_handle(5) ,'Parent',tz)
set(frame_handle(8) ,'Parent',tz)

ty = hgtransform('Parent',ax);
Ty = makehgtform('translate',[0,max(zrod(:,1)),0]);
set(ty,'Matrix',Ty)

set(ty,'Matrix',(Transformation*Ty))
set(frame_handle(6) ,'Parent',ty)
set(frame_handle(9) ,'Parent',ty)

tx = hgtransform('Parent',ax);
Tx = makehgtform('translate',[max(zrod(:,1)),0,0]);
set(tx,'Matrix',Tx)

set(tx,'Matrix',(Transformation*Tx))
set(frame_handle(7)  ,'Parent',tx)
set(frame_handle(10) ,'Parent',tx)

% Remove the temporary surface.
delete(temp_surface)

end