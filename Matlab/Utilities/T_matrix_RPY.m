function [T] = T_matrix_RPY(Position,RPY)


ang = RPY(1);                 
rotMat_Z = [ cos(ang) -sin(ang)        0 ;
             sin(ang) cos(ang)         0 ;
                    0        0         1 ];

ang = RPY(2);
rotMat_Y = [ cos(ang)        0   sin(ang);
                   0         1         0 ;
            -sin(ang)        0   cos(ang)];

ang = RPY(3);
rotMat_X = [       1         0         0 ;
                   0   cos(ang)  -sin(ang);
                   0   sin(ang)  cos(ang)];

RotRPY = (rotMat_Z * rotMat_Y * rotMat_X);
T =  makehgtform('translate',Position);


for i=1:3
    for j=1:3
        T(i,j) =  RotRPY(i,j);
    end
end

end