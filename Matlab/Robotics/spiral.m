x = 0.0;
y = 0.0;

for i=0.0:pi/32:6*pi
   newx = 0.01*i*cos(i);
   x = [x,newx];
   newy = 0.01*i*sin(i);
   y = [y,newy];
    
end

scatter(x,y)

%%
%x(t) = exp(t) cos(t), y(t) = exp(t) sin(t)

x = 0.0;
y = 0.0;
oldX = 0.0;
oldY= 0.0;

for i=0.0:0.01:6*pi
   newx = 0.00065*i*cos(i)
   newy = 0.00065*i*sin(i)
   dist = sqrt((oldX-newx)*(oldX-newx) + (oldY-newy)*(oldY-newy))
   if dist>=0.0004
       x = [x,newx];
       y = [y,newy];
       oldX = newx;
       oldY = newy;
   end
end

scatter(x,y)