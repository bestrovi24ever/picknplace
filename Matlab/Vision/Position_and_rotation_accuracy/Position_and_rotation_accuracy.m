clc,clear all,close all;

%% Definition:
% This script computes the accuracy of the pose estimation by comparing the position and rotation of the 
% estimation eith the known ones.

% Add paths:
addpath("..\..\Utilities");
addpath("..\..\Utilities\STLRead");
addpath("..\..\Utilities\Quaternion_tools");
addpath(".\PE_1");
addpath(".\PE_2");
addpath(".\PE_3");
addpath(".\PE_4");
addpath(".\PE_5");
addpath(".\PE_6");

% Initialization:
Difference_position = [];
Difference_angles = [];  
Difference_position_real = [];
Difference_angles_real = [];

% Figure parameters:
param_3d = [0.05,10,10,80,0.2,20,100];

% Calculate transformation world to camera:
world_T_base = T_matrix_RPY([0,0,0.81],[1.57,0,0]);

base_T_camP = [ 0.952511,  0.146336, -0.267249,  0.304012 ;
             0.302002, -0.569868,  0.764305, -0.85643 ;
            -0.040424, -0.808612, -0.586966,  0.266306 ;
             0 ,0 ,0 ,1 ];
         
world_T_camP = world_T_base * base_T_camP;

% Table plane Parameters
xd=linspace(0.1,0.85,8);
yd=linspace(-0.1,0.4,8);
[x,y]=meshgrid(xd,yd);

a3=0;
b3=0;
c3=1;
d3=0.7;
z3=-a3/c3*x-b3/c3*y+d3/c3;

% Load Data left:
Final_PositionPE_1
Data_pos = Data;
Final_Rot_quaternionsPE_1
Data_rot = Data;

% Save the successes.
[m,n] = size(Data);

Positions = [];
Quaternions = [];

for i = 1:m
    if Data_pos(i,4) == 1         
       Positions    = [Positions  ;Data_pos(i,1:3)];    
       Quaternions  = [Quaternions;Data_rot(i,1:4)];              
    end
end

% Calculate The average:
Position_left       = mean(Positions);
Avg_Quaternion_left = avg_quaternion_markley(Quaternions);

% Compute transformation matrix with the average values:
camP_T_ObjL = T_matrix_Quaternions(Position_left,Avg_Quaternion_left);

%% Case 1:
% Load data right.
Final_PositionPE_2;
Data_pos = Data;
Final_Rot_quaternionsPE_2;
Data_rot = Data;

% Save the successes.
[m,n] = size(Data);

Positions = [];
Quaternions = [];

for i = 1:m
    if Data_pos(i,4) == 1         
       Positions    = [Positions  ;Data_pos(i,1:3)];    
       Quaternions  = [Quaternions;Data_rot(i,1:4)];              
    end
end

% Calculate The average:
Position_right    = mean(Positions);
Avg_Quaternion_right = avg_quaternion_markley(Quaternions);

% Compute transformation matrix with the average values:
camP_T_ObjR = T_matrix_Quaternions(Position_right,Avg_Quaternion_right);

% Calculate distance:       
world_T_ObjR = world_T_camP * camP_T_ObjR;   
world_T_ObjL = world_T_camP * camP_T_ObjL;  

% Save Result:
Difference_position = [Difference_position;abs([(world_T_ObjR(1,4) - world_T_ObjL(1,4)), (world_T_ObjR(2,4) - world_T_ObjL(2,4)) ,(world_T_ObjR(3,4) - world_T_ObjL(3,4))])*100];
Difference_angles   = [Difference_angles ;abs(rotm2eul(world_T_ObjR(1:3,1:3))) - (rotm2eul(world_T_ObjL(1:3,1:3)))];

% Save Real values:
Difference_position_real = [Difference_position_real;[10,15,0]];
Difference_angles_real = [Difference_angles_real;[0,0,0]];

% initialize figure:
fig_num = 1;
figure(fig_num);
ax = axes('XLim',[0.3,0.7],'YLim',[-0.05,0.3],'ZLim',[0.7,1]);

% Object left:

plot3dframe(fig_num,ax,world_T_ObjL,param_3d);
plotPiece(fig_num,ax,world_T_ObjL);

% Object right:
plot3dframe(fig_num,ax,world_T_ObjR,param_3d);
plotPiece(fig_num,ax,world_T_ObjR);

% Table plane
hold on
mesh(x,y,z3,'EdgeColor', 'black','LineWidth',0.5)

%title('XY')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

%% Case 2:
% Load data right.
Final_PositionPE_3;
Data_pos = Data;
Final_Rot_quaternionsPE_3;
Data_rot = Data;


% Save the successes.
[m,n] = size(Data);

Positions = [];
Quaternions = [];

for i = 1:m
    if Data_pos(i,4) == 1         
       Positions    = [Positions  ;Data_pos(i,1:3)];    
       Quaternions  = [Quaternions;Data_rot(i,1:4)];              
    end
end

% Calculate The average:
Position_right    = mean(Positions);
Avg_Quaternion_right = avg_quaternion_markley(Quaternions);

% Compute transformation matrix with the average values:
camP_T_ObjR = T_matrix_Quaternions(Position_right,Avg_Quaternion_right);

% Calculate distance:       
world_T_ObjR = world_T_camP * camP_T_ObjR;   
world_T_ObjL = world_T_camP * camP_T_ObjL;  

% Save Result:
Difference_position = [Difference_position;abs([(world_T_ObjR(1,4) - world_T_ObjL(1,4)), (world_T_ObjR(2,4) - world_T_ObjL(2,4)) ,(world_T_ObjR(3,4) - world_T_ObjL(3,4))])*100];
Difference_angles   = [Difference_angles ;abs(rotm2eul(world_T_ObjR(1:3,1:3))) - (rotm2eul(world_T_ObjL(1:3,1:3)))];

% Save Real values:
Difference_position_real = [Difference_position_real;[10,15,0]];
Difference_angles_real = [Difference_angles_real;[90,0,0]];

% initialize figure:
fig_num = 2;
figure(fig_num);
ax = axes('XLim',[0.3,0.7],'YLim',[-0.05,0.3],'ZLim',[0.7,1]);

% Object left:
plot3dframe(fig_num,ax,world_T_ObjL,param_3d);
plotPiece(fig_num,ax,world_T_ObjL);

% Object right:
plot3dframe(fig_num,ax,world_T_ObjR,param_3d);
plotPiece(fig_num,ax,world_T_ObjR);

% Table plane
hold on
mesh(x,y,z3,'EdgeColor', 'black','LineWidth',0.5)

%title('Roll')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

%% Case 3:
% Load data right.
Final_PositionPE_4;
Data_pos = Data;
Final_Rot_quaternionsPE_4;
Data_rot = Data;


% Save the successes.
[m,n] = size(Data);

Positions = [];
Quaternions = [];

for i = 1:m
    if Data_pos(i,4) == 1         
       Positions    = [Positions  ;Data_pos(i,1:3)];    
       Quaternions  = [Quaternions;Data_rot(i,1:4)];              
    end
end

% Calculate The average:
Position_right    = mean(Positions);
Avg_Quaternion_right = avg_quaternion_markley(Quaternions);

% Compute transformation matrix with the average values:
camP_T_ObjR = T_matrix_Quaternions(Position_right,Avg_Quaternion_right);

% Calculate distance:       
world_T_ObjR = world_T_camP * camP_T_ObjR;   
world_T_ObjL = world_T_camP * camP_T_ObjL;  

% Save Result:
Difference_position = [Difference_position;abs([(world_T_ObjR(1,4) - world_T_ObjL(1,4)), (world_T_ObjR(2,4) - world_T_ObjL(2,4)) ,(world_T_ObjR(3,4) - world_T_ObjL(3,4))])*100];
Difference_angles   = [Difference_angles ;abs(rotm2eul(world_T_ObjR(1:3,1:3))) - (rotm2eul(world_T_ObjL(1:3,1:3)))];

% Save Real values:
Difference_position_real = [Difference_position_real;[10,15,12]];
Difference_angles_real = [Difference_angles_real;[90,0,180]];

% initialize figure:
fig_num = 3;
figure(fig_num);
ax = axes('XLim',[0.3,0.7],'YLim',[-0.05,0.3],'ZLim',[0.7,1]);

% Object left:
plot3dframe(fig_num,ax,world_T_ObjL,param_3d);
plotPiece(fig_num,ax,world_T_ObjL);

% Object right:
plot3dframe(fig_num,ax,world_T_ObjR,param_3d);
plotPiece(fig_num,ax,world_T_ObjR);

% Table plane
hold on
mesh(x,y,z3,'EdgeColor', 'black','LineWidth',0.5)

%title('Yaw')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

%% Case 4:
% Load data right.
Final_PositionPE_5;
Data_pos = Data;
Final_Rot_quaternionsPE_5;
Data_rot = Data;


% Save the successes.
[m,n] = size(Data);

Positions = [];
Quaternions = [];

for i = 1:m
    if Data_pos(i,4) == 1         
       Positions    = [Positions  ;Data_pos(i,1:3)];    
       Quaternions  = [Quaternions;Data_rot(i,1:4)];              
    end
end

% Calculate The average:
Position_right    = mean(Positions);
Avg_Quaternion_right = avg_quaternion_markley(Quaternions);

% Compute transformation matrix with the average values:
camP_T_ObjR = T_matrix_Quaternions(Position_right,Avg_Quaternion_right);

% Calculate distance:       
world_T_ObjR = world_T_camP * camP_T_ObjR;   
world_T_ObjL = world_T_camP * camP_T_ObjL;  

% Save Result:
Difference_position = [Difference_position;abs([(world_T_ObjR(1,4) - world_T_ObjL(1,4)), (world_T_ObjR(2,4) - world_T_ObjL(2,4)) ,(world_T_ObjR(3,4) - world_T_ObjL(3,4))])*100];
Difference_angles   = [Difference_angles ;abs(rotm2eul(world_T_ObjR(1:3,1:3))) - (rotm2eul(world_T_ObjL(1:3,1:3)))];

% Save Real values:
Difference_position_real = [Difference_position_real;[10,15,11.5-6]];
Difference_angles_real = [Difference_angles_real;[0,90,0]];

% initialize figure:
fig_num = 4;
figure(fig_num);
ax = axes('XLim',[0.3,0.7],'YLim',[-0.05,0.3],'ZLim',[0.7,1]);

% Object left:
plot3dframe(fig_num,ax,world_T_ObjL,param_3d);
plotPiece(fig_num,ax,world_T_ObjL);

% Object right:
plot3dframe(fig_num,ax,world_T_ObjR,param_3d);
plotPiece(fig_num,ax,world_T_ObjR);

% Table plane
hold on
mesh(x,y,z3,'EdgeColor', 'black','LineWidth',0.5)

%title('Pitch')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

%% Case 5:
% Load data right.
Final_PositionPE_6;
Data_pos = Data;
Final_Rot_quaternionsPE_6;
Data_rot = Data;


% Save the successes.
[m,n] = size(Data);

Positions = [];
Quaternions = [];

for i = 1:m
    if Data_pos(i,4) == 1         
       Positions    = [Positions  ;Data_pos(i,1:3)];    
       Quaternions  = [Quaternions;Data_rot(i,1:4)];              
    end
end

% Calculate The average:
Position_right    = mean(Positions);
Avg_Quaternion_right = avg_quaternion_markley(Quaternions);

% Compute transformation matrix with the average values:
camP_T_ObjR = T_matrix_Quaternions(Position_right,Avg_Quaternion_right);

% Calculate distance:       
world_T_ObjR = world_T_camP * camP_T_ObjR;   
world_T_ObjL = world_T_camP * camP_T_ObjL;  

% Save Result:
Difference_position = [Difference_position;abs([(world_T_ObjR(1,4) - world_T_ObjL(1,4)), (world_T_ObjR(2,4) - world_T_ObjL(2,4)) ,(world_T_ObjR(3,4) - world_T_ObjL(3,4))])*100];
Difference_angles   = [Difference_angles ;abs(rotm2eul(world_T_ObjR(1:3,1:3))) - (rotm2eul(world_T_ObjL(1:3,1:3)))];

% Save Real values:
Difference_position_real = [Difference_position_real;[10,15,4.75]];
Difference_angles_real = [Difference_angles_real;[0,0,0]];

% initialize figure:
fig_num = 5;
figure(fig_num);
ax = axes('XLim',[0.3,0.7],'YLim',[-0.05,0.3],'ZLim',[0.7,1]);

% Object left:
plot3dframe(fig_num,ax,world_T_ObjL,param_3d);
plotPiece(fig_num,ax,world_T_ObjL);

% Object right:
plot3dframe(fig_num,ax,world_T_ObjR,param_3d);
plotPiece(fig_num,ax,world_T_ObjR);

% Table plane
hold on
mesh(x,y,z3,'EdgeColor', 'black','LineWidth',0.5)

%title('XYZ')
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

%% Results:

disp(Difference_position);
disp((180/pi)*Difference_angles); 

disp(Difference_position_real);
disp(Difference_angles_real);

abs(Difference_position  -  Difference_position_real)
abs((180/pi)*Difference_angles -Difference_angles_real)