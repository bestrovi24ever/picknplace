clear all,close all,clc;

Number_of_points;

[m,n] = size(Data);

Size_PCD_Original =[];
Size_PCD_Filtered_z =[];
Size_PCD_Filtered_y =[];
Size_PCD_Filtered_x =[];
Size_PCD_Plane_inliers =[];
Size_PCD_Plane_under =[];
Size_PCD_Cluster_Downsample  =[];
Size_PCD_Cluster_removal =[];

for i = 1:3:m
    
    Size_PCD_Original = [Size_PCD_Original,Data(i,1)];
    Size_PCD_Filtered_z = [Size_PCD_Filtered_z,Data(i,2)]
    Size_PCD_Filtered_y = [Size_PCD_Filtered_y,Data(i,3)]
    Size_PCD_Filtered_x = [Size_PCD_Filtered_x,Data(i,4)]
    Size_PCD_Plane_inliers = [Size_PCD_Plane_inliers,Data(i+1,2)]
    Size_PCD_Plane_under = [Size_PCD_Plane_under,Data(i+1,3)]
    Size_PCD_Cluster_Downsample = [Size_PCD_Cluster_Downsample,Data(i+2,2)]
    Size_PCD_Cluster_removal = [Size_PCD_Cluster_removal,Data(i+2,3)]
    
end

figure(1)
subplot(4,3,1)
boxplot(Size_PCD_Original,'.','orientation'  , 'horizontal')
ylabel('PCL size')
subplot(4,3,4)
boxplot(Size_PCD_Filtered_z,'.','orientation'  , 'horizontal')
ylabel('PCL size')
subplot(4,3,5)
boxplot(Size_PCD_Filtered_y,'.','orientation'  , 'horizontal')
subplot(4,3,6)
boxplot(Size_PCD_Filtered_x,'.','orientation'  , 'horizontal')

subplot(4,3,7)
boxplot(Size_PCD_Plane_inliers,'.','orientation'  , 'horizontal')
ylabel('PCL size')
subplot(4,3,8)
boxplot(Size_PCD_Plane_under,'.','orientation'  , 'horizontal')


subplot(4,3,10)
boxplot(Size_PCD_Cluster_Downsample,'.','orientation'  , 'horizontal')
ylabel('PCL size')
subplot(4,3,11)
boxplot(Size_PCD_Cluster_removal,'.','orientation'  , 'horizontal')